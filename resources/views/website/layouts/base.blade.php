<!DOCTYPE html>
<html class=""
    xmlns="https://www.w3.org/1999/xhtml" lang="en-US" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-38622306-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'UA-38622306-2');
    </script>
    <meta name="google-site-verification" content="3qRC2ssBoQzESeIQt7cWf_zoXeridEF5QjJw5zugiUw" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>My Tutor Source</title>
    <meta property="og:image" content="{{ asset('web_wp-content/uploads/2017/07/logo_031.png') }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />    
    <!-- For iPhone -->
    <link rel="apple-touch-icon-precomposed" href="{{ asset('web_wp-content/uploads/2015/11/favicon-retina-ipad.html') }}">
    <!-- For iPhone 4 Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
        href="{{ asset('web_wp-content/uploads/2015/11/favicon-retina-ipad.html') }}">
    <!-- For iPad -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('web_wp-content/uploads/2015/11/favicon-retina-ipad.html') }}"> 
    <!-- For iPad Retina display -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
        href="{{ asset('web_wp-content/uploads/2015/11/favicon-retina-ipad.html') }}">
    <meta name='robots' content='noindex,follow' />
    <link rel="alternate" type="application/rss+xml" title="My Tutor Source &raquo; Feed" href="feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="My Tutor Source &raquo; Comments Feed" href="comments/feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="My Tutor Source &raquo; Online tutoring | Find a Tutor Comments Feed" href="home/feed/index.html" />
	<meta property="og:title" content="Online tutoring | Find a Tutor"/>
	<meta property="og:type" content="article"/>
	<meta property="og:url" content="https://dev.mytutorsource.com/"/>
	<meta property="og:site_name" content="My Tutor Source (MTS) provides High Quality and very Affordable online tuition to students around the globe. "/>
	<meta property="og:description" content="My Tutor Source (MTS) provides High Quality and very Affordable online tuition to students around the globe."/>
	<meta property="og:image" content="https://www.mytutorsource.com/wp-content/uploads/2018/04/logo_031.png"/>
	<script type="text/javascript">
		window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"https:\/\/dev.mytutorsource.com\/wp-includes\/js\/wp-emoji-release.min.js"}}; !function(a,b,c){function d(a){var c,d,e,f=b.createElement("canvas"),g=f.getContext&&f.getContext("2d"),h=String.fromCharCode;return g&&g.fillText?(g.textBaseline="top",g.font="600 32px Arial","flag"===a?(g.fillText(h(55356,56806,55356,56826),0,0),f.toDataURL().length>3e3):"diversity"===a?(g.fillText(h(55356,57221),0,0),c=g.getImageData(16,16,1,1).data,g.fillText(h(55356,57221,55356,57343),0,0),c=g.getImageData(16,16,1,1).data,e=c[0]+","+c[1]+","+c[2]+","+c[3],d!==e):("simple"===a?g.fillText(h(55357,56835),0,0):g.fillText(h(55356,57135),0,0),0!==g.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag"),unicode8:d("unicode8"),diversity:d("diversity")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag&&c.supports.unicode8&&c.supports.diversity||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
	</script>
	<style type="text/css">
        img.wp-smiley,
        img.emoji {
        	display: inline !important;
        	border: none !important;
        	box-shadow: none !important;
        	height: 1em !important;
        	width: 1em !important;
        	margin: 0 .07em !important;
        	vertical-align: -0.1em !important;
        	background: none !important;
        	padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='usrStyle-css'  href="{{ asset('wp-content/plugins/universal-star-rating/css/style.css') }}" type='text/css' media='all' />
    <link rel='stylesheet' id='contact-form-7-css'  href="{{ asset('wp-content/plugins/contact-form-7/includes/css/styles.css') }}" type='text/css' media='all' />
    <link rel='stylesheet' id='mts-parent-stylesheet-css'  href="{{ asset('wp-content/themes/MTS/style.css') }}" type='text/css') }}" media='all' />
    <link rel='stylesheet' id='avada-google-fonts-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A400%2C400italic%2C700%2C700italic%2C800%7CRaleway%3A400%2C400italic%2C700%2C700italic%2C800%7CPT+Sans%3A400%2C400italic%2C700%2C700italic%2C800' type='text/css' media='all' />
    <link rel='stylesheet' id='avada-stylesheet-css'  href="{{ asset('wp-content/themes/MTS-Child-Theme/style.css') }}" type='text/css' media='all' />
    <link rel='stylesheet' id='fontawesome-css'  href="{{ asset('wp-content/themes/MTS/assets/fonts/fontawesome/font-awesome.css') }}" type='text/css' media='all' />
    <link rel='stylesheet' id='avada-iLightbox-css'  href="{{ asset('wp-content/themes/MTS/ilightbox.css') }}" type='text/css' media='all' />
    <link rel='stylesheet' id='avada-animations-css'  href="{{ asset('wp-content/themes/MTS/animations.css') }}" type='text/css' media='all' />
    <script type='text/javascript' src="{{ asset('wp-includes/js/jquery/jquery.js') }}"></script>
    <script type='text/javascript' src="{{ asset('wp-includes/js/jquery/jquery-migrate.min.js') }}"></script>
    <link rel='https://api.w.org/' href='wp-json/index.html' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="{{ asset('wp-includes/wlwmanifest.xml') }}" /> 
    <meta name="generator" content="WordPress 4.4.23" />
    <link rel="canonical" href="index.html" />
    <link rel='shortlink' href='index.html' />
    <script type="text/javascript">
        var ajaxurl = 'wp-admin/admin-ajax.html';
    </script>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/website/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/website/style.css') }}">
    <input type='hidden' value='no' id='check_ad'>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=693125580868582&amp;ev=PageView&amp;noscript=1" /></noscript>
    <link rel="image_src" type="image/png" href="{{ asset('wp-content/uploads/2018/04/logo_031.png') }}" />
    <meta property="og:image" content="{{ asset('wp-content/uploads/2017/07/logo_031.png') }}" />
</head>
<body class="home page page-id-8 page-template page-template-new100-width page-template-new100-width-php fusion-body no-mobile-slidingbar no-mobile-totop mobile-logo-pos-left layout-boxed-mode menu-text-align-right mobile-menu-design-modern fusion-image-hovers" data-spy="scroll">
    <input type='hidden' value='no' id='country_uae_selection'>
    <div id="wrapper" class="">
        <div id="home" style="position:relative;top:1px;"></div>
        @include('website.layouts.header')
        <div id="sliders-container"></div>
        <div class="fusion-youtube-flash-fix"></div>
        <div id="main" class="clearfix " style="">
            <div class="fusion-row" style="">
                @yield('content')
            </div>
        </div>

    @include('website.layouts.footer')

				
		</div> <!-- wrapper -->



		


		<a class="fusion-one-page-text-link fusion-page-load-link"></a>



		<!-- W3TC-include-js-head -->



		<link rel='stylesheet' id='owl_carousel_css-css'  href="{{ asset('wp-content/plugins/slide-anything/owl-carousel/owl.carousel.css') }}" type='text/css' media='all' />
<link rel='stylesheet' id='owl_theme_css-css'  href="{{ asset('wp-content/plugins/slide-anything/owl-carousel/sa-owl-theme.css') }}" type='text/css' media='all' />
<link rel='stylesheet' id='owl_animate_css-css'  href="{{ asset('wp-content/plugins/slide-anything/owl-carousel/animate.min.css') }}" type='text/css' media='all' />
<script type='text/javascript' src="{{ asset('wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js') }}"></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"https:\/\/dev.mytutorsource.com\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","recaptchaEmpty":"Please verify that you are not a robot.","sending":"Sending ..."};
/* ]]> */
</script>
<script type='text/javascript' src="{{ asset('wp-content/plugins/contact-form-7/includes/js/scripts.js') }}"></script>
<script type='text/javascript' src="{{ asset('wp-includes/js/comment-reply.min.js') }}"></script>
<script type='text/javascript'>
/* <![CDATA[ */
var js_local_vars = {"admin_ajax":"https:\/\/dev.mytutorsource.com\/wp-admin\/admin-ajax.php","admin_ajax_nonce":"0de7fc6139","protocol":"1","theme_url":"https:\/\/dev.mytutorsource.com\/wp-content\/themes\/MTS","dropdown_goto":"Go to...","mobile_nav_cart":"Shopping Cart","page_smoothHeight":"false","flex_smoothHeight":"false","language_flag":"","infinite_blog_finished_msg":"<em>All posts displayed.<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","infinite_blog_text":"<em>Loading the next set of posts...<\/em>","portfolio_loading_text":"<em>Loading Portfolio Items...<\/em>","faqs_loading_text":"<em>Loading FAQ Items...<\/em>","order_actions":"Details","avada_rev_styles":"1","avada_styles_dropdowns":"0","blog_grid_column_spacing":"40","blog_pagination_type":"Pagination","body_font_size":"14","carousel_speed":"2500","custom_icon_image_retina":"","disable_mobile_animate_css":"1","disable_mobile_image_hovers":"0","portfolio_pagination_type":"Pagination","form_bg_color":"#ffffff","header_transparency":"0","header_padding_bottom":"0px","header_padding_top":"0px","header_position":"Top","header_sticky":"1","header_sticky_tablet":"1","header_sticky_mobile":"1","header_sticky_type2_layout":"menu_only","is_responsive":"1","is_ssl":"true","isotope_type":"masonry","layout_mode":"boxed","lightbox_animation_speed":"Fast","lightbox_arrows":"1","lightbox_autoplay":"0","lightbox_behavior":"all","lightbox_desc":"0","lightbox_deeplinking":"1","lightbox_gallery":"1","lightbox_opacity":"0.98","lightbox_path":"vertical","lightbox_post_images":"1","lightbox_skin":"light","lightbox_slideshow_speed":"5000","lightbox_social":"0","lightbox_title":"0","lightbox_video_height":"720","lightbox_video_width":"1280","logo_alignment":"Left","logo_margin_bottom":"0px","logo_margin_top":"0px","megamenu_max_width":"1100px","mobile_menu_design":"modern","nav_height":"65","nav_highlight_border":"0","page_title_fading":"0","pagination_video_slide":"0","related_posts_speed":"2500","retina_icon_height":"","retina_icon_width":"","submenu_slideout":"0","side_header_break_point":"800","sidenav_behavior":"Hover","site_width":"1050px","slider_position":"below","slideshow_autoplay":"1","slideshow_speed":"7000","smooth_scrolling":"1","status_lightbox":"0","status_totop_mobile":"0","status_vimeo":"0","status_yt":"0","testimonials_speed":"4000","tfes_animation":"sides","tfes_autoplay":"1","tfes_interval":"3000","tfes_speed":"800","tfes_width":"150","title_style_type":"single","typography_responsive":"0","typography_sensitivity":"0.6","typography_factor":"1.5","woocommerce_shop_page_columns":"4","sticky_header_shrinkage":"1","side_header_width":"0"};
/* ]]> */
</script>
<script type='text/javascript' src="{{ asset('wp-content/themes/MTS/assets/js/main.min.js') }}" async ></script> 
<script type='text/javascript' src="{{ asset('wp-includes/js/wp-embed.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('wp-content/plugins/slide-anything/owl-carousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript">


jQuery(document).ready(function($) {
  
  var flag = false;
  $(".fusion-icon").on("click", function(){

  if (flag == false)
  {
    $(".fusion-icon").removeClass("fusion-icon-bars").addClass("fa fa-times");
    flag = true;
  }
  else if (flag == true)
  {
    $(".fusion-icon").removeClass("fa fa-times").addClass("fusion-icon-bars");
    flag = false;
  }
  
  });

$(window).scroll(function() {
    if ($('.fusion-mobile-nav-holder').is(':visible') == false) {
      $('.fusion-icon').removeClass('fa fa-times');
      $('.fusion-icon').addClass('fusion-icon-bars');
      flag = false;
    }
  });
  
  });
</script>






 





		<!--[if lte IE 8]>

			<script type="text/javascript" src="https://dev.mytutorsource.com/wp-content/themes/MTS/asset/js/respond.js"></script>

		<![endif]-->










<input type='hidden' value='yes' id='change_ip_cnnnnt'><input type='hidden' value='PK' id='change_ip_cnnnnt_value'><input type='hidden' value='Online tutoring | Find a Tutor' id='pagename_js'>




<style>
        #_hj-f5b2a1eb-9b07_feedback{display:none;}
        #_hj-f5b2a1eb-9b07_feedback_minimized{display:none;}
        #_hj-f5b2a1eb-9b07_feedback_minimized{display:none;}
        #_hj-f5b2a1eb-9b07_feedback_minimized_message{display:none;}

#_hj-f5b2a1eb-9b07_feedback[data-minimized-position="bottom_left"] ._hj-f5b2a1eb-9b07_hotjar_buddy {
    left: 0;
    display: none !important;
}

    
    #_hj-f5b2a1eb-9b07_feedback #_hj-f5b2a1eb-9b07_feedback_minimized ._hj-f5b2a1eb-9b07_hotjar_buddy:after {
    content: "";
    position: absolute;
    z-index: 1;
    top: 17px;
    left: 25px;
    background: none !important;
    width: 6px;
    height: 1px;
    -webkit-box-shadow:  none !important;
    -moz-box-shadow:  none !important;
    /* box-shadow: 0 2px 18px 18px rgba(0, 0, 0, .48); */
    -webkit-transition:  none !important;
    -moz-transition:  none !important;
    -o-transition:  none !important;
    -ms-transition:  none !important;
    transition: all .2s ease-in-out;
}
</style>


<script>  

		var doc = document.documentElement;

		doc.setAttribute('data-useragent', navigator.userAgent);





jQuery(document).ready(function(){



 



if(jQuery("#check_ad").val() == 'yes'){

//$("#menu-item-1973").show(); 

}else{

}

if (window.matchMedia('(max-width:700px)').matches) {

     jQuery(".mobi-topbar").html("<a id='mobibtn' href='get-started-for-free/index.html'>Online Trial (Free!)</a><a id='mobibtn' href='how-it-works/index.html'>How It Works</a>");

    }

 if(jQuery("#check_ccnty").val() =='hk'){

	jQuery("select[name='menu-872']").html('<option value="Hong Kong" selected>Hong Kong</option><option value="United Arab Emirates">United Arab Emirates</option><option value="United Kingdom">United Kingdom</option><option value="Singapore">Singapore</option>');

}





if(jQuery("#check_ad").val() == 'yes'){

//$("#menu-item-1973").show(); 

}else{

}

if (window.matchMedia('(max-width:700px)').matches) {

     jQuery(".mobi-topbar").html("<a id='mobibtn' href='get-started-for-free/index.html'>Online Trial (Free!)</a><a id='mobibtn' href='how-it-works/index.html'>How It Works</a>");

    }

 if(jQuery("#check_ccnty").val() =='hk'){

	jQuery("select[name='menu-872']").html('<option value="Hong Kong" selected>Hong Kong</option><option value="United Arab Emirates">United Arab Emirates</option><option value="United Kingdom">United Kingdom</option><option value="Singapore">Singapore</option>');

}

});



jQuery(document).ready(function(){



 			if(jQuery("#country_uae_selection").val()  == 'yes'){

			jQuery('select[name="menu-872"]').find('option[value="United Arab Emirates"]').attr("selected",true);

			}

 			if(jQuery("#country_uae_selection").val()  == 'yes'){

			jQuery('select[name="menu-628"]').find('option[value="United Arab Emirates"]').attr("selected",true);

			}



		if(	jQuery("#pagename_js").val() == 'High Quality Private Online Tutoring'){ 



		jQuery("#cus_cities_united2").hide(); 

		jQuery('#cus_cities_united2 input').val("none");





		if(jQuery('select[name="menu-872"]').val()== 'United Arab Emirates'){

		}else{

			jQuery("#cus_cities_united").hide();

				jQuery("#cus_cities_united").html('<div class="div_span1"><p class="heading_p">Select Your City *</p></div><div class="div_span2"><span class="wpcf7-form-control-wrap menu-481"><div class="wpcf7-select-parent"><select name="menu-481" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false"><option value="none">none</option></select><div class="select-arrow" style="height: 33px; width: 33px; line-height: 33px;"></div></div></span></div>');

				jQuery("#cus_cities_united2").show(); 

				jQuery('#cus_cities_united2 input').val("");

		}





		jQuery(".menu-872 select").change(function(){  



			if(jQuery('select[name="menu-872"]').val()== 'United Arab Emirates'){

				jQuery("#cus_cities_united").show();

				jQuery("#cus_cities_united2").hide();

				jQuery('#cus_cities_united2 input').val("none");

				jQuery("#cus_cities_united").html('<div class="div_span1"><p class="heading_p">Select Your City *</p></div><div class="div_span2"><span class="wpcf7-form-control-wrap menu-481"><div class="wpcf7-select-parent"><select name="menu-481" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false"><option value="Dubai">Dubai</option><option value="Abu Dhabi">Abu Dhabi</option><option value="Sharjah">Sharjah</option><option value="Al Ain">Al Ain</option><option value="Ajman">Ajman</option><option value="Ras Al Khaimah">Ras Al Khaimah</option><option value="Fujairah">Fujairah</option><option value="Umm al-Quwain">Umm al-Quwain</option></select><div class="select-arrow" style="height: 33px; width: 33px; line-height: 33px;"></div></div></span></div>');



			}else{

				jQuery("#cus_cities_united").hide();

				jQuery("#cus_cities_united").html('<div class="div_span1"><p class="heading_p">Select Your City *</p></div><div class="div_span2"><span class="wpcf7-form-control-wrap menu-481"><div class="wpcf7-select-parent"><select name="menu-481" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false"><option value="none">none</option></select><div class="select-arrow" style="height: 33px; width: 33px; line-height: 33px;"></div></div></span></div>');

				jQuery("#cus_cities_united2").show(); 

				jQuery('#cus_cities_united2 input').val("");

			}



		});





			}

		var all_uae11=",Dubai,Abu Dhabi,Sharjah,Al Ain,Ajman,Ras Al Khaimah,Fujairah,Umm al-Quwain";

		var all_uk=",Aberdeen,Armagh,Bangor,Bath,Belfast,Birmingham,Bradford,Brighton and Hove,Bristol,Cambridge,Canterbury,Cardiff,Carlisle,Chester,Chichester,London,Coventry,Derby,Dundee,Durham,Edinburgh,Ely,Exeter,Glasgow,Gloucester,Hereford,Inverness,Kingston upon Hull,Lancaster,Leeds,Leicester,Lichfield,Lincoln,Lisburn,Liverpool,Londonderry,Manchester,Newcastle upon Tyne,Newport,Newry,Norwich,Nottingham,Oxford,Peterborough,Plymouth,Portsmouth,Preston,Ripon,Salford,Salisbury,Sheffield,Southampton,St Albans,St Davids,Stirling,Stoke-on-Trent,Sunderland,Swansea,Truro,Wakefield,Wells,Westminster,Winchester,Wolverhampton,Worcester,York";

		var all_hk=",Aberdeen,Fanling,Hong Kong Island,Kowloon,Kwai Chung,Kwai Tsing,Ma On Shan,Pol Fu Lam,Sai Kung,Sha Tin,Sheung Shui,Tai Po,Tin Shui Wai,Tseun Wan,Tseung Kwan ,Tsing Yi,Tuen Mun,Victoria,Yuen Long";

		var all_sg=",Singapore,Hougang,Tampines,Pasir Ris,Yishun,Choa Chu Kang,Toa Payoh,Bukit Batok,Queenstown,Clementi,Serangoon,Sembawang";

  			var all_uae="";



			if(jQuery(".menu-872 select").val()  == 'United Arab Emirates'){

			all_uae=all_uae11;

			}



			if(jQuery(".menu-872 select").val()  == 'United Kingdom'){

			all_uae=all_uk;

			}



			if(jQuery(".menu-872 select").val()  == 'Singapore'){

			all_uae=all_sg;

			}



			if(jQuery(".menu-872 select").val()  == 'Hong Kong'){

			all_uae=all_hk;

			}





			all_uae = all_uae.split(',');

			var all_appending="";

			for(var ii=0;ii<all_uae.length;ii++){

			all_appending +="<option value='"+all_uae[ii]+"'>"+all_uae[ii]+"</option>";

			}

			jQuery("#custom_city select").html(all_appending);

			jQuery(".menu-872 select").change(function(){ 

			if(jQuery(this).val() == 'United Arab Emirates'){

			all_uae=all_uae11;

			}

			if(jQuery(this).val() == 'United Kingdom'){

			all_uae=all_uk;

			}

			if(jQuery(this).val() == 'Singapore'){

			all_uae=all_sg;

			}

			if(jQuery(this).val() == 'Hong Kong'){

			all_uae=all_hk;

			}

			all_uae = all_uae.split(',');

			var all_appending="";

			for(var ii=0;ii<all_uae.length;ii++){

			all_appending +="<option value='"+all_uae[ii]+"'>"+all_uae[ii]+"</option>";

			}

			jQuery("#custom_city select").html(all_appending);



			}); 

		jQuery('#menu-item-2652').hide();
		jQuery('#mobile-menu-item-2652').hide();
		



		});
</script>
	<style>
	#menu-item-2652, #mobile-menu-item-2652{
		display:none;
	}
	.owl-carousel .owl-item img {
		max-width:100% !important;
		width:110px;
	}
	</style>






 

 