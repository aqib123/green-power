@extends('admin.layouts.base')
@section('content')
@php
    $glRecord = [];
    $status = false;
    $debitAccount = 0;
    $creditAccount = 0;
    if(count($record) > 0) {
        $glRecord = $record;
        $status = true;
    }
@endphp
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            @include('error.flash_message')
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">GL Account Report</h3>
                </div>
                <div class="panel-body">
                    <form action="{{ route('glaccount.form') }}" method="get">
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">GL Code</label>
                            <div class="col-sm-2">
                                <input type="number" name="gl_code" value="" class="form-control" placeholder="Enter GL Code for Filteration" />

                            </div>
                        </div>
                         <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Document No</label>
                            <div class="col-sm-2">
                                <input type="number" name="document_no" value="" class="form-control" placeholder="Enter Document Number for Filteration" />

                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Year</label>
                            <div class="col-sm-2">
                                <input type="text" name="year" class="year form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">From Period</label>
                            <div class="col-sm-2">
                                <select class="form-control from_period" name="from_period">
                                    <option value="">Select Option</option>
                                    <option value="1">January</option>
                                    <option value="2">Feburary</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">To Period</label>
                            <div class="col-sm-2">
                                <select class="form-control to_period" name="to_period">
                                    <option value="">Select Option</option>
                                    <option value="1">January</option>
                                    <option value="2">Feburary</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-11">
                                <button type="submit" class="btn btn-primary float-right offset-sm-2">Generate Report</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @if($status)
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Generated GL Report</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Transaction Type</th>
                            <th>GL Codes</th>
                            <th>Description</th>
                            <th>Amount</th>
                            <th>Company</th>
                            <th>Cost Center</th>
                            <th>Po Number</th>
                            <th>Container Number</th>
                            <th>Document Number</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($glRecord as $key => $value)
                            @php
                                if($value->transaction_type == 20)
                                {
                                    $creditAccount = $creditAccount + $value->amount;
                                }
                                else {
                                    $debitAccount = $debitAccount + $value->amount;
                                }
                            @endphp
                            <tr>
                                <td>{{$value->id}}</td>
                                <td>{{$value->transaction_type == 10 ? 'Debit' : 'Credit'}}</td>
                                <td>{{$value->gl_code}}</td>
                                <td>{{$value->gl_description}}</td>
                                <td>{{$value->transaction_type == 10 ? $value->amount : '- ('.$value->amount.')' }}</td>
                                <td>{{$value->company}}</td>
                                <td>{{$value->cost_center}}</td>
                                <td>{{$value->po_id}}</td>
                                <td>{{$value->container_id}}</td>
                                <td>{{$value->document_no}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <h4 style="background-color: beige;">Total = Debit ({{ $debitAccount }}) - Credit ({{ $creditAccount }}) = {!! $debitAccount - $creditAccount !!} </h4>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        $('.year').datepicker({
            format: "yyyy",
            viewMode: "years", 
            minViewMode: "years"
        });
    })
</script>
@endsection
