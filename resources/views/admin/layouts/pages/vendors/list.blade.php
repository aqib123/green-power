@extends('admin.layouts.base')
@section('css')
    <link rel="stylesheet" href="{{ asset('datatable/datatables.min.css') }}">
@endsection
@section('content')
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            <!-- BASIC TABLE -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Vendor List</h3>
                </div>
                <div class="panel-heading">
                    <a class="btn btn-primary" href="{{url('admin/add/vendor')}}">Add Vendor</a>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Full Name</th>
                            <th>Phone Number</th>
                            <th>Address</th>
                            <th>Description</th>
                            <th>Vendor Type</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END BASIC TABLE -->
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="{{ asset('datatable/datatables.min.js') }}"></script>
    <script type="text/javascript">
        $(function ($) {
            $(document).ready(function ($) {
                var result = [];
                $('.table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url:"{{ route('vendor.datatable.request') }}",
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                        },
                    },
                    columns: [

                        { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
                        { data: "full_name", name: 'full_name' },
                        { data: "phone_number", name: 'phone_number' },
                        { data: "address", name: 'address' },
                        { data: "description", name: 'description' },
                        { data: "vendor_type", name: 'vendor_type' }
                    ]
                });
		$.fn.dataTable.ext.errMode = 'none';
            });
        });
    </script>
@endsection

