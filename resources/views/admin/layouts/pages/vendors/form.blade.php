@extends('admin.layouts.base')
@section('content')
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            @include('error.flash_message')
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Add Vendor</h3>
                </div>
                <div class="panel-body">
                    <form action="{{ route('add.vendor') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Full Name</label>
                            <div class="col-sm-9">
                                <input type="text" name="full_name" value="{{old('full_name')}}" class="form-control" placeholder="Enter full name" required />
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Phone number</label>
                            <div class="col-sm-9">
                                <input type="text" name="phone_number" value="{{old('phone_number')}}" class="form-control" placeholder="Enter valid phone number" required />
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Address</label>
                            <div class="col-sm-9">
                                <textarea name="address" class="form-control">{{old('address')}}</textarea>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Description</label>
                            <div class="col-sm-9">
                                <textarea name="description" class="form-control">{{old('description')}}</textarea>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Vendor Type</label>
                            <div class="col-sm-9">
                                <select name="vendor_type" value="" class="form-control">
                                    <option value="">Select Role</option>
                                    <option value="0" {{ (old('vendor_type') == '0'? "selected":"") }}>Local</option>
                                    <option value="1" {{ (old('vendor_type') == '1'? "selected":"") }}>International</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-11">
                                <button type="submit" class="btn btn-primary float-right offset-sm-2">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
