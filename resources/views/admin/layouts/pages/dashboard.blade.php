@extends('admin.layouts.base')
@section('content')
<div class="container-fluid">
	<!-- OVERVIEW -->
	<div class="panel panel-headline">
		<div class="panel-heading">
			<h3 class="panel-title">Report Overview</h3>
			<p class="panel-subtitle">Todaye is {!! date("d-m-Y") !!}</p>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-3">
					<div class="metric">
						<span class="icon"><i class="fa fa-download"></i></span>
						<p>
							<span class="number">{{ $totalUserRegistration }}</span>
							<span class="title">Total Registration</span>
						</p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="metric">
						<span class="icon"><i class="fa fa-shopping-bag"></i></span>
						<p>
							<span class="number">{{ $totalTutorialRequest }}</span>
							<span class="title">Total Request Forms</span>
						</p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="metric">
						<span class="icon"><i class="fa fa-eye"></i></span>
						<p>
							<span class="number">0</span>
							<span class="title">Visits</span>
						</p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="metric">
						<span class="icon"><i class="fa fa-bar-chart"></i></span>
						<p>
							<span class="number">0</span>
							<span class="title">Form Request</span>
						</p>
					</div>
				</div>
			</div>
			<div class="row">

					<div id="headline-chart" class="ct-chart"></div>
				</div>
				<div class="col-md-3">
					<div class="weekly-summary text-right">
						<span class="number">2,315</span> <span class="percentage"><i class="fa fa-caret-up text-success"></i> 12%</span>
						<span class="info-label">Total Sales</span>
					</div>
					<div class="weekly-summary text-right">
						<span class="number">$5,758</span> <span class="percentage"><i class="fa fa-caret-up text-success"></i> 23%</span>
						<span class="info-label">Monthly Income</span>
					</div>
					<div class="weekly-summary text-right">
						<span class="number">$65,938</span> <span class="percentage"><i class="fa fa-caret-down text-danger"></i> 8%</span>
						<span class="info-label">Total Income</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection