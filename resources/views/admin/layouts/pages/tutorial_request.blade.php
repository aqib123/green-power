@extends('admin.layouts.base')
@section('content')
<div class="container-fluid">
	<h3 class="page-title">Tutorials Request Form</h3>
	<div class="row">
		<div class="col-md-12">
			<!-- BASIC TABLE -->
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Request Forms</h3>
				</div>
				<div class="panel-body">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Full Name</th>
								<th>Email</th>
								<th>Subject</th>
								<th>Type</th>
								<th>Country</th>
								<th>Status</th>
								<th>Phone Number</th>
								<th>Comment</th>
							</tr>
						</thead>
						<tbody>
							@if(isset($result))
									@foreach($result as $key => $value)
										<tr>
											<td></td>
											<td>{{$value->full_name}}</td>
											<td>{{$value->email}}</td>
											<td>{{$value->subjects}}</td>
											<td>{{$value->type}}</td>
											<td>{{$value->country}}</td>
											<td>{!!$value->status!!}</td>
											<td>{{$value->phone_no}}</td>
											<td>{{$value->comment}}</td>
										</tr>
									@endforeach
							@endif
						</tbody>
					</table>
					{{ $result->links() }}
				</div>
			</div>
			<!-- END BASIC TABLE -->
		</div>
	</div>
</div>
@endsection