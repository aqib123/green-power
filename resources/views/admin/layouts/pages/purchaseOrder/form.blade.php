@extends('admin.layouts.base')
@section('content')
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            @include('error.flash_message')
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Add Purchase Order</h3>
                </div>
                <div class="panel-body">
                    <form action="{{ route('add.purchase.order') }}" method="post">
                        {{ csrf_field() }}
                        @if(isset($newCode))
                            <div class="row form-group">
                                <label class="col-sm-2 col-form-label">Code</label>
                                <div class="col-sm-2">
                                    <input type="text" name="code" value="{{$newCode}}" class="form-control" placeholder="Enter name" disabled />
                                </div>
                            </div>
                        @endif
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Purchase Order Date</label>
                            <div class="col-sm-3">
                                <input type="date" name="purchase_order_date" class="form-control" required />
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Vendor Details</label>
                            <div class="col-sm-3">
                                <select name="vendor_id" id="id_stock_1_material" class="form-control" required>
                                    <option value="">Select Value</option>
                                    @foreach($vendorInfo as $key => $value)
                                        <option value="{{$value['id']}}">{{$value['full_name']}}</option>
                                    @endforeach  
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Exchange Rate (Yuan/PKR)</label>
                            <div class="col-sm-3">
                                <input type="text" name="exchange_rate_yuan" class="form-control exchange_rate_yuan" required />
                            </div>
                        </div>
                        <hr>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Material Information</label>
                        </div>    
                        <div class="row form-group">
                            <div id="czContainer">
                                <div id="first">
                                    <div class="recordset">
                                        <div class="fieldRow clearfix">
                                            <div class="col-md-2">
                                                <div id="div_id_stock_1_service" class="form-group">
                                                    <label for="id_stock_1_product" class="control-label  requiredField">
                                                        M. Name<span class="asteriskField">*</span>
                                                    </label>
                                                    <div class="controls">
                                                        <select name="material_id[]" id="id_stock_1_material" class="form-control">
                                                            <option value="">Select Value</option>
                                                            @foreach($materialInfo as $key => $value)
                                                                <option value="{{$value['id']}}">{{$value['name']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="parentDiv">
                                                <div class="col-md-2">
                                                    <div id="div_id_stock_1_quantity" class="form-group">
                                                        <label for="id_stock_1_quantity" class="control-label  requiredField">
                                                            Quantity<span class="asteriskField">*</span>
                                                        </label><div class="controls "><input class="numberinput form-control quantity_data" id="id_stock_1_quantity" name="quantity[]" type="number" /> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div id="div_id_stock_1_quantity" class="form-group">
                                                        <label for="id_stock_1_quantity" class="control-label  requiredField">
                                                            Unit(PKR)<span class="asteriskField">*</span>
                                                        </label><div class="controls "><input class="numberinput form-control pkr" id="id_stock_1_unit_price_pkr" name="unit_price_in_pkr[]" readonly /> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div id="div_id_stock_1_quantity" class="form-group">
                                                        <label for="id_stock_1_quantity" class="control-label  requiredField">
                                                            Unit(Yuan)<span class="asteriskField">*</span>
                                                        </label>
                                                        <div class="controls ">
                                                            <input class="numberinput form-control yuan" id="id_stock_1_unit_price_yuan" name="unit_price_in_yuan[]" /> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div id="div_id_stock_1_quantity" class="form-group">
                                                        <label for="id_stock_1_quantity" class="control-label  requiredField">
                                                            Total in PKR<span class="asteriskField">*</span>
                                                        </label>
                                                        <div class="controls ">
                                                            <input class="numberinput form-control total_in_pkr" id="id_stock_1_total_price_in_pkr" name="total_price_in_pkr[]" /> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div id="div_id_stock_1_quantity" class="form-group">
                                                        <label for="id_stock_1_quantity" class="control-label  requiredField">
                                                            Total in Yuan<span class="asteriskField">*</span>
                                                        </label>
                                                        <div class="controls ">
                                                            <input class="numberinput form-control total_in_yuan" id="id_stock_1_unit_price_yuan" name="total_price_in_yuan[]" /> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Amount Paid</label>
                            <div class="col-sm-3">
                                <input type="text" name="amount_paid" class="form-control" required />
                            </div>
                        </div>
                        <!-- <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Total in Yuan (Chineese)</label>
                            <div class="col-sm-3">
                                <input type="text" name="purchase_order_date" class="form-control" readonly />
                            </div>
                        </div> -->
                        <div class="row form-group">
                            <div class="col-sm-11">
                                <button type="submit" class="btn btn-primary float-right offset-sm-2">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        //One-to-many relationship plugin by Yasir O. Atabani. Copyrights Reserved.
        $("#czContainer").czMore();
        // Code for exchange rate
        $(document).on('change', '.yuan', function()
        {
            // let exchangeRate = ($(this).val() * 2.16)
            if($('.exchange_rate_yuan').val() == undefined || !$('.exchange_rate_yuan').val())
            {
                alert('Please add Todays Exchange Rate')
            }
            else 
            {
                let amount = $(this).val() * $('.exchange_rate_yuan').val()
                $(this).parent().parent().parent().parent().find(".pkr").val(amount)
                let quantity = $(this).parent().parent().parent().parent().find(".quantity_data").val()
                let unit_price_yuan = $(this).parent().parent().parent().parent().find(".yuan").val()
                $(this).parent().parent().parent().parent().find(".total_in_pkr").val(amount * quantity)
                $(this).parent().parent().parent().parent().find(".total_in_yuan").val(unit_price_yuan * quantity)
            } 
        })
        $(document).on('change', '.pkr', function()
        {
            // let exchangeRate = ($(this).val() * 2.16)
            if($('.exchange_rate_yuan').val() == undefined || !$('.exchange_rate_yuan').val())
            {
                alert('Please add Todays Exchange Rate')
            }
            else 
            {
                let amount = $('.exchange_rate_yuan').val() * $(this).val()
                $(this).parent().parent().parent().parent().find(".yuan").val(amount)
            } 
        })
    })
</script>
@endsection
