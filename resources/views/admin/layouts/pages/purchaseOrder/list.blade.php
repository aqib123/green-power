@extends('admin.layouts.base')
@section('css')
    <link rel="stylesheet" href="{{ asset('datatable/datatables.min.css') }}">
@endsection
@section('content')
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            <!-- BASIC TABLE -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Purchase Order List</h3>
                </div>
                <div class="panel-heading">
                    <a class="btn btn-primary" href="{{url('admin/add/purchase/order')}}">Add Purchase Order</a>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Vendor</th>
                                <th>Code</th>
                                <th>Status</th>
                                <th>Total (Pkr)</th>
                                <th>Total (Yuan)</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                </div>
            </div>
            <!-- END BASIC TABLE -->
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="{{ asset('datatable/datatables.min.js') }}"></script>
    <script type="text/javascript">
        $(function ($) {
            $(document).ready(function ($) {
                var result = [];
                $('.table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url:"{{ route('purchase_order.datatable.request') }}",
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                        },
                    },
                    columns: [

                        { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
                        { data: "vendor.full_name", name: 'vendor.full_name' },
                        { data: "code", name: 'code' },
                        { data: "status", name: 'status' },
                        { data: "total_in_pkr", name: 'total_in_pkr' },
                        { data: "total_in_yuan", name: 'total_in_yuan' }
                    ]
                });
		$.fn.dataTable.ext.errMode = 'none';
            });
        });
    </script>
@endsection

