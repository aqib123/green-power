@extends('admin.layouts.base')
@section('content')

@php
    $total_purchase_order = 0;
    $date = null;
    $no_of_packages = null;
    $custom_reference = null;
    $cash_no = null;
    $igm_no = null;
    $vessel = null;
    $bill_no = null;
    $description = null;
    $exchange_rate_yuan = null;
    $personal_expense_in_yuan = null;
    $shipping_expense_in_yuan = null;
    $personal_expense_in_pkr = null;
    $shipping_expense_in_pkr = null;
    $custom_duty_one = null;
    $custom_duty_two = null;
    $excise_one = null;
    $excise_two = null;
    $do = null;
    $do_charges = null;
    $examination = null;
    $group_misc = null;
    $yard_payment = null;
    $duty = null;
    $token = null;
    $de_blocking = null;
    $counter_ground = null;
    $sepy = null;
    $pdc_charges = null;
    $auction_release = null;
    $agency = null;
    $id = null;
    $containerFiles = null;
    $containerFilesStatus = false;
    if(isset($containerInfo)) {
        $date =  $containerInfo->date;
        $no_of_packages =  $containerInfo->no_of_packages;
        $custom_reference =  $containerInfo->custom_reference;
        $cash_no =  $containerInfo->cash_no;  
        $igm_no =  $containerInfo->igm_no;
        $vessel =  $containerInfo->vessel;
        $bill_no =  $containerInfo->bill_no;
        $description =  $containerInfo->description;
        $exchange_rate_yuan =  $containerInfo->exchange_rate_yuan;
        $personal_expense_in_yuan =  $containerInfo->personal_expense_in_yuan;
        $shipping_expense_in_yuan =  $containerInfo->shipping_expense_in_yuan;
        $personal_expense_in_pkr =  $containerInfo->personal_expense_in_pkr;
        $shipping_expense_in_pkr =  $containerInfo->shipping_expense_in_pkr;
        $custom_duty_one = $containerInfo->custom_duty_one;
        $custom_duty_two = $containerInfo->custom_duty_two;
        $excise_one = $containerInfo->excise_one;
        $excise_two = $containerInfo->excise_two;
        $do = $containerInfo->do;
        $do_charges = $containerInfo->do_charges;
        $examination = $containerInfo->examination;
        $group_misc = $containerInfo->group_misc;
        $yard_payment = $containerInfo->yard_payment;
        $duty = $containerInfo->duty;
        $token = $containerInfo->token;
        $de_blocking = $containerInfo->de_blocking;
        $counter_ground = $containerInfo->counter_ground;
        $sepy = $containerInfo->sepy;
        $pdc_charges = $containerInfo->pdc_charges;
        $auction_release = $containerInfo->auction_release;
        $agency = $containerInfo->agency;
        $id = $containerInfo->id;
        $total = $personal_expense_in_pkr + $shipping_expense_in_pkr + $custom_duty_one + $custom_duty_two + $excise_one + $excise_two + $do + $do_charges + $examination + $group_misc + $yard_payment + $duty + $token + $de_blocking + $counter_ground + $sepy + $pdc_charges + $auction_release + $agency;
        $totalChinaExpense = $personal_expense_in_pkr + $shipping_expense_in_pkr;
        $totalPakistanExpense = $custom_duty_one + $custom_duty_two + $excise_one + $excise_two + $do + $do_charges + $examination + $group_misc + $yard_payment + $duty + $token + $de_blocking + $counter_ground + $sepy + $pdc_charges + $auction_release + $agency;
        if(count($containerInfo->container_files_details) > 0)
        {
            $containerFiles = $containerInfo->container_files_details;
            $containerFilesStatus = true;
        }
    }   
@endphp
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            @include('error.flash_message')
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Add Container Details</h3>
                </div>
                <div class="panel-body">
                    <form action="{{ route('add.container.details') }}" method="post" class="container-form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @if(isset($newCode))
                            <div class="row form-group">
                                <label class="col-sm-2 col-form-label">New Code</label>
                                <div class="col-sm-2">
                                    <input type="text" name="code" value="{{$newCode}}" class="form-control" placeholder="Enter name" disabled />
                                </div>
                            </div>
                        @endif
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Date</label>
                            <div class="col-sm-3">
                                <input type="date" name="date" class="form-control" value="{{old('date', $date)}}" required {!! $date ? 'disabled' : '' !!}/>
                                @if($id)
                                    <input type="hidden" name="id" class="form-control" value="{{$id}}" />
                                @endif
                            </div>
                        </div>
                        @if($containerFilesStatus)
                            <hr>
                            <h3>Uploaded Images</h3>
                            <div class="row">
                                @foreach($containerFiles as $key => $value)
                                    <div class="col-xs-2">
                                        <a href="{{url($value->file_path)}}" target="_blank"><img src="{{url($value->file_path)}}" class="img-thumbnail container-img" alt="Cinque Terre"></a>
                                    </div>
                                @endforeach  
                            </div>
                            <hr>
                        @else
                            <div class="row form-group">
                                <label class="col-sm-2 col-form-label">Upload Multiple Images</label>
                                <div class="col-sm-2">
                                    <input type="file" id="container_files" name="container_files[]" multiple>
                                </div>
                            </div>
                        @endif
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">No of Packages *</label>
                            <div class="col-sm-2">
                                <input type="text" name="no_of_packages" class="form-control no_of_packages" value="{{old('no_of_packages', $no_of_packages)}}" required {!! $no_of_packages ? 'disabled' : '' !!}/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Custom Reference</label>
                            <div class="col-sm-2">
                                <input type="text" name="custom_reference" class="form-control custom_reference" value="{{old('custom_reference', $custom_reference)}}" required {!! $custom_reference ? 'disabled' : '' !!}/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Cash No</label>
                            <div class="col-sm-2">
                                <input type="text" name="cash_no" class="form-control cash_no" value="{{old('cash_no', $cash_no)}}" required {!! $cash_no ? 'disabled' : '' !!}/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">IGM No</label>
                            <div class="col-sm-2">
                                <input type="text" name="igm_no" value="{{old('igm_no', $igm_no)}}" class="form-control igm_no" required {!! $igm_no ? 'disabled' : '' !!}/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">VESSEL</label>
                            <div class="col-sm-2">
                                <input type="text" name="vessel" value="{{old('vessel', $vessel)}}" class="form-control vessel" required {!! $vessel ? 'disabled' : '' !!}/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Bill No</label>
                            <div class="col-sm-2">
                                <input type="text" name="bill_no" class="form-control bill_no" value="{{old('bill_no', $bill_no)}}" required {!! $bill_no ? 'disabled' : '' !!}/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Description</label>
                            <div class="col-sm-4">
                                <textarea class="form-control description" name="description" {!! $description ? 'disabled' : '' !!}>{{old('description', $description)}}</textarea>
                            </div>
                        </div>
                        <h3 class="panel-title"><b>China Expenses</b></h3><hr/>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Exchange Rate (Yuan/PKR)*</label>
                            <div class="col-sm-2">
                                <input type="text" name="exchange_rate_yuan" class="form-control exchange_rate_yuan" value="{{old('exchange_rate_yuan', $exchange_rate_yuan)}}" required {!! $exchange_rate_yuan ? 'disabled' : '' !!}/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">Personal Expense (yuan)</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="personal_expense_in_yuan" class="form-control personal_expense_in_yuan" value="{{old('personal_expense_in_yuan', $personal_expense_in_yuan)}}" required {!! $personal_expense_in_yuan ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">Shipping Expense (yuan)</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="shipping_expense_in_yuan" class="form-control shipping_expense_in_yuan" value="{{old('shipping_expense_in_yuan', $shipping_expense_in_yuan)}}" required {!! $shipping_expense_in_yuan ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">Personal Expense (pkr)</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="personal_expense_in_pkr" class="form-control personal_expense_in_pkr" value="{{old('personal_expense_in_pkr', $personal_expense_in_pkr)}}" readonly />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">Shipping Expense (pkr)</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="shipping_expense_in_pkr" class="form-control shipping_expense_in_pkr" value="{{old('shipping_expense_in_pkr', $shipping_expense_in_pkr)}}" readonly />
                                    </div>
                                </div>
                            </div>
                            @if(isset($containerInfo))
                                @if($containerInfo->status == 1)
                                    <div class="col-md-6">
                                        <div class="row form-group">
                                            <label class="col-sm-4 col-form-label">Total China Expense (pkr)</label>
                                            <div class="col-sm-4">
                                                <p>{!! number_format($totalChinaExpense, 3) !!}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @else
                                <div class="col-md-6">
                                    <div class="row form-group">
                                        <label class="col-sm-4 col-form-label">Total China Expense (pkr)</label>
                                        <div class="col-sm-4">
                                            <p class="total_china_expense_bar"></p>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <hr>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Add Purchase Orders</label>
                        </div>    
                        <div class="row form-group">
                            <div id="czContainer">
                                @if(isset($containerInfo->container_item_details))
                                    @if($containerInfo->container_item_details)
                                        @foreach($containerInfo->container_item_details as $containerInfoItemskey => $containerInfoItemsValue)
                                            @php
                                                $count = 1;
                                                $div = 'div_id_stock_'.$count.'_service';
                                            @endphp
                                            <div class="recordset">
                                                <div class="fieldRow clearfix">
                                                    <div class="col-md-2">
                                                        <div id="{{$div}}" class="form-group">
                                                            <label for="id_stock_{{$count}}_product" class="control-label  requiredField">
                                                                Purchase Order<span class="asteriskField">*</span>
                                                            </label>
                                                            <div class="controls">
                                                                <select name="purchase_order_id[]" id="id_stock_{{$count}}_material" class="form-control purchaseOrder" required {!! $containerInfoItemsValue['purchase_order_id'] ? 'disabled' : '' !!}>
                                                                    <option value="">Select Value</option>
                                                                    @foreach($purchaseOrderInfo as $key => $value)
                                                                        <option value="{{$value['id']}}" data-total="{{$value['total_in_pkr']}}" {{ $value['id'] == $containerInfoItemsValue['purchase_order_id'] ? 'selected="selected"' : ''}} >{{$value['code']}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div id="div_id_stock_{{$count}}_quantity" class="form-group">
                                                            <label for="id_stock_{{$count}}_quantity" class="control-label  requiredField">
                                                                Total Purchase Order<span class="asteriskField">*</span>
                                                            </label><div class="controls "><input class="numberinput form-control totalPurchaseOrder" id="id_stock_{{$count}}_quantity" name="total_in_pkr[]" type="number" value="{{$containerInfoItemsValue['total_in_pkr']}}" readonly required /> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @php 
                                                $total_purchase_order = $total_purchase_order + $containerInfoItemsValue['total_in_pkr'];
                                                $total = $total + $containerInfoItemsValue['total_in_pkr'];
                                                $count++;
                                            @endphp
                                        @endforeach
                                    @endif
                                @endif
                                <div id="first">
                                    <div class="recordset">
                                        <div class="fieldRow clearfix">
                                            <div class="col-md-2">
                                                <div id="div_id_stock_1_service" class="form-group">
                                                    <label for="id_stock_1_product" class="control-label  requiredField">
                                                        Purchase Order<span class="asteriskField">*</span>
                                                    </label>
                                                    <div class="controls">
                                                        <select name="purchase_order_id[]" id="id_stock_1_material" class="form-control purchaseOrder" required>
                                                            <option value="">Select Value</option>
                                                            @foreach($purchaseOrderInfo as $key => $value)
                                                                <option value="{{$value['id']}}" data-total="{{$value['total_in_pkr']}}">{{$value['code']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div id="div_id_stock_1_quantity" class="form-group">
                                                    <label for="id_stock_1_quantity" class="control-label  requiredField">
                                                        Total Purchase Order<span class="asteriskField">*</span>
                                                    </label><div class="controls "><input class="numberinput form-control totalPurchaseOrder" id="id_stock_1_quantity" name="total_in_pkr[]" type="number" readonly required /> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <hr>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Settlement Option</label>
                            <div class="col-sm-2">
                                <input type="checkbox" class="settlement_option">
                            </div>
                        </div>
                        <div class="row form-group settlementOption" style="display: none;">
                            <label class="col-sm-2 col-form-label">Setlement Option (Optional)</label>
                        </div> 
                        <!-- Settlement Form to Add here -->
                        <div class="row form-group settlementOption" style="display: none;">
                            <div id="czContainer1">
                                <div id="first">
                                    <div class="recordset">
                                        <div class="fieldRow clearfix">
                                            <div class="col-md-2">
                                                <div id="div_id_stock_1_service" class="form-group">
                                                    <label for="id_stock_1_product" class="control-label  requiredField">
                                                        Completed PO<span class="asteriskField">*</span>
                                                    </label>
                                                    <div class="controls">
                                                        <select name="completed_purchase_order_id[]" id="id_stock_1_material" class="form-control completedPurchaseOrder" required>
                                                            <option value="">Select Value</option>
                                                            @foreach($complatedPurchaseOrderInfo as $key => $value)
                                                                <option value="{{$value['id']}}">{{$value['code']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div id="div_id_stock_1_service" class="form-group">
                                                    <label for="id_stock_1_product" class="control-label  requiredField">
                                                        Material<span class="asteriskField">*</span>
                                                    </label>
                                                    <div class="controls">
                                                        <select name="settlement_material_id[]" id="id_stock_1_material" class="form-control materialItems">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div id="div_id_stock_1_quantity" class="form-group">
                                                    <label for="id_stock_1_quantity" class="control-label  requiredField">
                                                        Quantity<span class="asteriskField">*</span>
                                                    </label><div class="controls "><input class="numberinput form-control settlementQuantitly" id="id_stock_1_quantity" name="settlement_quantity[]" type="number" required /> </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End of Settlement Form -->
                        @if(isset($containerInfo))
                            @if($containerInfo->status == 1)
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row form-group">
                                        <label class="col-sm-4 col-form-label">Total Purchase Order (pkr)</label>
                                        <div class="col-sm-4">
                                            <p>{!! number_format($total_purchase_order, 3) !!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endif
                        @if(isset($status))
                        <hr>
                        <h3 class="panel-title"><b>Pakistan Expenses</b></h3><hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">Custom Duty 1</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="custom_duty_one" class="form-control custom_duty_one" value="{{old('custom_duty_one', $custom_duty_one)}}" required {!! $custom_duty_one ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">Custom Duty 2</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="custom_duty_two" class="form-control custom_duty_two" value="{{old('custom_duty_two', $custom_duty_two)}}" required {!! $custom_duty_two ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">Excise Charges 1</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="excise_one" class="form-control excise_one" value="{{old('excise_one', $excise_one)}}" required {!! $excise_one ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">Excise Charges 2</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="excise_two" class="form-control excise_two" value="{{old('excise_two', $excise_two)}}" required {!! $excise_two ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">DO</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="do" class="form-control do" value="{{old('do', $do)}}" required {!! $do ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">DO Charges</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="do_charges" class="form-control do_charges" value="{{old('do_charges', $do_charges)}}" required {!! $do_charges ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">Examination</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="examination" class="form-control examination" value="{{old('examination', $examination)}}" required {!! $examination ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">Group Misc</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="group_misc" class="form-control group_misc" value="{{old('group_misc', $group_misc)}}" required {!! $group_misc ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">Yard Payment</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="yard_payment" class="form-control yard_payment" value="{{old('yard_payment', $yard_payment)}}" required {!! $yard_payment ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">Duty</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="duty" class="form-control duty" value="{{old('duty', $duty)}}" required {!! $duty ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">Token Payment</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="token" class="form-control token" value="{{old('token', $token)}}" required {!! $token ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">De Blocking Charges</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="de_blocking" class="form-control de_blocking" value="{{old('de_blocking', $de_blocking)}}" required {!! $de_blocking ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">Counter Ground</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="counter_ground" class="form-control counter_ground" value="{{old('counter_ground', $counter_ground)}}" required {!! $counter_ground ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">Sepy Charges</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="sepy" class="form-control sepy" value="{{old('sepy', $sepy)}}" required {!! $sepy ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">PDC Charges</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="pdc_charges" class="form-control pdc_charges" value="{{old('pdc_charges', $pdc_charges)}}" required {!! $pdc_charges ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">Agency Charges</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="agency" class="form-control agency" value="{{old('agency', $agency)}}" required {!! $agency ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label class="col-sm-4 col-form-label">Auction Release</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="auction_release" class="form-control auction_release" value="{{old('auction_release', $auction_release)}}" required {!! $auction_release ? 'disabled' : '' !!}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @if(isset($containerInfo))
                            @if($containerInfo->status == 1)
                            <div class="row form-group">
                                <div class="col-md-6">
                                    <div class="row form-group">
                                        <label class="col-sm-4 col-form-label">Total Pakistani Expense (pkr)</label>
                                        <div class="col-sm-4">
                                            <p>{!! number_format($totalPakistanExpense, 3) !!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                            @endif
                        @endif
                        <div class="row form-group">
                            <div class="col-sm-11">
                                @if(isset($containerInfo))
                                    @if($containerInfo->status != 1)
                                        <button type="submit" class="btn btn-success float-right offset-sm-2">Complete Container Info</button>
                                    @else
                                        <h3><b>Total in Pkr = </b>{{number_format($total,3)}}</h3> 
                                    @endif
                                @else
                                    <button type="submit" class="btn btn-primary float-right offset-sm-2">Create</button>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    function submit() {
       return confirm('Do you really want to submit the form?');
    }
    $(document).ready(function(){
        //One-to-many relationship plugin by Yasir O. Atabani. Copyrights Reserved.
        $("#czContainer").czMore();
        $("#czContainer1").czMore();
        let totalChinaExpenses = 0;
        $(document).on('keyup', '.personal_expense_in_yuan', function()
        {
            // let exchangeRate = ($(this).val() * 2.16)
            if($('.exchange_rate_yuan').val() == undefined || !$('.exchange_rate_yuan').val())
            {
                alert('Please add Todays Exchange Rate')
            }
            else 
            {
                // let amount = $(this).val() / $('.exchange_rate_yuan').val()
                let amount = $(this).val() * $('.exchange_rate_yuan').val()
                $(".personal_expense_in_pkr").val(amount)
                totalChinaExpenses = totalChinaExpenses + amount
                $('.total_china_expense_bar').text(totalChinaExpenses)
            } 
        })

        $(document).on('keyup', '.shipping_expense_in_yuan', function()
        {
            // let exchangeRate = ($(this).val() * 2.16)
            if($('.exchange_rate_yuan').val() == undefined || !$('.exchange_rate_yuan').val())
            {
                alert('Please add Todays Exchange Rate')
            }
            else 
            {
                let amount = $(this).val() * $('.exchange_rate_yuan').val()
                $(".shipping_expense_in_pkr").val(amount)
                totalChinaExpenses = totalChinaExpenses + amount
                $('.total_china_expense_bar').text(totalChinaExpenses)
            } 
        })

        $(document).on('change','.purchaseOrder', function(){
            var dataTotal = $(this).find(':selected').attr('data-total');
            console.log(dataTotal)
            $(this).parent().parent().parent().parent().find(".totalPurchaseOrder").val(dataTotal)
        })

        $(document).on('change','.completedPurchaseOrder', function(){
            var purchaseId = $(this).val();
            if(purchaseId !== '') {
                var $t = $(this);
                $.ajax({
                    type: "POST",
                    url: "{{ url('/admin/po/material/list') }}",
                    data: { id: purchaseId, _token: "{{ csrf_token() }}" },
                    success: function(record) {
                        let content = '';
                        if(record.status) {
                            $.each( record.data.purchase_order_item_details, function( index, value ){
                                content += '<option>Select Value</option><option value="'+value.material_detail.id+'">'+value.material_detail.name+'</option>' 
                            });
                            console.log(content)
                            $t.parent().parent().parent().parent().find(".materialItems").html(content)
                        }
                        else {
                            alert('Please provide valid purchase order');
                        }
                    }
                });
            }
        })

        $(document).on('change','.settlement_option',function() {
            if($(this).is(":checked")) {
                $('.settlementOption').show()
            }
            else {
                $('.settlementOption').hide()
            }
        });

        $('.container-form').submit(function() {
            var c = confirm("Click OK to continue?");
            return c; //you can just return c because it will be true or false
        });
    })
</script>
@endsection
