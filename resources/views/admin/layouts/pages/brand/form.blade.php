@extends('admin.layouts.base')
@section('content')
@php
    $name = null;
    $code = null;
    $heading = 'Add Brand';
    if(isset($brand)) {
        $heading = 'Update Brand';
        $name = $brand['name'];
        $code = $brand['code'];
    }
@endphp
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            @include('error.flash_message')
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">{{$heading}}</h3>
                </div>
                <div class="panel-body">
                    <form action="{{ route('add.brand') }}" method="post">
                        {{ csrf_field() }}
                        @if(isset($newCode))
                            <div class="row form-group">
                                <label class="col-sm-2 col-form-label">New Code</label>
                                <div class="col-sm-2">
                                    <input type="text" name="code" value="{{$newCode}}" class="form-control" placeholder="Enter name" disabled />
                                </div>
                            </div>
                        @endif
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" name="name" value="{{old('name', $name)}}" class="form-control" placeholder="Enter name" />

                            </div>
                        </div>
                        @if($code)
                            <div class="row form-group">
                                <label class="col-sm-2 col-form-label">Code</label>
                                <div class="col-sm-9">
                                    <input type="text" name="code" value="{{$code}}" class="form-control" placeholder="Enter name" disabled />
                                    <input type="hidden" name="id" value="{{$brand['id']}}">
                                </div>
                            </div>
                        @endif
                        <div class="row form-group">
                            <div class="col-sm-11">
                                <button type="submit" class="btn btn-primary float-right offset-sm-2">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
