@extends('admin.layouts.base')
@section('content')
<div class="container-fluid">
	<h3 class="page-title"></h3>
	<div class="row">
		<div class="col-md-12">
			<!-- BASIC TABLE -->
			<div class="panel">
				<div class="panel-heading">
					<h3 class="panel-title">Register Parents List</h3>
				</div>
				<div class="panel-body">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Email</th>
								<th>Location</th>
								<th>Status</th>
								<th>Phone Number</th>
							</tr>
						</thead>
						<tbody>
							@if(isset($result))
								@foreach($result as $key => $value)
									<tr>
										<td></td>
										<td>{{$value->first_name}}</td>
										<td>{{$value->last_name}}</td>
										<td>{{$value->email}}</td>
										<td>{{$value->location}}</td>
										<td>{!! $value->active == 0 ? 'Not verified' : 'Verified' !!}</td>
										<td>{{$value->phone}}</td>
									</tr>
								@endforeach
							@endif
						</tbody>
					</table>
					{{ $result->links() }}
				</div>
			</div>
			<!-- END BASIC TABLE -->
		</div>
	</div>
</div>
@endsection