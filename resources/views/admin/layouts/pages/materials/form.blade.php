@extends('admin.layouts.base')
@section('content')
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            @include('error.flash_message')
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Add Material Item</h3>
                </div>
                <div class="panel-body">
                    <form action="{{ route('add.material') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" name="name" value="{{old('name')}}" class="form-control" placeholder="Enter Material name" required />
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Description</label>
                            <div class="col-sm-9">
                                <textarea name="description" class="form-control">{{old('description')}}</textarea>
                            </div>
                        </div>

                        <div class="row form-group watt_section">
                            <label class="col-sm-2 col-form-label">Watt</label>
                            <div class="col-sm-9">
                                <input type="text" name="watt" value="{{old('watt')}}" class="form-control watt" placeholder="Enter Watt"/>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">FG Type</label>
                            <div class="col-sm-9">
                                <select name="fg_type" value="" class="form-control">
                                    <option value="">Select FG Type</option>
                                    @foreach($fgTypes as $key => $value)
                                        <option value="{{$value['id']}}" {{ (old('fg_type') == $value['id'] ? "selected":"") }}>{{$value['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Brand</label>
                            <div class="col-sm-9">
                                <select name="brand" value="" class="form-control">
                                    <option value="">Select Brand</option>
                                    @foreach($brands as $key => $value)
                                        <option value="{{$value['id']}}" {{ (old('brand') == $value['id'] ? "selected":"") }}>{{$value['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Material Varient</label>
                            <div class="col-sm-9">
                                <select name="material_type" value="" class="form-control">
                                    <option value="">Select Type</option>
                                    <option value="0" {{ (old('material_type') == '0'? "selected":"") }}>Local</option>
                                    <option value="1" {{ (old('material_type') == '1' ? "selected":"") }}>International</option>
                                </select>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Material Type</label>
                            <div class="col-sm-9">
                                <select name="material_varient" value="" class="form-control material_varient">
                                    <option value="">Select Type</option>
                                    <option value="0" {{ (old('material_varient') == '0'? "selected":"") }}>Raw</option>
                                    <option value="1" {{ (old('material_varient') == '1'? "selected":"") }}>Pack</option>
                                    <option value="2" {{ (old('material_varient') == '2'? "selected":"") }}>Store Items</option>
                                    <option value="3" {{ (old('material_varient') == '3'? "selected":"") }}>Finish Goods</option>
                                </select>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">UOM</label>
                            <div class="col-sm-9">
                                <select name="no_of_uom" value="" class="form-control no_of_uom" required>
                                    <option value="">Select Type</option>
                                    <option value="0" { (old('material_varient') == '0'? "selected":"") }}>Kg</option>
                                    <option value="1" { (old('material_varient') == '1'? "selected":"") }}>Liter</option>
                                    <option value="2" { (old('material_varient') == '2'? "selected":"") }}>Per Unit</option>
                                    <option value="3" { (old('material_varient') == '3'? "selected":"") }}>Carton</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-11">
                                <button type="submit" class="btn btn-primary float-right offset-sm-2">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function ($) {
        $(document).on('change', '.material_varient', function(){
            if($(this).val() == 2) {
                $('.watt').val('')
                $('.watt_section').hide();
            }
            else {
                $('.watt_section').show();
            }
        })
    })
</script>
@endsection