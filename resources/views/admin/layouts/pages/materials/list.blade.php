@extends('admin.layouts.base')
@section('css')
    <link rel="stylesheet" href="{{ asset('datatable/datatables.min.css') }}">
@endsection
@section('content')
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            <!-- BASIC TABLE -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Material List</h3>
                </div>
                <div class="panel-heading">
                    <a class="btn btn-primary" href="{{url('admin/add/material')}}">Add Material</a>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Code</th>
                            <th>Full Name</th>
                            <th>Material Varient</th>
                            <th>Watt</th>
                            <th>Material Type</th>
                            <th>Material Brand</th>
                            <th>Material FG Type</th>
                            <th>UOM</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END BASIC TABLE -->
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="{{ asset('datatable/datatables.min.js') }}"></script>
    <script type="text/javascript">
        $(function ($) {
            $(document).ready(function ($) {
                var result = [];
                $('.table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url:"{{ route('material.datatable.request') }}",
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                        },
                    },
                    columns: [

                        { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
                        { data: "code", name: 'code' },
                        { data: "name", name: 'name' },
                        { data: "material_type", name: 'material_type' },
                        { data: "watt", name: 'watt' },
                        { data: "material_varient", name: 'material_varient' },
                        { data: "brand.name", name: 'brand.name' },
                        { data: "fg_type.name", name: 'fg_type.name' },
                        { data: "no_of_uom", name: 'no_of_uom'}
                    ]
                });
		$.fn.dataTable.ext.errMode = 'none';
            });
        });
    </script>
@endsection

