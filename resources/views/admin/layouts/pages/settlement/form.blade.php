@extends('admin.layouts.base')
@section('content')
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            @include('error.flash_message')
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title"><b>Settlement Form</b></h3>
                </div>
                <hr>
                <div class="panel-body">
                    <form action="{{ route('add.settlement.record') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row form-group">
                            <label class="col-sm-3 col-form-label">PO Code</label>
                            <div class="col-sm-6">
                                <p>{{$record['purchase_order_code']}}</p>
                                <input type="hidden" name="material_settlement" value="{{Crypt::encryptString($record['id'])}}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-3 col-form-label">Material Code</label>
                            <div class="col-sm-6">
                                <p>{{$record['material_code']}}</p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-3 col-form-label">Container Code</label>
                            <div class="col-sm-6">
                                <p>{{$record['container_code']}}</p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-3 col-form-label">Settlement Qty</label>
                            <div class="col-sm-6">
                                <p>{{$record['settlement_qty']}}</p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-3 col-form-label">Settlement Rate</label>
                            <div class="col-sm-6">
                                <p>{{$record['settlement_rate']}}</p>
                            </div>
                        </div>
                        @if($record['recieved_vendor_qty'])
                            <div class="row form-group">
                                <label class="col-sm-3 col-form-label">Recieved Vendor Qty</label>
                                <div class="col-sm-6">
                                    <p>{{$record['recieved_vendor_qty']}}</p>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-3 col-form-label">Status</label>
                                <div class="col-sm-6">
                                    <label class="badge badge-success text-white">{{ $record['status'] == 1 ? 'Completed' : 'Pending' }}</label>
                                </div>
                            </div>
                        @else
                            <div class="row form-group">
                                <label class="col-sm-3 col-form-label">Recieved Vendor Qty</label>
                                <div class="col-sm-3">
                                    <input type="number" name="recieved_vendor_qty" class="form-control">
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-11">
                                    <button type="submit" class="btn btn-primary float-right offset-sm-2">Update</button>
                                </div>
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
