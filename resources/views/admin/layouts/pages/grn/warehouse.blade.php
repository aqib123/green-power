@extends('admin.layouts.base')
@section('content')
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Inventory Warehouse details</h3>
                </div>
                <div class="panel-body">
                    <form>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">From Period</label>
                            <div class="col-sm-2">
                                <select class="form-control from_period" required>
                                    <option value="1">January</option>
                                    <option value="2">Feburary</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">To Period</label>
                            <div class="col-sm-2">
                                <select class="form-control to_period" required>
                                    <option value="1">January</option>
                                    <option value="2">Feburary</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Year</label>
                            <div class="col-sm-2">
                                <input type="text" name="year" class="year form-control" required>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Filteration on Material Code</label>
                            <div class="col-sm-2">
                                <input type="checkbox" value="" class="checkbox_material">
                            </div>
                        </div>
                        <div class="row form-group material_code_block" style="display: none;">
                            <label class="col-sm-2 col-form-label">Material Code</label>
                            <div class="col-sm-2">
                                <input type="number" name="material_code" class="material_code form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-2">
                                <a href="javascript:void(0)" class="btn btn-success generate_report">Generate Report</a>
                            </div>
                            <div class="col-sm-1 loading" style="display: none;">
                                <img src="{{ url('img/loading.gif') }}" class="loading_img">
                            </div>
                        </div>
                        <br>
                        <div class="inventory_record" style="display: none;">
                            <h3><b>Material Record</b></h3>
                            <div class="row form-group material_record">
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        $('.year').datepicker({
            format: "yyyy",
            viewMode: "years", 
            minViewMode: "years"
        });
        $(document).on('click', '.generate_report', function(){
            $('.loading').show();
            let record_data = {
                from_period: $('.from_period').val(),
                to_period: $('.to_period').val(),
                year: $('.year').val(),
                material_code: $('.material_code').val()
            }
            $.ajax({
                type: "POST",
                url: "{{ url('/admin/inventory/report') }}",
                data: { record: record_data, _token: "{{ csrf_token() }}" },
                success: function(record) {
                    let content = "";
                    if(record.status) {
                        for (const property in record.data) {
                            let openingQty = record.data[property][0].opening_qty
                            let openingValue = record.data[property][0].opening_value
                            let recievingQty = 0
                            let recievingValue = 0
                            let issuanceQty = 0
                            let issuanceValue = 0
                            let closingQty = 0
                            let closingValue = 0
                            let issuanceRate = 0

                            let url = window.location.origin+'/admin/show/inventory/details?materialcode='+record.data[property][0].material_code+'&year='+record.data[property][0].year+'&period='+record.data[property][0].period;
                            $.each( record.data[property], function( index, value ){
                                recievingQty = parseFloat(recievingQty) + parseFloat(value.total_receipt_qty)
                                recievingValue = parseFloat(recievingValue) + parseFloat(value.total_receipt_value)
                                issuanceQty = parseFloat(issuanceQty) + parseFloat(value.total_issuance_qty)
                                issuanceValue = parseFloat(issuanceValue) + parseFloat(value.total_issuance_value)
                                closingQty = value.closing_position_qty
                                closingValue = value.closing_position_value
                                issuanceRate = value.issuance_rate

                            })
                            content += '<h3><b>Material Code : <b>'+property+'</h3><h3><b>Issuance Rate : <b>'+issuanceRate+'</h3><a href="'+url+'" class="btn btn-warning">Show Details</a><table class="table table-responsive"><thead><tr><th>Description</th><th>Quantity</th><th>Value</th></tr></thead><tbody><tr><td>Opening</td><td>'+openingQty+'</td><td>'+openingValue+'</td></tr><tr><td>Recieved</td><td>'+recievingQty+'</td><td>'+recievingValue+'</td></tr><tr><td>Issuance</td><td>'+issuanceQty+'</td><td>'+issuanceValue+'</td></tr><tr><td>Closing</td><td>'+closingQty+'</td><td>'+closingValue+'</td></tr></tbody><table><br>'  
                        }
                        $('.material_record').html(content)
                        $('.loading').hide()
                        $('.inventory_record').show()
                    }
                    else {
                        $('.loading').hide()
                        alert('Validation error');
                    }
                }
            });
        })

        $(document).on('change', '.checkbox_material', function(){
            if(this.checked) {
                $('.material_code_block').show()
            }
            else {
                $('.material_code').val('');
                $('.material_code_block').hide()
            }
        })
    })
</script>
@endsection
