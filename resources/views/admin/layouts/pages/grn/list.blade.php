@extends('admin.layouts.base')
@section('css')
    <link rel="stylesheet" href="{{ asset('datatable/datatables.min.css') }}">
@endsection
@section('content')
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            <!-- BASIC TABLE -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">GRN List</h3>
                </div>
                <div class="panel-heading">
                    <a class="btn btn-primary" href="{{url('admin/grn/form')}}">Add GRN Record</a>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Purchase Order Code</th>
                            <th>Container Code</th>
                            <th>Container Total Amount</th>
                            <th>GRN Type</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END BASIC TABLE -->
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="{{ asset('datatable/datatables.min.js') }}"></script>
    <script type="text/javascript">
        $(function ($) {
            $(document).ready(function ($) {
                var result = [];
                $('.table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url:"{{ route('grn.datatable.request') }}",
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                        },
                    },
                    columns: [

                        { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
                        { data: "po_code", name: 'po_code' },
                        { data: "container_code", name: 'container_code' },
                        { data: "container_total_amount", name: 'container_total_amount' },
                        { data: "grn_type", name: 'grn_type' },
                        { data: "status", name: 'status' },
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
		$.fn.dataTable.ext.errMode = 'none';
            });
        });
    </script>
@endsection

