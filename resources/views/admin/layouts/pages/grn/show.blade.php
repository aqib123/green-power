@php
    $totalAmount = 0;
@endphp
@extends('admin.layouts.base')
@section('content')
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Good Recieve Notes Detail</h3>
                </div>
                <div class="panel-body">
                    <form>
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Purchase Order Code</label>
                            <div class="col-sm-9">
                                <p>{{$record['po_code']}}</p>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Container Code</label>
                            <div class="col-sm-9">
                                <p>{{$record['container_code']}}</p>
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Container Total Amount</label>
                            <div class="col-sm-9">
                               <p>{{number_format($record['container_total_amount'])}}</p>
                            </div>
                        </div>
                        
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">GRN Type</label>
                            <div class="col-sm-9">
                                <p>{{$record['grn_type']}}</p>
                            </div>
                        </div>
                        <hr>
                        <h3><b>Material Record</b></h3>
                        <div class="row form-group">
                                <table class="table table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Material Number</th>
                                            <th>POS Code</th>
                                            <th>Container Code</th>
                                            <th>Order Qty</th>
                                            <th>Recieved Qty</th>
                                            <th>Direct Purchase</th>
                                            <th>Direct Rate</th>
                                            <th>Container Expense</th>
                                            <th>Total Amount</th>
                                            <th>Revised Rate</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($record['grn_order_items_details'] as $key => $value)
                                            @php
                                                $totalAmount = $totalAmount + $value['total_amount'];
                                            @endphp
                                            <tr>
                                                <td>{{$value['material_code']}}</td>
                                                <td>{{$record['po_code']}}</td>
                                                <td>{{$record['container_code']}}</td>
                                                <td>{{number_format($value['qty'])}}</td>
                                                <td>{{number_format($value['recieved_qty'])}}</td>
                                                <td>{{number_format($value['direct_purchase'])}}</td>
                                                <td>{{number_format($value['direct_rate'])}}</td>
                                                <td>{{number_format($value['container_expense'])}}</td>
                                                <td>{{number_format($value['total_amount'])}}</td>
                                                <td>{{number_format($value['revised_rate'])}}</td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>{{number_format($totalAmount)}}</td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
