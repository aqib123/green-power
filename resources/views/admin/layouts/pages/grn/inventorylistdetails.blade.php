@extends('admin.layouts.base')
@section('css')
    <link rel="stylesheet" href="{{ asset('datatable/datatables.min.css') }}">
@endsection
@section('content')
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            <!-- BASIC TABLE -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">GRN List</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm">
                        <thead>
                        <tr>
                            <th>Storage Location</th>
                            <th>Maovement Type</th>
                            <th>Material Code</th>
                            <th>PO Code</th>
                            <th>Material Documentation Number</th>
                            <th>Quantity</th>
                            <th>Rate</th>
                            <th>Amount</th>
                            <th>Date of Entry</th>
                            <th>Year</th>
                            <th>Month</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($detailList as $key => $value)
                            <tr>
                                <td>{{$value['storage_location']}}</td>
                                <td>{{$value['movement_type']}}</td>
                                <td>{{$value['material_code']}}</td>
                                <td>{{$value['po_code']}}</td>
                                <td>{{$value['material_document_number']}}</td>
                                <td>{{$value['quantity']}}</td>
                                <td>{{$value['rate']}}</td>
                                <td>{{$value['amount']}}</td>
                                <td>{{ date('d-m-Y', strtotime( $value['created_at'] ))}}</td>
                                <td>{{$value['year']}}</td>
                                <td>{{date('F', mktime(0, 0, 0, $value['period'], 10))}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END BASIC TABLE -->
        </div>
    </div>
</div>
@endsection

