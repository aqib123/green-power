@extends('admin.layouts.base')
@section('content')
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            @include('error.flash_message')
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Add GRN</h3>
                </div>
                <div class="panel-body">
                    <form action="{{ route('add.grn.record') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row form-group">
                            <label class="col-sm-3 col-form-label">Type of GRN</label>
                            <div class="col-sm-3">
                                <select class="form-control grn_type" name="grn_type">
                                    <option value="">Select Options</option>
                                    @foreach(config('settings.TypeOfGRN') as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group po_details" style="display: none;">
                            <label class="col-sm-3 col-form-label">Purchase Order Number</label>
                            <div class="col-sm-3">
                                <input type="number" name="po_number" class="form-control po_number" placeholder="Purchase Order Code (*****)">
                            </div>
                            <div class="col-sm-1">
                                <a href="javascript:void(0)" class="btn btn-success search_purchase_order">Search</a>
                            </div>
                            <div class="col-sm-1 loading" style="display: none;">
                                <img src="{{ url('img/loading.gif') }}" class="loading_img">
                            </div>
                        </div>
                        <div class="po_details_report" style="display: none;">
                            <div class="row form-group">
                                <label class="col-sm-3 col-form-label">PO Number</label>
                                <div class="col-sm-3">
                                    <input type="text" value="" class="po_number_detail" disabled>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-3 col-form-label">PO Amount</label>
                                <div class="col-sm-3">
                                    <input type="text" value="" class="po_amount_detail" disabled>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-3 col-form-label">Container Code</label>
                                <div class="col-sm-3">
                                    <input type="text" value="" class="container_detail" disabled>
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-3 col-form-label">Container Amount</label>
                                <div class="col-sm-3">
                                    <input type="text" value="" class="container_amount_detail" disabled>
                                </div>
                            </div>
                            <div class="row form-group">
                                <table class="table table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Material Number</th>
                                            <th>POS Code</th>
                                            <th>Container Code</th>
                                            <th>Order Qty</th>
                                            <th>Recieved Qty</th>
                                            <th>Settlement</th>
                                        </tr>
                                    </thead>
                                    <tbody class="material_details"></tbody>
                                </table>
                            </div>
                            <div class="row form-group">
                                <label class="col-sm-3 col-form-label">Settlement Inventory</label>
                            </div>
                            <div class="row form-group">
                                <table class="table table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Material Number</th>
                                            <th>Purchase Code</th>
                                            <th>Quantity</th>
                                        </tr>
                                    </thead>
                                    <tbody class="settlementTable"></tbody>
                                </table>
                            </div>
                            <div class="row form-group po_details_report">
                                <div class="col-sm-3">
                                    <button type= "submit" class="btn btn-success">Create</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        let selectedValue = '';
        // 1) On GRN Type Change
        $(document).on('change', '.grn_type', function() {
            selectedValue = $(this).val();
            console.log('Selected Value = ', selectedValue)
            if(selectedValue == 100) {
                $('.po_details').show();
            }
            else {
                $('.po_details').hide();
            }
        })

        // 2) On search Purchase order number
        $(document).on('click', '.search_purchase_order', function() {
            $('.loading').show();
            if($('.po_number').val() !== '') {
                $.ajax({
                    type: "POST",
                    url: "{{ url('/admin/purchase/order') }}",
                    data: { code: $('.po_number').val(), _token: "{{ csrf_token() }}" },
                    success: function(record) {
                        let content = '';
                        if(record.status) {
                            $('.po_number_detail').val(record.data.code);
                            $('.po_amount_detail').val(record.data.total_in_pkr);
                            $('.container_detail').val(record.data.container_item_details.container_details.code)
                            $('.container_amount_detail').val(record.container_amount)
                            $.each( record.data.purchase_order_item_details, function( index, value ){
                                content += '<tr><td>'+value.material_detail.code+'</td><td>'+record.data.code+'</td><td>'+record.data.container_item_details.container_details.code+'</td><td>'+value.quantity+'</td><td><input type="number" class="form-control col-sm-2" name="recievedQty[]" value="" required><input type="hidden" class="form-control col-sm-2" name="materialId[]" value="'+value.material_detail.id+'" required><input type="hidden" class="form-control col-sm-2" name="qty[]" value="'+value.quantity+'" required><input type="hidden" class="form-control col-sm-2" name="materialCode[]" value="'+value.material_detail.code+'" required></td><td><select class="form-control" name="settlement_type[]"><option value="0">No Settlement</option><option value="1">Settlement</option></select></td></tr>' 
                            });
                            /*----------  Section for Settlement Information  ----------*/
                            console.log(record.data.container_item_details.container_details.get_settlement_options)
                            if(record.data.container_item_details.container_details.get_settlement_options){
                                let settlementContent = ''
                                if(record.data.container_item_details.container_details.get_settlement_options.length > 0){
                                    $.each( record.data.container_item_details.container_details.get_settlement_options, function( index, valueSettlement ){
                                        console.log(valueSettlement)
                                        settlementContent += '<tr><td>'+valueSettlement.material_detail.code+'</td><td>'+valueSettlement.purchase_order_detail.code+'</td><td>'+valueSettlement.quantity+'</td></tr>' 
                                    });
                                    $('.settlementTable').html(settlementContent)
                                }
                            }
                            /*----------  End Section for Settlement Information  ----------*/
                            $('.po_details_report').show()
                            $('.material_details').html(content)

                            $('.loading').hide();
                        }
                        else {
                            $('.po_details_report').hide()
                            $('.loading').hide();
                            alert('Please provide valid purchase order');
                        }
                    }
                });
            }
            else {
                alert('Please add purchase order number')
            }
        })
    })
</script>
@endsection
