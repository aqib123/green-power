@extends('admin.layouts.base')
@section('content')
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Add User</h3>
                </div>
                <div class="panel-body">
                    <form action="{{ route('add.user') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" name="name" value="" class="form-control" placeholder="Enter name" />
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-9">
                                <input type="text" name="email" value="" class="form-control" placeholder="Enter email" />
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Phone number</label>
                            <div class="col-sm-9">
                                <input type="text" name="phone" value="" class="form-control" placeholder="Enter phone number" />
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Password</label>
                            <div class="col-sm-9">
                                <input type="password" name="password" value="" class="form-control" placeholder="Enter Password" />
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Confirm Password</label>
                            <div class="col-sm-9">
                                <input type="password" name="password_confirmation" value="" class="form-control" placeholder="Enter Password" />
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-sm-2 col-form-label">Role Type</label>
                            <div class="col-sm-9">
                                <select name="role" value="" class="form-control">
                                    <option value="0">Select Role</option>
                                    @foreach($roles as $role)
                                        <option value="{{ Crypt::encryptString($role->id) }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-11">
                                <button type="submit" class="btn btn-primary float-right offset-sm-2">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
