@extends('admin.layouts.base')
@section('css')
    <link rel="stylesheet" href="{{ asset('datatable/datatables.min.css') }}">
@endsection
@section('content')
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            <!-- BASIC TABLE -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Role List</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive-sm table-bordered table-striped table-sm">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Created By</th>
                            <th>Created_at</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{--@foreach($roles as $key => $role)
                            <tr>
                                <td>{{$role['id']}}</td>
                                <td>
                                    {{ucwords($role['name'])}}
                                </td>
                                <td>{{ $role['user']['name'] }}</td>
                                <td>{!! $role['created_at'] !!}</td>
                                <td>
                                    @if(in_array ($role['id'], [1,2,3,4]) != true)
                                    <div class="btn-group" role="group">
                                        <button id="btnGroupDrop1" type="button" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Action
                                        </button>
                                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                <a href="{{ route('edit.role', ['id' => \Illuminate\Support\Facades\Crypt::encryptString($role['id'])]) }}" class="dropdown-item"><i class="fa fa-edit"></i>Edit</a>
                                                <a href="{{ route('delete.role', ['id' => \Illuminate\Support\Facades\Crypt::encryptString($role['id'])]) }}" class="dropdown-item delete_btn"><i class="fa fa-trash"></i>Delete</a>
                                            </div>
                                    </div>
                                    @else
                                    <lable class="badge badge-danger">Can not Change</lable>
                                        @endif

                                </td>
                            </tr>
                        @endforeach--}}
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END BASIC TABLE -->
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src="{{ asset('datatable/datatables.min.js') }}"></script>
    <script type="text/javascript">
        $(function ($) {
            $(document).ready(function ($) {
                var result = [];
                $('.table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url:"{{ route('role.datatable.request') }}",
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                        },
                    },
                    columns: [

                        { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
                        { data: "name", name: 'name' },
                        { data: "user.name", name: 'user.name'},
                        { data: "created_at", name: 'created_at'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
		$.fn.dataTable.ext.errMode = 'none';
            });
        });
    </script>
@endsection
