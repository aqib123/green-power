@extends('admin.layouts.base')
@section('content')
<div class="container-fluid">
    <h3 class="page-title"></h3>
    <div class="row">
        <div class="col-md-12">
            <!-- BASIC TABLE -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Manage Role</h3>
                </div>
                <div class="panel-body">
                    <form action="{{ route('add.role') }}" method="POST">
                        <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ empty($role_info) ? '' : \Illuminate\Support\Facades\Crypt::encryptString($role_info['id']) }}" />
                        <div class="row form-group">
                            <div class="col-sm-2">
                                <label class="form_label" for="userName">Role name</label>
                            </div>
                            <div class="col-sm-9">
                                <input name="name" class="form-control" type="text" value="{{$role_info['name']??''}}" {{ (isset($role_info['id']) && in_array($role_info['id'], [\App\Setting\UserRole::Admin,\App\Setting\UserRole::Manager] )) ? 'readonly' : '' }}>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-4">
                                <label>Screen Permissions</label>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-8">
                                <div class="bottom_border add_user_role_detail m-b-10">
                                    <div class="card-box add_user_role_box m-b-10">
                                        <dl class="row bottom_border m-b-10">
                                            <dt class="col-sm-2 text-capitalize">add</dt>
                                            <dt class="col-sm-2 text-capitalize">edit</dt>
                                            <dt class="col-sm-2 text-capitalize">delete</dt>
                                            <dt class="col-sm-2 text-capitalize">view</dt>
                                            <dt class="col-sm-2 text-capitalize">screens</dt>
                                        </dl>
                                        @php $i=0; @endphp
                                        @foreach($screens as $key => $value)
                                            <dl class="row bottom_border m-b-10">
                                                <dt class="col-sm-2">
                                                    <span class=" m-b-0 m-t-10">
                                                        <div class="checkbox checkbox-success checkbox-single">
                                                            <input id="leasing.{{$i}}" type="checkbox" name="permissions[<?php echo $value['code']; ?>][add]"
                                                                    @php
                                                                        if(isset($roles_details[$value['code']])){
                                                                            if(isset($roles_details[$value['code']]['add'])){
                                                                                echo 'checked';
                                                                            }
                                                                        }
                                                                    @endphp
                                                            >
                                                            <label for="leasing.{{$i}}"></label>
                                                        </div>
                                                    </span>
                                                </dt>

                                                <dt class="col-sm-2">
                                                    <span class=" m-b-0 m-t-10">
                                                        <div class="checkbox checkbox-success checkbox-single">
                                                            <input id="leasing.{{$i + 2}}" type="checkbox" name="permissions[@php echo $value['code']; @endphp][edit]"
                                                                    @php
                                                                        if(isset($roles_details[$value['code']])){
                                                                            if(isset($roles_details[$value['code']]['edit'])){
                                                                                echo 'checked';
                                                                            }
                                                                        }
                                                                    @endphp
                                                            >
                                                            <label for="leasing.{{$i + 2}}"></label>
                                                        </div>
                                                    </span>
                                                </dt>

                                                <dt class="col-sm-2">
                                                    <span class=" m-b-0 m-t-10">
                                                        <div class="checkbox checkbox-success checkbox-single">
                                                            <input id="leasing.{{$i + 3}}" type="checkbox" name="permissions[<?php echo $value['code']; ?>][delete]"
                                                                    @php
                                                                        if(isset($roles_details[$value['code']])){
                                                                            if(isset($roles_details[$value['code']]['delete'])){
                                                                                echo 'checked';
                                                                            }
                                                                        }
                                                                    @endphp
                                                            >
                                                            <label for="leasing.{{$i + 3}}"></label>
                                                        </div>
                                                    </span>
                                                </dt>

                                                <dt class="col-sm-2">
                                                    <span class=" m-b-0 m-t-10">
                                                        <div class="checkbox checkbox-success checkbox-single">
                                                            <input id="leasing.{{$i + 1}}" type="checkbox" name="permissions[<?php echo $value['code']; ?>][view]" @php
                                                                if(isset($roles_details[$value['code']])){
                                                                    if(isset($roles_details[$value['code']]['view'])){
                                                                        echo 'checked';
                                                                    }
                                                                }
                                                            @endphp
                                                            >
                                                            <label for="leasing.{{$i + 1}}"></label>
                                                        </div>
                                                    </span>
                                                </dt>

                                                <dd class="col-sm-4">{{ $value['name'] }}</dd>
                                            </dl>
                                            @php $i++; @endphp
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-12">
                                @if(isset($role_info['id']))
                                    <a href="{{ route('role.list') }}" class="btn btn-primary float-right m-sm-1">Cancel</a>
                                @endif
                                <button type="submit" class="btn btn-primary float-right m-sm-1">{{ (isset($role_info['id'])) ? 'Upade' : 'Save' }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END BASIC TABLE -->
        </div>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript">
        $(function ($) {
            $(document).ready(function ($) {

                $("input[name*='[edit]'], input[name*='[add]'], input[name*='[delete]']").change(function () {

                    if ($(this).is(':checked') == true)
                    {
                        if ($(this).closest(".row").find("input[name*='[view]']").is(':checked') == false)
                        {
                            $(this).closest(".row").find("input[name*='[view]']").prop('checked', true);
                        }
                    }
                });

                $("input[name*='[view]']").change(function () {
                    if ($(this).is(':checked') == false)
                    {
                        $(this).closest(".row").find("input[name*='[edit]'], input[name*='[add]'], input[name*='[delete]']").prop('checked', false);
                    }
                });
            });
        });
    </script>
@endsection