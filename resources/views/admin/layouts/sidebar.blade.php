@inject('GeneralFunctions', 'App\Role')
<div id="sidebar-nav" class="sidebar">
	<div class="sidebar-scroll">
		<nav>
			<ul class="nav">
				@if($GeneralFunctions->checkViewPermission('dashboard'))
				<li><a href="{{url('admin/dashboard')}}" class="active"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
				@endif
				@if($GeneralFunctions->checkViewPermission('tutorial.request'))
				<li><a href="{{url('admin/company/list')}}" class=""><i class="lnr lnr-cog"></i> <span>Company Details</span></a></li>
				@endif
				@if($GeneralFunctions->checkViewPermission('tutorial.request'))
				<li><a href="{{url('admin/brand/list')}}" class=""><i class="lnr lnr-cog"></i> <span>Brand Details</span></a></li>
				@endif
				@if($GeneralFunctions->checkViewPermission('tutorial.request'))
				<li><a href="{{url('admin/fgtype/list')}}" class=""><i class="lnr lnr-cog"></i> <span>FG Types Details</span></a></li>
				@endif
				@if($GeneralFunctions->checkViewPermission('parents.list'))
				<li><a href="{{url('admin/costcenter/list')}}" class=""><i class="lnr lnr-chart-bars"></i> <span>Cost Center Details</span></a></li>
				@endif
				@if($GeneralFunctions->checkViewPermission('purchase.list'))
				<li>
					<a href="#purchasePage" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Purchases</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="purchasePage" class="collapse ">
						<ul class="nav">
							<li><a href="{{url('admin/vendor/list')}}" class="">Vendor Details</a></li>
							<li><a href="{{url('admin/material/list')}}" class="">Meterial Details</a></li>
							<li><a href="{{url('admin/container/list')}}" class="">Container List</a></li>
							<li><a href="{{url('admin/purchase/order/list')}}" class="">Purchase Order Details</a></li>
						</ul>
					</div>
				</li>
				@endif
				@if($GeneralFunctions->checkViewPermission('purchase.list'))
				<li>
					<a href="#inventoryPage" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Inventory</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="inventoryPage" class="collapse">
						<ul class="nav">
							<li><a href="{{url('admin/grn/list')}}" class="">GRN Details</a></li>
							<li><a href="{{url('admin/inventory/warehouse/detail')}}" class="">Inventory Warehouse details</a></li>
							<li><a href="{{url('admin/settlement/list')}}" class="">Material Settlement List</a></li>
						</ul>
					</div>
				</li>
				@endif
				@if($GeneralFunctions->checkViewPermission('roles.list'))
				<li>
					<a href="#rolesPage" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Roles</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="rolesPage" class="collapse ">
						<ul class="nav">
							<li><a href="{{url('admin/add/role')}}" class="">Add Role</a></li>
							<li><a href="{{url('admin/role/list')}}" class="">Roles List</a></li>
						</ul>
					</div>
				</li>
				@endif
				@if($GeneralFunctions->checkViewPermission('user.list'))
				<li>
					<a href="#usersPage" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Users</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
					<div id="usersPage" class="collapse ">
						<ul class="nav">
							<li><a href="{{url('admin/add/user')}}" class="">Add User</a></li>
							<li><a href="{{url('admin/user/list')}}" class="">User List</a></li>
						</ul>
					</div>
				</li>
				@endif
				@if($GeneralFunctions->checkViewPermission('dashboard'))
				<li><a href="{{url('admin/glaccount/report')}}" class="active"><i class="lnr lnr-chart-bars"></i> <span>GL Account Report</span></a></li>
				@endif
			</ul>
		</nav>
	</div>
</div>