@extends('website.layouts.base')
@section('content')
<input type="hidden" value="PK" id='hiddenchekuae'><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<input type="hidden" value="mtsaqiby236" id='hiddencp_code'>

<div id="fb-root"></div>

<div id="content" class=""    style="width: 100%;">

    <style>
    .referral_code_div {
        width: 75%;
        min-height: 184px;
        display: block;
        margin: 0 auto;
        background-color: white;
        padding: 10px;
        border: 2px solid cyan;
    }

    .class_1 {
        max-width: 100%;
        margin-top: 61px;
        background: #f2f2f2;
        margin-left: 71px;
        text-align: center;
        padding: 3%;
    }



    .class_2 {

        max-width: 100%;

        margin-top: 61px;

        margin-left: 71px;

        padding: 2%;

        margin-top: 5%;

        margin-left: 70px;

        border: 2px solid #2bd8ca;

        padding: 5%;

    }

    .field_area {
        width: 100%;
        display: inline-block;
    }

    .field_1 {
        width: 70%;
        float: left;
    }

    .field_2 {
        width: 30%;
        float: left;
    }

    .field_3 {
        width: 100%;
    }

    .field_4 {
        width: 100%;
    }

    .field_4a {
        width: 49%;
        float: left;
        margin-top: 8px;
    }

    .field_4b {
        width: 2%;
        float: left;
    }

    .field_4c {
        width: 49%;
        float: left;
        margin-top: 8px;
    }

    .field_5 {
        width: 100%;
    }

    .field_5a {
        width: 20%;
        float: left;
    }

    .field_5b {
        width: 20%;
        float: left;
        margin-left: 5px;
    }

    .class_3 {
        max-width: 100%;
        margin-left: 71px;
        text-align: center;
        padding: 3% 1%;
    }

    .field_area {
        width: 100%;
        display: inline-block;
    }

    .field_6 {
        width: 100%;
        display: inline-block;
    }

    .field_7 {
        width: 100%;
        display: inline-block;
    }

    .field_7a {
        width: 32%;
        float: left;
        padding: 5px 10px;
    }

    .field_7b {
        width: 32%;
        float: left;
        padding: 5px 10px;
    }

    .field_7c {
        width: 32%;
        float: right;
        padding: 5px 10px;
    }

    .btn-send-email {
        -webkit-border-top-left-radius: 0;
        -moz-border-radius-topleft: 0;
        border-top-left-radius: 0;
        -webkit-border-bottom-left-radius: 0;
        -moz-border-radius-bottomleft: 0;
        border-bottom-left-radius: 0;
    }

    .btn-font-normal {
        font-weight: normal;
    }

    [disabled].btn,
    fieldset[disabled] .btn {
        pointer-events: none;
    }

    .disabled.btn,
    [disabled].btn,
    fieldset[disabled] .btn {
        opacity: 0.35;
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=35)";
        filter: alpha(opacity=35);
        cursor: default;
    }

    .btn-block,
    .btnsharebnts {
        display: block;
        white-space: normal;
        width: 100%;
    }

    .btn-primary,
    .btnsharebnts {
        border-color: #ff5a5f;
        background-color: #ff5a5f;
        color: #fff;
    }

    .btn,
    .btnsharebnts {
        border-color: #c4c4c4;
        background: white;
        color: #484848;
    }

    .btn-large,
    .btnsharebnts {
        padding: 9px 27px;
        font-size: 16px;
    }

    .btn,
    .btnsharebnts {
        padding: 7px 21px;
        font-size: 14px;
    }

    .btn {
        display: inline-block;
        margin-bottom: 0;
        border: 1px solid;
        text-align: center;
        vertical-align: middle;
        font-weight: bold;
        line-height: 1.73;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        white-space: nowrap;
        cursor: pointer;
    }

    .btnsharebnts {
        display: inline-block;
        margin-bottom: 0;
        border: 1px solid;
        text-align: center;
        vertical-align: middle;
        font-weight: bold;
        line-height: 1.73;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        white-space: nowrap;
        cursor: pointer;
    }

    a {
        color: #008489;
        text-decoration: none;
    }

    .input_field_class {
        width: 100% !important;
    }

    .btn-primary2 {
        border-color: #ff5a5f;
        background-color: #ff5a5f;
        color: #fff;
    }

    .btn-primary2 span:hover {
        color: #FFF;
    }

    .field_3 span {
        margin-left: 30px;
    }

    .gmail-icon {
        background: url(/make_payment/gmail-64.png) left center no-repeat;
        -moz-background-size: contain;
        background-size: contain;
    }

    .windowslive-icon {
        background: url(/make_payment/Outlook-64.png) left center no-repeat;
        -moz-background-size: contain;
        background-size: contain;
    }

    .yahoo-icon {
        background: url(/make_payment/yahoo-64.png) left center no-repeat;
        -moz-background-size: contain;
        background-size: contain;
    }

    h3,
    p {
        color: black;
    }

    footer h3,
    footer p {
        color: #999999;
    }

    p.hide {
        display: none;
    }

    .fb_a_two {
        margin-left: 28px;
    }

    .fb_a_one {
        margin-left: 10px;
        margin-top: 3px;
    }



    @media only screen and (max-width: 767px) {

        p.hide {
            display: block !important;
        }

        .fb_a_two {
            margin-left: 28px !important;
        }

        .fb_a_one {
            margin-left: 60px !important;
        }

        .field_5a p {
            text-align: center !important;
        }

        .hide-sm {
            display: none !important
        }

        .field_7c {
            width: 100% !important;
        }
    }

    #standalone {
        -webkit-transform: scale(0.8);
        -moz-transform: scale(0.8);
        -ms-transform: scale(0.8);
        transform: scale(0.8);
    }

    .popup_visible #standalone {
        -webkit-transform: scale(1);
        -moz-transform: scale(1);
        -ms-transform: scale(1);
        transform: scale(1);
    }

    #standalone {
        padding: 10px;
    }

    /* Styling testimonial author */

    .fusion-testimonials.clean .author {
        font-size: 12px;
        line-height: 18px;
        letter-spacing: 2px;
    }

    @media only screen and (max-width: 767px) {
        .float_fifty {
            width: 100% !important;
            margin: 10px 0px !important;
        }

        .fusion-layout-column {
            margin-left: 0px !important;
        }

        .class_1,
        .class_2,
        .class_3 {
            margin-left: 0px !important;
            margin-top: 20px !important;
        }

        .area_1,
        .area_2,
        .bottom_box {
            width: 100% !important;
        }

        .wpcf7-form-control-wrap input,
        select.wpcf7-form-control,
        .wpcf7-form input[type="submit"],

        a.cusombtn,
        .field_7a,
        .field_7b {
            width: 100% !important;
        }

        a.cusombtn {
            text-align: center;
        }

        p.hide {
            dispaly: block;
        }



    }

    @media only screen and (max-width:480px) {

        .innerdiv a.cusombtn {
            display: block !important;
            text-align: center;
            margin-bottom: 10px;
        }

        .float_fifty,
        .field_2,
        .field_1 {
            width: 100% !important;
        }

        .field_5b,
        .field_5a {
            width: 100% !important;
            margin-bottom: 10px;
            margin-left: 0px !important;
        }

        .field_1 {
            margin-bottom: 10px;
        }

        .field_3 span.fa-gmail {
            display: block;
            text-align: center;
            margin-bottom: 10px;
            font-weight: bold;
        }
    }

    .btn-save {
        width: 150px;
        height: 30px;
        margin: 10px;
        background-color: #3F47CC;
        border: 0;
        background: #3F47CC;
        box-shadow: none;
        border-radius: 0px;
        color: #fff;
    }
    .unbold {
    font-weight: 300 !important;
    }
    .tooltip-inner {
    max-width: 100% !important;
    text-align:left !important;
    }
    </style>

    <link href="/make_payment/updated_custom_css.css" rel="stylesheet" type="text/css" />
    <div class="fusion-three-fourth fusion-layout-column fusion-spacing-yes" style="width: 100%">
        <div class="fusion-column-wrapper">
            <div class="fusion-sep-clear"></div>
            <div class="fusion-row ">
                <div class="fusion-clearfix"></div>
                <div class="fusion-row class_1">

                    <h3 class="main_headings unbold">Become a Brand Ambassador and earn upto <strong>$5000/month</strong></h3>
                    <p>Earn upto <span style="color:red;font-weight:bold;">15%</span> revenue share from all your referred friends and families into your Bank Account! <br> Read
                        the <a href='https://www.mytutorsource.com/mts-referral-program-terms-conditions/'
                            target="_blank" style='color:blue; text-decoration:underline;'>terms</a></p>
                    <div class="referral_code_div">
                        <div class="">
                            <span>
                            <img src="https://www.mytutorsource.com/make_payment/badge_bronze.png" width="30" /> <strong style="color:black;font-size: 16px;">Bronze Ambassador</strong>
                                (Your Current Status)</span> <i
                                data-toggle="tooltip" data-html="true" data-placement="bottom" title="List of Ambassador Statuses:<br><br>

Bronze Ambassador (10% Revenue Share on each referred student):<br>
Default Ambassador Status of anyone signing up on MTS for the first time is “Bronze”. Bronze Ambassador gets a revenue share of 10% on each payment made by each one of their referred students<br>
<br>
Silver Ambassador (12% Revenue Share on each referred student):<br>
When an Ambassador has successfully referred more than 10 students to MTS, his/her Ambassador status is upgraded to “Silver”. Silver Ambassador gets a revenue share of 12% on each payment made by each one of their referred students (new and existing).<br>
<br>
Gold Ambassador (15% Revenue Share on each referred student):<br>
When an Ambassador has successfully referred more than 25 students to MTS, his/her Ambassador status is upgraded to “Gold”. Gold Ambassador gets a revenue share of 15% on each payment made by each one of their referred students (new and existing).<br>
<br>"
                                class="fa fa-question-circle" aria-hidden="true" style="color:#38A94D;"></i> </div>
                                                <div class="">Your initial signup (BONUS):
                            <strong>$ 15.00</strong> <i
                                data-toggle="tooltip" data-placement="bottom" title="Along with the percentage commission from your first referred customer to MTS, a bonus will also be transferred to your PayPal/Skrill account. This bonus may only be transferred into your PayPal/Skrill account if you have successfully referred at least one customer."
                                class="fa fa-question-circle" aria-hidden="true" style="color:#38A94D;"></i></div>
                                                                        <div class="">Your Referral Earning:
                            <strong>$ 0.00</strong> <i
                                data-toggle="tooltip" data-placement="bottom" title="This is the current total payable commission amount which is available in your MTS wallet account. When your refer a customer to MTS for tutoring services and that customer makes a purchase from MTS, a certain percentage of that purchase (10 to 15%, depending on your current Ambassador Status) is transferred automatically into your MTS wallet account. Every month, this amount will be transferred directly into your PayPal/Skrill account."
                                class="fa fa-question-circle" aria-hidden="true" style="color:#38A94D;"></i></div>
                                                <div class="">Total Number of Reffered Students:
                            <strong>0</strong></div>
                        <h3 class="unbold">Your Family Referral Code: <span
                                style="font-weight: bold;color: green;">mtsaqiby236</span></h3>
                    </div>
                </div>
            </div>
            
            <div class="fusion-row">
                <div class="fusion-clearfix"></div>
                <div class="field_6" style="margin-top:60px;">
                        <h3 class="main_headings">Refer Families/Friends to Earn Money</h3>
                </div>
                <div class="fusion-row class_2" style="margin-top:1%;">
                    <div class="field_area">
                        <div class="field_1">
                            <input type="text" style="width:100% !important;" id='sendemailcontacts' autocomplete="none"
                                placeholder="Enter email addresses" spellcheck="false"></div>
                        <div class="field_2"><a href="javascript:void(0)"
                                class="btn btn-block btn-large btn-font-normal btn-send-email btn-primary2"
                                disabled="" id="btn-send-email"><span id='sendcustomemail'>Send Invites</span></a> <img
                                src='/make_payment/495.gif' id='tickmark_image2'
                                style="width: 27px;display: none;margin-left: 20px;display: inline-block;margin-top: 4px; display: none"><span
                                id="email_sent_message"
                                style='display:none; color:green;color: green;margin-left: 24%;width: 100%;'><br>Email
                                sent successfully! </span>
                        </div>
                    </div>
                    <div class="field_area">
                        <div class="field_4">
                            <div class="field_4a">
                                <hr />
                            </div>
                            <div class="field_4b"> or </div>
                            <div class="field_4c">
                                <hr />
                            </div>
                        </div>
                    </div>
                    <script>
                    window.fbAsyncInit = function() {

                        FB.init({

                            appId: '164226164284967',
                            status: true,
                            cookie: true,
                            xfbml: true
                        });

                    };

                    (function(d, debug) {
                        var js, id = 'facebook-jssdk',
                            ref = d.getElementsByTagName('script')[0];
                        if (d.getElementById(id)) {
                            return;
                        }
                        js = d.createElement('script');
                        js.id = id;
                        js.async = true;
                        js.src = "//connect.facebook.net/en_US/all" + (debug ? "/debug" : "") + ".js";
                        ref.parentNode.insertBefore(js, ref);
                    }(document, /*debug*/ false));

                    function postToFeed(title, desc, url, image) {
                        var obj = {
                            method: 'feed',
                            link: url,
                            picture: 'https://www.mytutorsource.com/wp-content/uploads/2015/11/logo_031.png',
                            name: 'MTS',
                            description: 'MTS Tutor'
                        };

                        function callback(response) {}
                        FB.ui(obj, callback);

                    }
                    </script>
                    

                    <div class="field_area">

                        <div class="field_5">
                            <div class="field_5a" style='width: 50%;'>
                                <p style="margin-top: 4px;text-align: left;">Share your Referral Code: <span
                                        style="color:green;font-weight: bold">mtsaqiby236</span></p>
                            </div>

                            <div class="field_5a"
                                style='border-color: #3A5BA0;background-color: #3A5BA0;color: #fff;    height: 40px;padding-top: 5px;'>
                               <!--  <a href="https://www.mytutorsource.com/referral-welcome?rcode=mtsaqiby236"
                                    data-image="article-1.jpg" data-title="Article Title" data-desc="Desc 2"
                                    class="btnShare"
                                    style='border-color: #3A5BA0;background-color: #3A5BA0;color: #fff;'>
                                    <span class='fb_a_one'><img src='/make_payment/3-20.png'
                                            style='margin-top: 3px;'></span>
                                    <span class='fb_a_two'>Facebook</span></a> -->
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v5.0&appId=164226164284967&autoLogAppEvents=1"></script>


<div class="fb-share-button" data-href="https://www.mytutorsource.com/sharefacebook/?c=mtsaqiby236-Aqib-Yaqoob-118" data-layout="button_count" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.mytutorsource.com%2F%27&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Facebook</a></div>

           <span class='fb_a_one' ><img src='/make_payment/3-20.png' style='margin-top: 3px;'></span>
           <span class='fb_a_two'>Facebook</span></a>
                            </div>
                            <div class="field_5b" style=''>
                             <!--    <a class="btn btn-large btn-block social-share-btn btn-font-normal btn-facebook-messenger hide-sm"
                                    data-network="messenger"
                                    data-referral-link="https://www.mytutorsource.com/referral-welcome?rcode=mtsaqiby236"
                                    rel="nofollow noopener noreferrer"
                                    href="http://www.facebook.com/dialog/send?app_id=164226164284967&amp;link=https://www.mytutorsource.com/referral-welcome?rcode=mtsaqiby236&amp;redirect_uri=https://www.mytutorsource.com/get-10-discount/"
                                    target="_blank"
                                    style='border-color: #007FFF;background-color: #007FFF;color: #FFF;'><span
                                        class="hide-md"><span style='margin-left: -24px;margin-top: 3px;'><img
                                                src='/make_payment/if_messenger_3069715.png'
                                                style='margin-top: 3px;'></span><span
                                            style='margin-left: 28px;'>Messenger</span></a> -->
                                            <a class="btn btn-large btn-block social-share-btn btn-font-normal btn-facebook-messenger hide-sm" data-network="messenger" data-referral-link="https://www.mytutorsource.com/sharefacebook/?c=mtsaqiby236-Aqib-Yaqoob-118" rel="nofollow noopener noreferrer" href="http://www.facebook.com/dialog/send?app_id=164226164284967&amp;link=https://www.mytutorsource.com/sharefacebook/?c=mtsaqiby236-Aqib-Yaqoob-118&amp;redirect_uri=https://www.mytutorsource.com/sharefacebook/?c=mtsaqiby236-Aqib-Yaqoob-118" target="_blank" style='border-color: #007FFF;background-color: #007FFF;color: #FFF;' id="btnMessenger_share"><span class="hide-md"><span  style='margin-left: -24px;margin-top: 3px;'><img src='/make_payment/if_messenger_3069715.png' style='margin-top: 3px;'></span><span style='margin-left: 28px;'>Messenger</span></a>
                                <p class="hide"><a
                                        style="padding: 5px;text-align: center;width: 100%;font-size: 14px;display: inline-block;color: #484848;line-height: 28px;font-weight: bold;border: 1px solid;"
                                        href="sms:?&body=Looking for High Quality Tutoring Classes? 

 Use Aqib's referral code: mtsaqiby236 for Tutoring Classes with MTS! MTS provides tutors for GCSE, A Level, IB and High School Subjects. For more information go to: https://www.mytutorsource.com/referral-welcome?rcode=mtsaqiby236" id="btnSms_share"><i
                                            class="fa fa-phone"></i> Text Message</a></p>
                                <p class="hide" id='whatsppbtn'>
                                    <a style="padding: 5px;text-align: center;width: 100%;font-size: 14px;display: inline-block;color: #FFF;line-height: 28px;font-weight: bold;background: #2FB518;"
                                        href="

javascript:void(0);" title="Share on WhatsApp" class="btn-whatsapp initialism standalone_open btn btn-success" id="btnWhatsapp_share"><i
                                            class="fa fa-whatsapp"></i> <span>Whatsapp</span></a>

                                </p>
                                <div id="standalone">
                                    <h3 data-fontsize="16" data-lineheight="30" style="text-align: center;">Whatsapp
                                        Message</h3>

                                    <p id='whatsapp_textarea'>

                                        <textarea id='whatsapp_textarea2'
                                            style="width: 100% !important;padding: 10px;margin-bottom: 5px;border: 1px solid;height: 150px;">Looking for High Quality Tutoring Classes? 

Use Aqib's referral code: mtsaqiby236 for Tutoring Classes with MTS! MTS provides tutors for GCSE, A Level, IB and High School Subjects. For more information go to: https://www.mytutorsource.com/referral-welcome?rcode=mtsaqiby236 </textarea>

                                        <a style="padding: 5px;text-align: center;width: 100%;font-size: 14px;display: inline-block;color: #FFF;line-height: 40px;font-weight: bold;background:#2FB518;"
                                            href="

javascript:void(0);" title="Share on WhatsApp" class="btn-whatsapp" id="whatsppbtn_send"><i class="fa fa-whatsapp"></i>
                                            <span id=''>Select Contacts</span></a>

                                        <a style="padding: 5px;text-align: center;width: 100%;font-size: 14px;display: inline-block;color: #FFF;    margin-top: 5px;line-height: 40px;font-weight: bold;background:darkgray;"
                                            href="

javascript:void(0);" title="Share on WhatsApp" class="btn-whatsapp standalone_close"> <span>Close</span></a>

                                    </p>
                                    </p>
                                </div>
                            </div>
                            <style type="text/css">
                            .inlineBlock {
                                width: 177px !important;
                                height: 40px !important;
                                border-radius: 0 !important;
                            }
                            </style>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="fusion-row">

            <div class="fusion-clearfix"></div>

            <div class="fusion-row class_3" style="padding:3% 0;">
                <div class="field_area">

                    <div class="field_6" style="margin-top:60px;">

                        <h3 class="main_headings">How to get Money into your Account</h3>

                    </div>

                    <div class="field_7" style="border: 2px solid #2DAB53;margin-top: 1%;">

                        <div class="field_7a">
                            <h3>Step 1</h3>
                            <p><img src='/make_payment/Invite Your Friends To Airbnb.png' style='width: 28px;'></p>
                            <p style="text-align: left;">Share your family referral code with friends or simply send
                                invites to them through Messenger/Email or WhatsApp.</p>
                        </div>

                        <div class="field_7b">
                            <h3>Step 2</h3>
                            <p><img src='/make_payment/Invite Your Friends To Airbnb (1).png'></p>
                            <p style="text-align: left;">As soon as your referred student pays for the first set of
                                tutoring classes (using your Family referral code), you will get a certain percentage
                                (10 to 15%, depending on your current Ambassador Status) from the total amount given by
                                the referred student. This commission amount will start showing in your MTS Wallet
                                Account credit.</p>
                        </div>

                        <div class="field_7c">
                            <h3>Step 3</h3>
                            <p><img src='/make_payment/dollar-sign-dollar-symbol-signs-icons-1.png' width="29"></p>
                            <p style="text-align: left;">At the start of each month, the amount available in your MTS
                                wallet account will be disbursed directly into your PayPal/Skrill account provided
                                below.</p>
                        </div>
                        <div class="fusion-clearfix"></div>
                        <br>
                        <div style="display:inline-block;width: 80%;color:black;font-weight:bold;">
                            <div style="width:100%;font-size:16px;font-weight:bold;">Skrill Email ID</div>
                            <div style="width:100%;font-size:16px;font-weight:bold;">(You need to make account on <a href="https://skrill.com" target="_blank">skrill.com</a> first)</div>
                            <br>
                            <div style="width:100%"><input id="skrill_id" type="text" placeholder="Skrill Email ID" value="">
                            </div>
                            <div style="color:red;text-align:center;" id="error_2" class="custom_error"></div>
                            <div style="width:100%; color:green; display:none;" id="skrill_saved">Saved Successfully!</div>
                            <div style="width:100%"><button id="skrill_save" type="button" class="btn-save">Save</button></div>
                        </div>
                    </div>
                    <div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="fusion-clearfix"></div>
</div>
</div>
<input type="hidden" id="whatsaappcounter" value="mtsaqiby236-Aqib-Yaqoob-118">
</div>

<script src="/make_payment/jquery.popupoverlay.js"></script>

<script type='text/javascript'>
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

</script>

<style>
.fusion-footer {
    width: 1110px;

    margin: 0 auto;
}

@media only screen and (max-width: 767px) {

    .fusion-footer {
        width: 100%;

        margin: 0 auto;
    }

}

footer p,
footer h3 {
    color: #999999;
}
</style>
@endsection