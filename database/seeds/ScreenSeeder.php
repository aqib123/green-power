<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class ScreenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $carbon = Carbon::now();
        DB::table('screens_details')->insert([
            [
                'name' => 'Dashboard',
                'code' => 'dashboard',
                'created_at' => $carbon
            ],
            [
                'name' => 'Add Vendors',
                'code' => 'add.vendors',
                'created_at' => $carbon
            ],
            [
                'name' => 'Add Materials',
                'code' => 'add.materials',
                'created_at' => $carbon
            ],
            [
                'name' => 'Add Purchase Orders',
                'code' => 'add.purchaseOrders',
                'created_at' => $carbon
            ],
        ]);

        DB::table('screen_uri')->insert([
            [
                'screen_id' => 1,
                'uri' => '/dashboard'
            ],[
                'screen_id' => 2,
                'uri' => '/add/vendors'
            ],[
                'screen_id' => 3,
                'uri' => '/add/materials'
            ],[
                'screen_id' => 4,
                'uri' => '/add/purchase/orders'
            ],
            ]
        );
    }
}
