<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate(
        [
            'name' => 'Super Admin',
            'email' => 'admin@admin.com',
            'type' => 1,
            'user_type' => 0,
            'active' => 1,
            'role' => 1
        ],
        [
            'password' => Hash::make('password'),
        ]
    );
    }
}
