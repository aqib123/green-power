<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order', function (Blueprint $table) {
            $table->id();
            $table->date('purchase_order_date');
            $table->unsignedBigInteger('vendor_id');
            $table->tinyInteger('status')->default(0)->comment('0 => pending, 1 => cleared');
            $table->integer('code')->comment('5 digit unique code for reporting');
            $table->string('description', 400)->nullable();
            $table->decimal('exchange_rate_yuan', 9, 3);
            $table->decimal('total_in_pkr', 9, 3);
            $table->decimal('total_in_yuan', 9, 3);
            $table->timestamps();
            $table->foreign('vendor_id')->references('id')->on('vendors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order');
    }
}
