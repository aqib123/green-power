<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_inventory', function (Blueprint $table) {
            $table->id();
            $table->integer('storage_location');
            $table->integer('movement_type');
            $table->integer('material_code');
            $table->integer('po_code')->nullable();
            $table->integer('material_document_number')->nullable();
            $table->integer('quantity');
            $table->decimal('rate', 9, 3);
            $table->decimal('amount', 9, 3);
            $table->integer('period');
            $table->integer('company_code')->nullable();
            $table->integer('cost_center_code')->nullable();
            $table->integer('production_order_code')->nullable();
            $table->integer('sales_order_code')->nullable();
            $table->tinyInteger('material_type')->default(0)->comment('0 => Pack, 1 => Other');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_inventory');
    }
}
