<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialSettlementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_settlement', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('purchase_order_id');
            $table->unsignedBigInteger('material_id');
            $table->unsignedBigInteger('container_id');
            $table->integer('purchase_order_code');
            $table->integer('material_code');
            $table->integer('container_code');
            $table->integer('settlement_qty');
            $table->decimal('settlement_rate', 65, 3)->default(0.000);
            $table->integer('recieved_vendor_qty')->nullable();
            $table->tinyInteger('status')->default(0)->comment('0 => incomplete, 1 => Complete');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_settlement');
    }
}
