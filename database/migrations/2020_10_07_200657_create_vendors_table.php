<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->id();
            $table->string('full_name', 200);
            $table->string('phone_number', 50)->nullable();
            $table->string('profile_image', 200)->nullable();
            $table->tinyInteger('vendor_type')->default(0)->comment('0 => local, 1 => international');
            $table->integer('code')->comment('5 digit unique code for reporting');
            $table->string('address', 400)->nullable();
            $table->string('description', 400)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
