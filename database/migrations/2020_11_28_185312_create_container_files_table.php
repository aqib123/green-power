<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContainerFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('container_files', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('conatainer_id');
            $table->string('file_path', 500);
            $table->foreign('conatainer_id')->references('id')->on('container_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('container_files');
    }
}
