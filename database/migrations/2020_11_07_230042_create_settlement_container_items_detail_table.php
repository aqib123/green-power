<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettlementContainerItemsDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settlement_container_items_detail', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('purchase_order_id');
            $table->unsignedBigInteger('material_id');
            $table->integer('quantity');
            $table->decimal('rate_in_pkr',65,3)->default(0.000);
            $table->decimal('total_in_pkr',65,3)->default(0.000);
            $table->tinyInteger('status')->default(0)->comment('0 => pending, 1 => cleared');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settlement_container_items_detail');
    }
}
