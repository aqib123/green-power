<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialAverageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_average', function (Blueprint $table) {
            $table->id();
            $table->integer('material_code');
            $table->integer('opening_qty')->default(0);
            $table->decimal('opening_value', 9, 3)->default(0);
            $table->integer('total_receipt_qty')->default(0);
            $table->decimal('total_receipt_value', 9, 3)->default(0);
            $table->integer('total_issuance_qty')->default(0);
            $table->decimal('total_issuance_value', 9, 3)->default(0);
            $table->integer('closing_position_qty')->default(0);
            $table->decimal('closing_position_value', 9, 3)->default(0);
            $table->decimal('issuance_rate', 9, 3)->default(0);
            $table->integer('period')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_average');
    }
}
