<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMaterialTale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material', function (Blueprint $table) {
            $table->unsignedBigInteger('brand')->default(1);
            $table->string('watt')->nullable();
            $table->unsignedBigInteger('fg_type')->default(1);
            $table->integer('material_varient')->default(0);
            $table->foreign('brand')->references('id')->on('brand');
            $table->foreign('fg_type')->references('id')->on('fg_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
