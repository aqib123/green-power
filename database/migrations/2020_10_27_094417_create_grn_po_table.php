<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrnPoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grn_po', function (Blueprint $table) {
            $table->id();
            $table->integer('po_code');
            $table->unsignedBigInteger('po_id');
            $table->integer('container_code');
            $table->unsignedBigInteger('container_id');
            $table->decimal('container_total_amount', 9, 3)->default(0);
            $table->decimal('container_weighted_amount', 9, 3)->default(0)->comment('This column will have weighted expenses value if PO is more than 1 in Container ');
            $table->integer('grn_type');
            $table->tinyInteger('status')->default(0)->comment('0 => pending, 1 => Complete');
            $table->timestamps();
            $table->foreign('po_id')->references('id')->on('purchase_order');
            $table->foreign('container_id')->references('id')->on('container_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grn_po');
    }
}
