<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->tinyInteger('type')->default(1)->index()->comment = '1 = Administration, 2 = Users';
            $table->string('location',100)->index()->nullable();
            $table->string('phone',100)->nullable();
            $table->tinyInteger('user_type')->default(0)->index()->comment = '0 = Admin, 1 = parent, 2 = student';
            $table->tinyInteger('active')->default(0)->index()->comment = '0 = unverified, 1 = verified';
            $table->string('first_name',100)->index()->nullable();
            $table->string('last_name',100)->index()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
