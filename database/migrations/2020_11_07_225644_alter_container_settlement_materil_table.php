<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterContainerSettlementMaterilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('container_details', function (Blueprint $table) {
            $table->integer('settlement_material')->default(0); // 0 => No settlemnt materila, 1 => Settlemnt materials
            $table->integer('total_settlement_material')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
