<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGlAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gl_account', function (Blueprint $table) {
            $table->id();
            $table->integer('transaction_type');
            $table->integer('gl_code');
            $table->integer('gl_description');
            $table->decimal('amount',9,3);
            $table->integer('company');
            $table->integer('cost_center');
            $table->integer('po_id')->nullable();
            $table->integer('container_id')->nullable();
            $table->integer('period');
            $table->tinyInteger('document_type')->default(0)->comment('1 => D0, 2 => D1');
            $table->integer('document_no');
            $table->date('posting_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gl_account');
    }
}
