<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContainerDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('container_details', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->tinyInteger('status')->default(0)->comment('0 => In Transit, 1 => Complete');
            $table->integer('code')->comment('5 digit unique code for reporting');
            $table->string('description', 400)->nullable();
            $table->integer('no_of_packages');
            $table->string('custom_reference', 200);
            $table->string('cash_no', 200);
            $table->string('igm_no', 200);
            $table->string('vessel', 200);
            $table->string('bill_no', 200);
            $table->decimal('exchange_rate_yuan', 9, 3);
            $table->decimal('personal_expense_in_yuan', 9, 3);
            $table->decimal('shipping_expense_in_yuan', 9, 3);
            $table->decimal('personal_expense_in_pkr', 9, 3);
            $table->decimal('shipping_expense_in_pkr', 9, 3);
            $table->decimal('custom_duty_one', 9, 3)->nullable();
            $table->decimal('custom_duty_two', 9, 3)->nullable();
            $table->decimal('excise_one', 9, 3)->nullable();
            $table->decimal('excise_two', 9, 3)->nullable();
            $table->decimal('do', 9, 3)->nullable();
            $table->decimal('do_charges', 9, 3)->nullable();
            $table->decimal('group_misc', 9, 3)->nullable();
            $table->decimal('examination', 9, 3)->nullable();
            $table->decimal('yard_payment', 9, 3)->nullable();
            $table->decimal('duty', 9, 3)->nullable();
            $table->decimal('token', 9, 3)->nullable();
            $table->decimal('de_blocking', 9, 3)->nullable();
            $table->decimal('counter_ground', 9, 3)->nullable();
            $table->decimal('sepy', 9, 3)->nullable();
            $table->decimal('pdc_charges', 9, 3)->nullable();
            $table->decimal('auction_release', 9, 3)->nullable();
            $table->decimal('agency', 9, 3)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('container_details');
    }
}
