<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrderMaterialItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_material_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('purchase_order_id');
            $table->unsignedBigInteger('material_id');
            $table->integer('quantity');
            $table->decimal('unit_price_in_pkr', 9, 3);
            $table->decimal('unit_price_in_yuan', 9, 3);
            $table->decimal('total_in_pkr', 9, 3);
            $table->decimal('total_in_yuan', 9, 3);
            $table->timestamps();
            $table->foreign('purchase_order_id')->references('id')->on('purchase_order');
            $table->foreign('material_id')->references('id')->on('material');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order_material_items');
    }
}
