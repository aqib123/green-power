<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrnPoMaterialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grn_po_material', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('grn_id');
            $table->unsignedBigInteger('material_id');
            $table->integer('material_code');
            $table->integer('qty');
            $table->integer('recieved_qty');
            $table->decimal('direct_purchase', 9, 3);
            $table->decimal('direct_rate', 9, 3);
            $table->decimal('container_expense', 9, 3);
            $table->decimal('total_amount', 9, 3);
            $table->decimal('revised_rate', 9, 3);
            $table->timestamps();
            $table->foreign('material_id')->references('id')->on('material');
            $table->foreign('grn_id')->references('id')->on('grn_po');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grn_po_material');
    }
}
