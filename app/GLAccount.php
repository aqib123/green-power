<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GLAccount extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transaction_type', 'gl_code', 'gl_description', 'amount', 'company', 'cost_center', 'po_id', 'container_id', 'period', 'document_type', 'document_no', 'posting_date'
    ];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gl_account';
}