<?php

namespace App\Setting;


class Setting
{
    const AccountPending = 0;
    const AccountActive = 1;
    const Assigned = 1;
    const Unassigned = 2;
    const Excel = 2;
    const Form = 1;
    const WorkingDays = 25; // attendance report
    const Daily = 1; // for visit report
    const Monthly = 2; // for visit report
    const Todate = 3; // for visit report
    const Verified = 1; // phone numbers
    const NotVerified = 0; // phone numbers
    const BD = 1; // reason

    public static $status = ['ACT', 'DIS', 'MOC'];
    const Allowed = 1;
    const NotAllowed = 0;
    const IsProfile = 1;
    const NotProfile = 0;
}
