<?php

namespace App\Setting;


class UserRole
{
    const SuperAdmin = 1;
    const Admin = 2;
    const Manager = 3;

    public static function roleName($role = 0)
    {
        switch ($role)
        {
            case self::SuperAdmin:
                return 'Super Admin';
                break;
            case self::Admin:
                return 'Admin';
                break;
            case self::Manager:
                return 'Manager';
                break;
            Default:
                return 'Not Specified';
        }
    }
}