<?php

namespace App\Setting;


class UserPermission
{
    const Add = 1;
    const Edit = 2;
    const Delete = 3;
    const View = 4;
}