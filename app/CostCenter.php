<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostCenter extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name'
    ];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cost_center';
}