<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialInventory extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'storage_location', 'movement_type', 'material_code', 'po_code', 'material_document_number', 'quantity', 'rate', 'amount', 'period', 'company_code', 'cost_center_code', 'production_order_code', 'sales_order_code', 'material_type'
    ];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'material_inventory';
}