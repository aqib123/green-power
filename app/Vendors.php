<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendors extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'phone_number', 'profile_image', 'vendor_type', 'code', 'address', 'description'
    ];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vendors';

    public function getVendorTypeAttribute($value)
    {
    	return $value == 1 ? 'International' : 'Local';
    }
}