<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GRNPurchaseOrder extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'po_code', 'po_id', 'container_code', 'container_id', 'container_total_amount', 'container_weighted_amount', 'grn_type', 'status'
    ];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'grn_po';

    public function grn_order_items_details()
    {
        return $this->hasMany(GRNPurchaseOrderMaterial::class, 'grn_id', 'id');
    }

    public function get_settlement_options()
    {
        return $this->hasMany(SettlementContainerItems::class, 'container_id', 'container_id');
    }
}