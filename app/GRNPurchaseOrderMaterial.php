<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GRNPurchaseOrderMaterial extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'grn_id', 'material_id', 'material_code', 'qty', 'recieved_qty', 'direct_purchase', 'direct_rate', 'container_expense', 'total_amount', 'revised_rate', 'settlement_type', 'settlement_qty', 'settlement_rate'
    ];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'grn_po_material';
}