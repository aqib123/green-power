<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContainerItemDetails extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'purchase_order_id', 'conatainer_id', 'total_in_pkr'
    ];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'container_item_details';

    public function container_details()
    {
        return $this->belongsTo(ContainerDetails::class, 'conatainer_id', 'id');
    }

    public function purchase_order_total()
    {
        return $this->belongsTo(PurchaseOrder::class, 'purchase_order_id', 'id')->select('id', 'total_in_pkr');
    }
}