<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'material_type', 'code', 'description', 'watt', 'fg_type', 'brand', 'material_varient', 'no_of_uom'
    ];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'material';

    public function getMaterialTypeAttribute($value)
    {
    	return $value == 1 ? 'International' : 'Local';
    }

    public function brand()
    {
        return $this->hasOne(Brand::class, 'id', 'brand');
    }

    public function fg_type()
    {
        return $this->hasOne(FGType::class, 'id', 'fg_type');
    }

    public function getMaterialVarientAttribute($value)
    {
        $record = ['Raw', 'Pack', 'Store Items', 'Finish Goods'];
        return $record[$value];
    }

    public function getNoOfUomAttribute($value)
    {
        $record = ['Kg', 'Litre', 'Per Unit', 'Carton'];
        return $record[$value];
    }
}