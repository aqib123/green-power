<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PurchaseOrderMaterialItems;

class PurchaseOrder extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'purchase_order_date', 'vendor_id', 'code', 'description', 'status', 'exchange_rate_yuan', 'total_in_pkr', 'total_in_yuan'
    ];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'purchase_order';

    public function getStatusAttribute($value)
    {
        $statuses = ['Pending', 'In Transit', 'Completed'];
    	return $statuses[$value];
    }

    public function purchase_order_item_details()
    {
        return $this->hasMany(PurchaseOrderMaterialItems::class, 'purchase_order_id', 'id');
    }

    public function container_item_details()
    {
        return $this->belongsTo(ContainerItemDetails::class, 'id', 'purchase_order_id');
    }

    public function vendor()
    {
        return $this->hasOne(Vendors::class, 'id', 'vendor_id');
    }

    public function settlement_container_items()
    {
        return $this->hasMany(SettlementContainerItems::class, 'purchase_order_id', 'id');
    }
}