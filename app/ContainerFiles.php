<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContainerFiles extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file_path', 'conatainer_id'
    ];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'container_files';
}