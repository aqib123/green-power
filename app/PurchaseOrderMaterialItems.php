<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Material;

class PurchaseOrderMaterialItems extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'purchase_order_id', 'material_id', 'quantity', 'unit_price_in_pkr', 'unit_price_in_yuan', 'total_in_pkr', 'total_in_yuan'
    ];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'purchase_order_material_items';

    public function material_detail()
    {
        return $this->hasOne(Material::class, 'id', 'material_id');
    }
}