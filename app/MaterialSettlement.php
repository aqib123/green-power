<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialSettlement extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'purchase_order_id', 'material_id', 'container_id', 'purchase_order_code', 'material_code', 'container_code', 'settlement_qty', 'settlement_rate', 'status'
    ];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'material_settlement';
}