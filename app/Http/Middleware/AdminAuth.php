<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if(Auth::user()->type == 1 && Auth::user()->active == 1 && Auth::user()->user_type == 0){
                return $next($request);
            }
            Auth::logout();
            return redirect('admin/login')->with('error','Permission Denied!!! You do not have access.');
        }
        return redirect('admin/login')->with('error','Permission Denied!!! You do not have access.');
    }
}
