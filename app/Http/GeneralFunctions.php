<?php

namespace App\Http;


use App\Models\Admin\Permission;
use App\Models\Admin\Screens;
use App\Setting\JWTToken;
use App\Setting\UserPermission;
use App\Setting\UserRole;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use phpseclib\Crypt\Random;
use App\GLAccount;

trait GeneralFunctions
{
    /*
     * @return string
     */
    public function GenerateRandomString()
    {
        return bin2hex(random_bytes(JWTToken::Length).Carbon::now()->timestamp);
    }
    public static function checkViewPermission($code)
    {
        return GeneralFunctions::permissions($code, UserPermission::View);
    }

    public static function checkEditPermission($code)
    {
        return GeneralFunctions::permissions($code, UserPermission::Edit);
    }

    public static function checkAddPermission($code)
    {
        return GeneralFunctions::permissions($code, UserPermission::Add);
    }

    public static function checkDeletePermission($code)
    {
        return GeneralFunctions::permissions($code, UserPermission::Delete);
    }

    public static function permissions($code, $permission, $flag = false, $uri = '')
    {
        if ($flag)
        {
            // use uri method
            $screen = DB::table('screen_uri')->where('uri', $uri)->first();
            if (!is_null($screen))
            {
                $rolePermission = DB::table('permission_role')->where('screen_id', $screen->screen_id)->where('role_id', Auth::user()->role)->whereIn('permission_id', $permission)->get();
                if ($rolePermission->count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            // use code method
            if (Auth::user()->role != UserRole::SuperAdmin)
            {
                $getScreenPermission = DB::table('screens_details')->where('code', $code)->first();
                if (is_null($getScreenPermission))
                {
                    return false;
                }
                $screenId = $getScreenPermission->id;
                $data = DB::table('permission_role')->where('screen_id', $screenId)->where('role_id', Auth::user()->role)->where('permission_id', $permission)->get();
                if ($data->count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
    }
    /*
     * @param string $path
     * @param string $storage
     *  @param array[] $files
     *  @param boolean $base64
     */

    public function fileUpload($path, $storage, $fileType, $files = [], $base64 = false)
    {
        try
        {
            // IF multiple files are failed to upload then uncomment those 2 lines
            //ini_set('max_execution_time', 300);
            //ini_set('memory_limit','256M');

            $return = [];
            foreach ($files as $key => $file)
            {
                if ($base64)
                {
                    $image = str_replace('data:image/png;base64,', '', $file);
                    $image = str_replace(' ', '+', $image);
                    $imageName = time().'_'.Random::string(5).'.png';
                    $return[] = Storage::disk($storage)->put($path . $imageName, base64_decode($image));
                }
                else
                {
                    if (Str::before($file->getClientMimeType(), '/') == $fileType)
                    {
                        $fileName = time().'_'.$file->getClientOriginalName();
                        $return[] = Storage::disk($storage)->put($path, file_get_contents($file));
                    }
                }
            }
            return $return;
        }
        catch (\Throwable $exception)
        {
            dd($exception->getMessage());
            return [];
        }
    }

    public function getFiles($filePath = [], $storage)
    {
        try
        {
            $disk = Storage::disk($storage);
            $returnPath = [];
            foreach ($filePath as $key => $path)
            {
                if ($disk->exists($path))
                {
                    $returnPath[] = $disk->url($path);
                }
            }
            return $returnPath;
        }
        catch (\Throwable $exception)
        {
            return [];
        }
    }

    public function destroyFile($filePath, $storage)
    {
        try
        {
            $disk = Storage::disk($storage);
            if($disk->exists($filePath))
            {
                $disk->delete($filePath);
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (\Throwable $exception)
        {
            return false;
        }
    }

    public static function generate_salt($length = 22){

        $random_string = md5(uniqid(mt_rand(),true));

        $base_64 = base64_encode($random_string);

        $modified_string = str_replace('+','.',$base_64);

        $salt = substr($modified_string,0,$length);

        return $salt;

    }

    public static function getNewCode($table = null, $type = 0, $typeColumn = null, $typeStatus = false)
    {
        $lastCodeFromRecord = DB::table($table);
        if($typeStatus)
        {
           $lastCodeFromRecord = $lastCodeFromRecord->where($typeColumn, $type); 
        }
        $lastCodeFromRecord = $lastCodeFromRecord->latest()->first();
        if($lastCodeFromRecord) {
            $code = $lastCodeFromRecord->code + 1;
            return $code;
        }
        // Get Code from Setup File
        return config('setup.tables.'.$table.'.'.$type);
    }

    public static function getDocumentCode($table = null)
    {
        $table = $table ? $table : 'gl_account';
        $lastCodeFromRecord = DB::table($table)->latest()->first();
        if($lastCodeFromRecord) {
            if($table == 'gl_account') 
            {
                $code = $lastCodeFromRecord->document_no + 1;
            }
            else
            {
                $code = $lastCodeFromRecord->material_document_number + 1;
            }
            return $code;
        }
        return 100000;
    }

    public static function addGLAccountEnteries($record = [])
    {
        if(count($record) == 0)
        {
            return false;
        }
        foreach ($record as $key => $value) {
            $record[$key]['document_no'] = GeneralFunctions::getDocumentCode();
        }
        GLAccount::insert($record);
        return true;
    }

    public static function calculateAverageWeight($specificFigure, $record)
    {
        $expenses = ['personal_expense_in_pkr', 'shipping_expense_in_pkr', 'custom_duty_one', 'custom_duty_two', 'excise_one', 'excise_two', 'do', 'do_charges', 'group_misc', 'examination', 'yard_payment', 'duty', 'token', 'de_blocking', 'counter_ground', 'sepy', 'pdc_charges', 'auction_release', 'agency'];
        $totalExpenseOutput = 0;
        $totalFigure = 0;
        $containerDetails = $record->container_details->toArray();
        foreach ($containerDetails as $key => $value) {
            if(in_array($key, $expenses)) 
            {
                $totalExpenseOutput = $totalExpenseOutput + $value;
            }
        }
        foreach ($record->container_details->container_item_details->toArray() as $key => $value) {
            $totalFigure = $totalFigure + $value['purchase_order_total']['total_in_pkr'];
        }
        return ($specificFigure / $totalFigure) * $totalExpenseOutput;
    }
}