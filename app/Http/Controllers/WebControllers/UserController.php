<?php

namespace App\Http\Controllers\WebControllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\TutorialRequest;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $userTable = null;
    private $tutorialRequestTable = null;

    public function __construct()
    {
        $this->userTable = new User;
        $this->tutorialRequestTable = new TutorialRequest;
    }

    /**
     * Show Login Screen for Users.
     *
     */
    public function getLoginScreen(Request $req)
    {
        return view('website.layouts.auth.login');
    }

    /**
     * Authentication
     * (email, password)
     */
    public function login(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false]);
        }
        $credentials = $req->only('email', 'password');
        $credentials['type'] = 2;
        $credentials['active'] = 1;
        if (Auth::attempt($credentials)) {
            return response()->json(['status' => true]);
        }
        return response()->json(['status' => false, 'msg' => 'Invalid Credentials or Verification Issue']);
    }

    /**
     * Show User Regostraton account Screen for Users.
     *
     */
    public function getUserRegistrationAccountScreen(Request $req)
    {
        return view('website.layouts.auth.register');
    }

    /**
     * Authentication
     * (email, password)
     */
    public function register(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required|confirmed|min:8',
            'phone' => 'required',
            'location' => 'required',
            'user_type' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'messages' => $validator->messages()]);
        }
        $input = $req->only(['first_name', 'last_name', 'email', 'password', 'phone', 'location', 'user_type']);
        $input['password'] = Hash::make($input['password']);
        $input['type'] = 2;
        $input['active'] = 1;
        $input['name'] = $req->first_name.' '.$req->last_name;
        $this->userTable->create($input);
        // Send Email to User for verfication
        return response()->json(['status' => true]);
    }

    /**
     * Logout
     * 
     */
    public function logout(Request $req)
    {
        Auth::logout();
        return redirect('/login_account');
    }

    /**
     *
     * Tution Form Submition
     *
     */
    public function postTutionForm(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'guardian_name' => 'required',
            'email' => 'required',
            'subjects' => 'required',
            'phone_no' => 'required',
            'country' => 'required',
            'gradelevels' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'mailSent' => false, 'messages' => $validator->messages(), 'message' => 'Validation Error !!']);
        }
        $input = $req->only(['guardian_name', 'subjects', 'email', 'phone_no', 'country', 'gradelevels', 'comment']);
        $input['full_name'] = $input['guardian_name'];
        $input['type'] = 2;
        $input['status'] = 0;
        $this->tutorialRequestTable->create($input);
        return response()->json(['status' => true, 'mailSent' => true, 'message' => 'Successfully Submited the form. Will Respond you in 24 hours']);
    }    
}
