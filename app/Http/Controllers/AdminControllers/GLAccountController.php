<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use App\Http\GeneralFunctions;
use DB;
use Validator;

class GLAccountController extends Controller
{
	private $table;
    private $tableName;
    use GeneralFunctions;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->tableName = 'gl_account';
        $this->middleware('auth');
    }

    /**
     *
     * GL Account Report Form
     *
     */
    public function index(Request $req)
    {
        $record = [];
        if(count($req->all()) > 0)
        {
            $record = DB::table($this->tableName);
            if($req->gl_code)
            {
                $record = $record->where('gl_code', $req->gl_code);
            }
            if($req->document_no)
            {
                $record = $record->where('document_no', $req->document_no);
            }
            if($req->from_period || $req->to_period || $req->year)
            {
                $this->validate($req, [
                    'from_period' => 'required',
                    'to_period' => 'required',
                    'year' => 'required',
                ]);
                $periodRecord = [];
                for ($i=$req->from_period; $i <= $req->to_period ; $i++) { 
                    array_push($periodRecord, $i);
                }
                $record = $record->whereIn('period', $periodRecord)->whereYear('created_at', $req->year);
            }
            $record =$record->get()->toArray();
            return view('admin.layouts.pages.glAccount.form', compact('record'));
        }
        return view('admin.layouts.pages.glAccount.form', compact('record'));
    }
}