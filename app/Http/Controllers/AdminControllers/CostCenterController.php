<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use Auth;
use App\CostCenter;
use App\Http\Controllers\Controller;
use App\Http\GeneralFunctions;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Crypt;

class CostCenterController extends Controller
{
	private $table;
    private $tableName;
    use GeneralFunctions;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->table = new CostCenter;
        $this->tableName = 'cost_center';
        $this->middleware('auth');
    }

    /**
     *
     * Vendors Form
     *
     */
    public function index(Request $req)
    {
    	return view('admin.layouts.pages.costcenter.list');
    }

    /**
     *
     * Store Cost Center Record
     *
     */
    public function store(Request $req)
    {
        $this->validate($req, [
            'name' => 'required|max:100',
        ], [
            'name.required' => 'Full Name is required',
        ]);
        // Save Record
        $data = $req->only(['name']);
        if($req->id)
        {
            $data = $req->only(['name', 'id']);
            $record = $this->table->where('id', $req->id)->update(['name' => $req->name]);
            return back()->with('success', 'Cost Center Updated Successfully');
        }
        $data['code'] = GeneralFunctions::getNewCode($this->tableName, 0, 'cost_center');
        $record = $this->table->create($data);
        if($record) 
        {
            return back()->with('success', 'Cost Center Added Successfully');
        }
    }

    /**
     *
     * Cost Center Data table record
     *
     */
    public function dataTableRequest(Request $request)
    {
        try {
            $costcenter = $this->table->select(['id', 'name', 'code']);//->orderBy('users.id', 'Asc');
            return DataTables::of($costcenter)
                ->addIndexColumn()
                ->addColumn('action', function ($costcenter) {
                    $aciton = '<div class="btn-group" role="group"><button id="btnGroupDrop1" type="button" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button><div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                    $aciton .= '<a href="' . route("edit.costcenter.form", ["id" => \Illuminate\Support\Facades\Crypt::encryptString($costcenter->id)]) . '" class="dropdown-item"><i class="fa fa-edit"></i>Edit Cost Cneter</a></div></div>';
                    return $aciton;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        catch (\Throwable $exception)
        {
            return DataTables::of([])->make(true);
        }
    }

    /**
     *
     * Cost Center Form
     *
     */
    public function costCenterForm(Request $request)
    {
        $newCode = GeneralFunctions::getNewCode($this->tableName, 0, 'cost_center');
        return view('admin.layouts.pages.costcenter.form', compact('newCode'));
    }

    /**
     *
     * Edit Cost Center Form
     *
     */
    public function editCostCenter($id)
    {   
        $costcenter = $this->table->where('id', Crypt::decryptString($id))->first();
        return view('admin.layouts.pages.costcenter.form', compact('costcenter'));
    }
    
}