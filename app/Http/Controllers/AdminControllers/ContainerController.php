<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use App\Http\GeneralFunctions;
use App\PurchaseOrder;
use App\ContainerDetails;
use App\ContainerItemDetails;
use App\Material;
use App\SettlementContainerItems;
use App\ContainerFiles;
use DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Crypt;

class ContainerController extends Controller
{
	private $table;
    private $tableName;
    private $PurchaseOrder;
    use GeneralFunctions;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->table = new ContainerDetails;
        $this->PurchaseOrder = new PurchaseOrder;
        $this->containerItemDetails = new ContainerItemDetails;
        $this->containerSettlementItemDetails = new SettlementContainerItems;
        $this->tableName = 'container_details';
        $this->middleware('auth');
    }

    /**
     *
     * Containers List
     *
     */
    public function index(Request $req)
    {
        $containersRecord = [$this->table->all()->toArray()];
        return view('admin.layouts.pages.container.list', compact('containersRecord'));
    }

    /**
     *
     * Container Form
     *
     */
    public function form(Request $req)
    {
        $purchaseOrderInfo = $this->PurchaseOrder->where('status',0)->get()->toArray();
        $complatedPurchaseOrderInfo = $this->PurchaseOrder->where('status',2)->get()->toArray();
        $materialRecord = Material::all()->toArray();
        $status = null;
        $newCode = GeneralFunctions::getNewCode($this->tableName, 0, 'container_details');
    	return view('admin.layouts.pages.container.form', compact('purchaseOrderInfo', 'status', 'complatedPurchaseOrderInfo', 'materialRecord', 'newCode'));
    }

    /**
     *
     * Store Vendor Record
     *
     */
    public function store(Request $req)
    {
        $settlementOption = false;
        if(!$req->id)
        {
            $validationRules = [
                'date' => 'required',
                'purchase_order_id' => 'required|array',
                'purchase_order_id.*' => 'required|numeric',
                'no_of_packages' => 'required|numeric',
                'custom_reference' => 'required',
                'cash_no' => 'required',
                'igm_no' => 'required',
                'vessel' => 'required',
                'bill_no' => 'required',
                'exchange_rate_yuan' => 'required|numeric',
                'personal_expense_in_yuan' => 'required|numeric',
                'shipping_expense_in_yuan' => 'required|numeric',
                'personal_expense_in_pkr' => 'required|numeric',
                'shipping_expense_in_pkr' => 'required|numeric',
            ];
            if($req->completed_purchase_order_id)
            {
                if(count($req->completed_purchase_order_id) > 0)
                {
                    $validationRules['completed_purchase_order_id'] = 'required|array';
                    $validationRules['completed_purchase_order_id.*'] = 'required|numeric';
                    $validationRules['settlement_material_id'] = 'required|array';
                    $validationRules['settlement_material_id.*'] = 'required|numeric';
                    $validationRules['settlement_quantity'] = 'required|array';
                    $validationRules['settlement_quantity.*'] = 'required|numeric';
                }
                $settlementOption = true;
            }
            $this->validate($req, $validationRules, [
                'date.required' => 'Container Despatch Date is required'
            ]);
        }
        // try
        // {
            // Save Record
            $containerRecord = $req->only(['date', 'no_of_packages', 'custom_reference', 'cash_no', 'igm_no', 'vessel', 'bill_no', 'exchange_rate_yuan', 'personal_expense_in_yuan', 'shipping_expense_in_yuan', 'personal_expense_in_pkr', 'shipping_expense_in_pkr']);
            // check if settlement Record is inserting

            if($req->id)
            {
                // Update Record
                $record = $req->only(['date', 'code', 'status', 'no_of_packages', 'custom_reference', 'cash_no', 'igm_no', 'vessel', 'bill_no', 'exchange_rate_yuan', 'personal_expense_in_yuan', 'shipping_expense_in_yuan', 'personal_expense_in_pkr', 'shipping_expense_in_pkr', 'custom_duty_one', 'custom_duty_two', 'excise_one', 'excise_two', 'do', 'do_charges', 'group_misc', 'examination','yard_payment', 'duty', 'token', 'de_blocking', 'counter_ground', 'sepy', 'pdc_charges', 'auction_release', 'agency']);
                $record['status'] = 1;
                $updatedRecord = $this->table->where('id', $req->id)->update($record);
                $pakistanExpenseAmount = $req->custom_duty_one + $req->custom_duty_two + $req->excise_one + $req->excise_two + $req->do + $req->do_charges + $req->group_misc + $req->examination + $req->yard_payment + $req->duty + $req->token + $req->de_blocking + $req->counter_ground + $req->sepy + $req->pdc_charges + $req->auction_release + $req->agency;
                $record = [
                    [
                        'transaction_type' => 10,
                        'gl_code' => 1021,
                        'gl_description' => config('glcodes.1021.0'),
                        'amount' => $pakistanExpenseAmount,
                        'company' => 104,
                        'cost_center' => 1005,
                        'po_id' => null,
                        'container_id' => $req->id,
                        'period' => 2,
                        'posting_date' => date('Y-m-d H:i:s'),
                        'created_at' => date('Y-m-d H:i:s')
                    ],
                    [
                        'transaction_type' => 20,
                        'gl_code' => 1043,
                        'gl_description' => config('glcodes.1043.0'),
                        'amount' => $pakistanExpenseAmount,
                        'company' => 104,
                        'cost_center' => 1005,
                        'po_id' => null,
                        'container_id' => $req->id,
                        'period' => 2,
                        'posting_date' => date('Y-m-d H:i:s'),
                        'created_at' => date('Y-m-d H:i:s')
                    ]
                ];
                GeneralFunctions::addGLAccountEnteries($record);
                return back()->with('success', 'Comtainer Completed Successfully');
            }
            $containerRecord['code'] = GeneralFunctions::getNewCode($this->tableName, 0, 'container_details');
            $recordData = [];
            $req->purchase_order_id = array_values(array_unique($req->purchase_order_id));
            $req->total_in_pkr = array_values(array_unique($req->total_in_pkr));
            foreach ($req->purchase_order_id as $key => $value)
            {
                $recordData[$key]['purchase_order_id'] = $value;
                $recordData[$key]['total_in_pkr'] = $req->total_in_pkr[$key];
            }
            if (count($recordData) == 0) {
                return back()->withErrors('Please add atleast one Purchase Order');
            }
            $containerDetails = $this->table->create($containerRecord);
            // Check if Any Files to Upload
            if($req->hasfile('container_files'))
            {
                foreach ($req->file('container_files') as $file) 
                {
                    $name = $file->getClientOriginalName();
                    $file->move(public_path() . '/img/container_files/', $name);
                    $uploadFileRecords[] = ['file_path' => '/img/container_files/'.$name, 'conatainer_id' => $containerDetails->id, 'created_at' => date('Y-m-d'), 'updated_at' => date('Y-m-d')];
                }
                if(count($uploadFileRecords) > 0)
                {
                    ContainerFiles::insert($uploadFileRecords);
                }
            }
            $record = [
                [
                    'transaction_type' => 10,
                    'gl_code' => 1021,
                    'gl_description' => config('glcodes.1021.0'),
                    'amount' => $req->personal_expense_in_pkr + $req->shipping_expense_in_pkr,
                    'company' => 104,
                    'cost_center' => 1005,
                    'po_id' => null,
                    'container_id' => $containerDetails->id,
                    'period' => 2,
                    'posting_date' => $req->date,
                    'created_at' => date('Y-m-d H:i:s')
                ],
                [
                    'transaction_type' => 20,
                    'gl_code' => 1043,
                    'gl_description' => config('glcodes.1043.0'),
                    'amount' => $req->personal_expense_in_pkr + $req->shipping_expense_in_pkr,
                    'company' => 104,
                    'cost_center' => 1005,
                    'po_id' => null,
                    'container_id' => $containerDetails->id,
                    'period' => 2,
                    'posting_date' => $req->date,
                    'created_at' => date('Y-m-d H:i:s')
                ]
            ];
            GeneralFunctions::addGLAccountEnteries($record);
            if($containerDetails) 
            {
                // If Purchase Record is succsfully Added
                foreach ($req->purchase_order_id as $key => $value) {
                    $recordData[$key]['conatainer_id'] = $containerDetails->id;
                }
                $containerItemRecords = $this->containerItemDetails->insert($recordData);
                if($settlementOption)
                {
                    $settlementRecord = [];
                    $req->completed_purchase_order_id = array_values(array_unique($req->completed_purchase_order_id));
                    $req->settlement_material_id = array_values(array_unique($req->settlement_material_id));
                    foreach ($req->completed_purchase_order_id as $settlementKey => $settlementValue)
                    {
                        $settlementRecord[$settlementKey]['purchase_order_id'] = $settlementValue;
                        $settlementRecord[$settlementKey]['material_id'] = $req->settlement_material_id[$settlementKey];
                        $settlementRecord[$settlementKey]['quantity'] = $req->settlement_quantity[$settlementKey];
                    }
                    $containerSettlementItemRecords = $this->containerSettlementItemDetails->insert($settlementRecord);
                }
                if($containerItemRecords)
                {
                    // Update Purchase Order
                    $updatePurchaseOrder = DB::table('purchase_order')->whereIn('id', $req->purchase_order_id)->update(['status' => 1]);
                    return back()->with('success', 'Container Details saved successfully');
                }
            }
        // } 
        // catch(\Throwable $exception) 
        // {
        //     return back()->withErrors('Internal server error');
        // }
    }

    public function dataTableRequest(Request $request)
    {
        try {
            $containers = $this->table->select(['id', 'date', 'code', 'status', 'no_of_packages', 'custom_reference']);//->orderBy('users.id', 'Asc');
            return DataTables::of($containers)
                ->addIndexColumn()
                ->addColumn('status', function ($containers) {
                    if ($containers->status == 0) {
                        $status = '<label class="badge badge-light text-white">In Transit</label>';
                    } else {
                        $status = '<label class="badge badge-success">Completed</label>';
                    }
                    return $status;
                })
                ->addColumn('action', function ($containers) {
                    $aciton = '<div class="btn-group" role="group"><button id="btnGroupDrop1" type="button" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button><div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                    if ($containers->status == 0) {
                        $aciton .= '<a href="' . route("edit.container", ["id" => \Illuminate\Support\Facades\Crypt::encryptString($containers->id), 'status' => 0]) . '" class="dropdown-item"><i class="fa fa-edit"></i>Complete Container</a>';
                    } else {
                        $aciton .= '<a href="' . route('edit.container', ['id' => \Illuminate\Support\Facades\Crypt::encryptString($containers->id), 'status' => 1]) . '" class="dropdown-item delete_btn"><i class="fa fa-trash"></i>Show Container Details</a></div></div>';
                    }
                    return $aciton;
                })
                ->rawColumns(['status', 'action'])
                ->make(true);
           /* ->addColumn('phone number',function($users)){
               {
                 if ($users->) {

                 }
               }
        }*/
        }

        catch (\Throwable $exception)
        {
            return DataTables::of([])->make(true);
        }
    }

    public function editContainer($id, $status)
    {   
        $purchaseOrderInfo = $this->PurchaseOrder->where('status',1)->get()->toArray();
        $containerInfo = $this->table->where('id', Crypt::decryptString($id))->with('container_item_details', 'container_files_details')->first();
        $complatedPurchaseOrderInfo = $this->PurchaseOrder->where('status',2)->get()->toArray();
        $newCode = $containerInfo->code;
        return view('admin.layouts.pages.container.form', compact('purchaseOrderInfo', 'containerInfo', 'status', 'complatedPurchaseOrderInfo', 'newCode'));
    }
}