<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use App\Http\GeneralFunctions;
use App\Vendors;
use App\Material;
use App\PurchaseOrder;
use App\PurchaseOrderMaterialItems;
use Yajra\DataTables\Facades\DataTables;

class PurchaseOrderController extends Controller
{
	private $table;
    private $tableName;
    private $purchaseOrderMaterialItems;
    use GeneralFunctions;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->table = new PurchaseOrder;
        $this->purchaseOrderMaterialItems = new PurchaseOrderMaterialItems;
        $this->tableName = 'purchase_order';
        $this->middleware('auth');
    }

    /**
     *
     * Purchase Order List
     *
     */
    public function index(Request $req)
    {
    	return view('admin.layouts.pages.purchaseOrder.list');
    }

    /**
     *
     * Purchase Order Form
     *
     */
    public function purchaseOrder(Request $req)
    {
        $vendorInfo = Vendors::all()->toArray();
        $materialInfo = Material::all()->toArray();
        $newCode = GeneralFunctions::getNewCode($this->tableName, 0, 'purchase_order');
        return view('admin.layouts.pages.purchaseOrder.form', compact('vendorInfo', 'materialInfo', 'newCode'));
    }

    /**
     *
     * Store Vendor Record
     *
     */
    public function store(Request $req)
    {
        $this->validate($req, [
            'purchase_order_date' => 'required',
            'vendor_id' => 'required',
            'exchange_rate_yuan' => 'required|numeric',
            'material_id' => 'required|array',
            'material_id.*' => 'required|numeric',
            'quantity' => 'required|array',
            'quantity.*' => 'required|numeric',
            'unit_price_in_pkr' => 'required|array',
            'unit_price_in_pkr.*' => 'required|numeric',
            'unit_price_in_yuan' => 'required|array',
            'unit_price_in_yuan.*' => 'required|numeric',
            'amount_paid' => 'required|numeric'
        ], [
            'purchase_order_date.required' => 'Purchase Order Date is required',
            'vendor_id.required' => 'Vendor is required',
            'exchange_rate_yuan.required' => 'Exchange Rate is required'
        ]);
        // try
        // {
            // Save Record
            $purchaseOrderRecord = $req->only(['purchase_order_date', 'vendor_id', 'exchange_rate_yuan', 'amount_paid']);
            $purchaseOrderRecord['code'] = GeneralFunctions::getNewCode($this->tableName, 0, 'purchase_order');
            $recordData = [];
            $totalInPkr = 0;
            $totalInYuan = 0;
            foreach ($req->material_id as $key => $value)
            {
                $recordData[$key]['material_id'] = $value;
                $recordData[$key]['quantity'] = $req->quantity[$key];
                $recordData[$key]['unit_price_in_pkr'] = $req->unit_price_in_pkr[$key];
                $recordData[$key]['unit_price_in_yuan'] = $req->unit_price_in_yuan[$key];
                $recordData[$key]['total_in_pkr'] = $req->quantity[$key] * $req->unit_price_in_pkr[$key];
                $recordData[$key]['total_in_yuan'] = $req->quantity[$key] * $req->unit_price_in_yuan[$key];
                $totalInPkr = $totalInPkr + $recordData[$key]['total_in_pkr'];
                $totalInYuan = $totalInYuan + $recordData[$key]['total_in_yuan'];
            }
            if (count($recordData) == 0) {
                return back()->withErrors('Please add atleast one Material Item in Purchase Order');
            }
            if ($purchaseOrderRecord['amount_paid'] > $totalInPkr) {
                return back()->withErrors('Your payment is exceeding from total PO Amount');
            }
            $purchaseOrderRecord['total_in_pkr'] = $totalInPkr;
            $purchaseOrderRecord['total_in_yuan'] = $totalInYuan;
            $purchaseOrder = $this->table->create($purchaseOrderRecord);
            if($purchaseOrder) 
            {
                // If Purchase Record is succsfully Added
                foreach ($req->material_id as $key => $value) {
                    $recordData[$key]['purchase_order_id'] = $purchaseOrder->id;
                }
                $purchaseOrderItemRecords = $this->purchaseOrderMaterialItems->insert($recordData);
                if($purchaseOrderItemRecords)
                {
                    $remainingAmount = $totalInPkr - $req->amount_paid;
                    // GL Account
                    $record = [
                        [
                            'transaction_type' => 10,
                            'gl_code' => 1021,
                            'gl_description' => config('glcodes.1021.0'),
                            'amount' => $totalInPkr,
                            'company' => 104,
                            'cost_center' => 105,
                            'po_id' => $purchaseOrderRecord['code'],
                            'container_id' => null,
                            'period' => date('m'),
                            'posting_date' => $req->purchase_order_date,
                            'created_at' => date('Y-m-d H:i:s')
                        ],
                        [
                            'transaction_type' => 20,
                            'gl_code' => 1043,
                            'gl_description' => config('glcodes.1043.0'),
                            'amount' => $req->amount_paid,
                            'company' => 104,
                            'cost_center' => 105,
                            'po_id' => $purchaseOrderRecord['code'],
                            'container_id' => null,
                            'period' => date('m'),
                            'posting_date' => $req->purchase_order_date,
                            'created_at' => date('Y-m-d H:i:s')
                        ]
                    ];
                    if ($remainingAmount > 0)
                    {
                        $record[2] = [                            
                            'transaction_type' => 20,
                            'gl_code' => 2002,
                            'gl_description' => config('glcodes.2002.0'),
                            'amount' => $remainingAmount,
                            'company' => 104,
                            'cost_center' => 105,
                            'po_id' => $purchaseOrderRecord['code'],
                            'container_id' => null,
                            'period' => date('m'),
                            'posting_date' => $req->purchase_order_date,
                            'created_at' => date('Y-m-d H:i:s')   
                        ]; 
                    }
                    GeneralFunctions::addGLAccountEnteries($record);
                    return back()->with('success', 'Purchase Order saved successfully');
                }
            }
        // } 
        // catch(\Throwable $exception) 
        // {
        //     return back()->withErrors('Internal server error');
        // }
    }

    /**
     *
     * Get Purchase Order Details
     *
     */
    public function getPurchaseOrderDetails(Request $req)
    {
        $record = PurchaseOrder::where('code', $req->code)->where('status', 1)->with(['purchase_order_item_details.material_detail', 'container_item_details.container_details.container_item_details.purchase_order_total', 'container_item_details.container_details.get_settlement_options.material_detail', 'container_item_details.container_details.get_settlement_options.purchase_order_detail'])->first();
        if ($record) 
        {
            $averageContainerCost = GeneralFunctions::calculateAverageWeight($record->total_in_pkr, $record->container_item_details);
            return response()->json(['status' => true, 'data' => $record, 'container_amount' => $averageContainerCost]);
        }
        return response()->json(['status' => false]);
    }

    /**
     *
     * Vendor Data table record
     *
     */
    public function dataTableRequest(Request $request)
    {
        try {
            $material = $this->table->with('vendor');//->orderBy('users.id', 'Asc');
            return DataTables::of($material)
                ->addIndexColumn()
                ->make(true);
        }

        catch (\Throwable $exception)
        {
            return DataTables::of([])->make(true);
        }
    }

    /**
     *
     * Purchase Order Material List
     *
     */
    public function getPurchaseOrderMaterials(Request $request)
    {
        $record = PurchaseOrder::where('id', $request->id)->with('purchase_order_item_details.material_detail')->first();
        if($record)
        {
            return response()->json(['status' => true, 'data' => $record]);
        }
        return response()->json(['status' => false]);
    }
}