<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use Auth;
use App\GRNPurchaseOrder;
use App\PurchaseOrder;
use App\Http\Controllers\Controller;
use App\Http\GeneralFunctions;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Crypt;
use App\MaterialInventory;
use App\GRNPurchaseOrderMaterial;
use App\MaterialAverage;
use App\MaterialSettlement;
use DB;
use Validator;


class GRNController extends Controller
{
	private $table;
    private $tableName;
    use GeneralFunctions;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->table = new GRNPurchaseOrder;
        $this->tableName = 'cost_center';
        $this->middleware('auth');
    }

    /**
     *
     * Vendors Form
     *
     */
    public function index(Request $req)
    {

    	return view('admin.layouts.pages.grn.list');
    }

    /**
     *
     * Store Cost Center Record
     *
     */
    public function store(Request $req)
    {
        $this->validate($req, [
            'grn_type' => 'required',
            'po_number' => 'required',
            'materialId' => 'required|array',
            'materialId.*' => 'required',
            'recievedQty' => 'required|array',
            'recievedQty.*' => 'required',
            'qty' => 'required|array',
            'qty.*' => 'required',
            'materialCode' => 'required|array',
            'materialCode.*' => 'required'
        ], [
            'grn_secret_data.required' => 'GRN Record Data is required',
            'grn_type.required' => 'GRN Type is required',
        ]);
        // Save Record
        $data = $req->only(['grn_secret_data', 'grn_type', 'po_number', 'materialId', 'recievedQty', 'qty', 'materialCode']);
        $record = PurchaseOrder::where('code', $req->po_number)->where('status', 1)->with(['purchase_order_item_details.material_detail', 'container_item_details.container_details.container_item_details.purchase_order_total'])->first();
        if ($record) 
        {
            // Formula to get average weight for container expenses
            $insertRemainingRecord = [];
            $materialRecords = [];
            $materialFilter = '(';
            $averageContainerCost = GeneralFunctions::calculateAverageWeight($record->total_in_pkr, $record->container_item_details);
            $grnMainRecord = $this->table->create([
                'po_code' => $record->code,
                'po_id' => $record->id,
                'container_code' => $record->container_item_details->container_details->code,
                'container_id' => $record->container_item_details->container_details->id,
                'container_total_amount' => $averageContainerCost,
                'container_weighted_amount' => $averageContainerCost,
                'grn_type' => $req->grn_type,
                'status' => 1,
            ]);
            $documentNumber = GeneralFunctions::getDocumentCode('material_inventory');
            $count = 0;
            $materialPeriodRecord=[];
            $remainingAverageItems=[];
            $purchaseItemDetailsArray = $record->purchase_order_item_details->toArray();
            $settlementRecordEnteries = [];

            foreach ($purchaseItemDetailsArray as $key => $value) {
                array_push($materialRecords, $value['material_detail']['code']);
                $totalAmount = $value['total_in_pkr'] +(($value['total_in_pkr'] / $record->total_in_pkr) * $averageContainerCost);

                if($count == (count($purchaseItemDetailsArray) - 1))
                {
                   $materialFilter .= $value['material_detail']['code'].')'; 
                } else {
                    $materialFilter .= $value['material_detail']['code'].',';
                }

                /*----------  Add Enteries on Settlement Table  ----------*/
                $counter = 0;
                if($req->settlement_type[$key] == 1)
                {
                    // Add container Code ID, Purchase Order Code ID, Settlement Qty, Settlement Rate, Material Code ID, Status (default => in complete)
                    $settlementRecordEnteries[$counter]['purchase_order_id'] = $record->id;
                    $settlementRecordEnteries[$counter]['purchase_order_code'] = $record->code;
                    $settlementRecordEnteries[$counter]['material_code'] = $value['material_detail']['code'];
                    $settlementRecordEnteries[$counter]['material_id'] = $value['material_detail']['id'];
                    $settlementRecordEnteries[$counter]['container_code'] = $record->container_item_details->container_details->code;
                    $settlementRecordEnteries[$counter]['container_id'] = $record->container_item_details->container_details->id;
                    $settlementRecordEnteries[$counter]['settlement_qty'] = (int) $value['quantity'] - (int) $req->recievedQty[$key];
                    $settlementRecordEnteries[$counter]['settlement_rate'] = $totalAmount / (int) $value['quantity'];
                    $counter++;
                }
                /*----------  End Section Enteries on Settlement Table  ----------*/
                
                // if($req->settlement_type[$key] == 1)
                // {
                //     $materialCodeQuantityUpdateArray[$value['material_detail']['code']] = ['settlement'  => true, 'qty' => $value['quantity']];
                // }
                $recordMaterialData[$key] = [
                    'grn_id' => $grnMainRecord->id,
                    'material_id' => $value['material_detail']['id'],
                    'material_code' => $value['material_detail']['code'],
                    'qty' => $value['quantity'],
                    'recieved_qty' => (int) $req->recievedQty[$key],
                    'direct_purchase' => $value['total_in_pkr'],
                    'direct_rate' => $value['unit_price_in_pkr'],
                    'container_expense' => ($value['total_in_pkr'] / $record->total_in_pkr) * $averageContainerCost,
                    'total_amount' => $totalAmount,
                    'revised_rate' => $totalAmount / (int) $req->recievedQty[$key],
                    'settlement_rate' => $totalAmount / (int) $value['quantity'],
                    'settlement_qty' => (int) $value['quantity'] - (int) $req->recievedQty[$key],
                    'settlement_type' => $req->settlement_type[$key], // 1 => settlement, 0 => No settlement 
                    'created_at' => date('Y-m-d')
                ];                
                $materialInventory[$key] = [
                    'storage_location' => 100,
                    'movement_type' => 105,
                    'material_code' => $value['material_detail']['code'],
                    'po_code' => $record->code,
                    'material_document_number' => $documentNumber,
                    'quantity' => (int) $req->recievedQty[$key],
                    'amount' => $req->settlement_type[$key] == 0 ? ($totalAmount * (int) $req->recievedQty[$key]) : ($totalAmount * (int) $value['quantity']),
                    'period' => date('m'),
                    'year' => date('Y'),
                    'company_code' => 104,
                    'cost_center_code' => 1005,
                    'rate' => ($req->settlement_type[$key] == 0) ? ($totalAmount / (int) $req->recievedQty[$key]) : $totalAmount / (int) $value['quantity'],
                    'created_at' => date('Y-m-d')
                ];
                $materialQtyValueRecord[$value['material_detail']['code']]['recieved_qty'] = (int) $req->recievedQty[$key];
                $materialQtyValueRecord[$value['material_detail']['code']]['totalAmount'] = $totalAmount;

                $defaultInsertionAverageMaterialRecord[$key] = [
                    'material_code' => $value['material_detail']['code'],
                    'opening_qty' => 0,
                    'opening_value' => 0,
                    'total_receipt_qty' => (int) $req->recievedQty[$key],
                    'total_receipt_value' => $totalAmount,
                    'closing_position_qty' => (int) $req->recievedQty[$key],
                    'closing_position_value' => $totalAmount,
                    'issuance_rate' => ($req->settlement_type[$key] == 0) ? ($totalAmount / (int) $req->recievedQty[$key]) : $totalAmount / (int) $value['quantity'],
                    'period' => date('m'),
                    'year' => date('Y')
                ];

                $count++;
            }
            // Create Average Inventory
            $currentMonth = date('m');
            $currentYear = date('Y');
            // $getLatestAverageRateRecords = DB::select("SELECT * FROM material_average WHERE material_code IN {$materialFilter} AND period = {$currentMonth} AND id IN ( SELECT MAX(id) FROM material_inventory GROUP BY material_code)");
            $getLatestAverageRateRecords = DB::select("SELECT * FROM material_average WHERE material_code IN {$materialFilter} AND id IN ( SELECT MAX(id) FROM material_average GROUP BY material_code)");

            if(count($getLatestAverageRateRecords) > 0)
            {
                $total_receipt_qty = '';
                $total_receipt_value = '';
                $closing_position_qty = '';
                $closing_position_value = '';
                $issuance_rate = '';

                $count = 0;
                foreach ($getLatestAverageRateRecords as $key => $value) 
                {
                    // echo 'Current Month = '. $currentMonth;
                    // echo 'Current Year = '. $currentYear;
                    // echo 'Current Period = '. $value->period;
                    // echo 'Year = '. $value->year;
                    // die();
                    if($currentMonth == $value->period && $currentYear == $value->year)
                    {
                        if(count($getLatestAverageRateRecords) == 1)
                        {
                            $total_receipt_qty .= "WHEN material_code = {$value->material_code} AND period = $currentMonth THEN total_receipt_qty + {$materialQtyValueRecord[$value->material_code]['recieved_qty']} ELSE total_receipt_qty END";
                            $total_receipt_value .= "WHEN material_code = {$value->material_code} AND period = $currentMonth THEN total_receipt_value + {$materialQtyValueRecord[$value->material_code]['totalAmount']} ELSE total_receipt_value END";
                            $closing_position_qty .= "WHEN material_code = {$value->material_code} AND period = $currentMonth THEN (opening_qty + total_receipt_qty - total_issuance_qty) ELSE closing_position_qty END";
                            $closing_position_value .= "WHEN material_code = {$value->material_code} AND period = $currentMonth THEN (opening_value + total_receipt_value - total_issuance_value) ELSE closing_position_value END";
                            $issuance_rate .= "WHEN material_code = {$value->material_code} AND period = $currentMonth THEN (closing_position_value / closing_position_qty) ELSE issuance_rate END";
                        }
                        else {
                            // echo $count;
                            // print_r(count($getLatestAverageRateRecords) - 1); 
                            // dd('------------');
                            if($count == (count($getLatestAverageRateRecords) - 1))
                            {
                                $total_receipt_qty .= "WHEN material_code = {$value->material_code} AND period = $currentMonth THEN total_receipt_qty + {$materialQtyValueRecord[$value->material_code]['recieved_qty']} ELSE total_receipt_qty END";
                                $total_receipt_value .= "WHEN material_code = {$value->material_code} AND period = $currentMonth THEN total_receipt_value + {$materialQtyValueRecord[$value->material_code]['totalAmount']} ELSE total_receipt_value END";
                                $closing_position_qty .= "WHEN material_code = {$value->material_code} AND period = $currentMonth THEN (opening_qty + total_receipt_qty - total_issuance_qty) ELSE closing_position_qty END";
                                $closing_position_value .= "WHEN material_code = {$value->material_code} AND period = $currentMonth THEN (opening_value + total_receipt_value - total_issuance_value) ELSE closing_position_value END";
                                $issuance_rate .= "WHEN material_code = {$value->material_code} AND period = $currentMonth THEN (closing_position_value / closing_position_qty) ELSE issuance_rate END";
                            }
                            else {
                                $total_receipt_qty .= "WHEN material_code = {$value->material_code} AND period = $currentMonth THEN total_receipt_qty + {$materialQtyValueRecord[$value->material_code]['recieved_qty']} ";
                                $total_receipt_value .= "WHEN material_code = {$value->material_code} AND period = $currentMonth THEN total_receipt_value + {$materialQtyValueRecord[$value->material_code]['totalAmount']} ";
                                $closing_position_qty .= "WHEN material_code = {$value->material_code} AND period = $currentMonth THEN (opening_qty + total_receipt_qty - total_issuance_qty) ";
                                $closing_position_value .= "WHEN material_code = {$value->material_code} AND period = $currentMonth THEN (opening_value + total_receipt_value - total_issuance_value) ";
                                $issuance_rate .= "WHEN material_code = {$value->material_code} AND period = $currentMonth THEN (closing_position_value / closing_position_qty) ";
                            }
                        }
                        // dd($materialQtyValueRecord[$value->material_code]['recieved_qty']);
                        $index = array_search($value->material_code, $materialRecords);
                        unset($materialRecords[$index]);
                        $materialRecords = array_values($materialRecords);

                        // dd("UPDATE material_average SET total_receipt_qty = CASE {$total_receipt_qty}, total_receipt_value = CASE {$total_receipt_value}, closing_position_qty = CASE {$closing_position_qty}, closing_position_value = CASE {$closing_position_value}, issuance_rate = CASE {$issuance_rate}");
                        $count++;
                    }
                    else {
                        $remainingAverageItems[$value->material_code] = $value;
                    } 
                }
                if($count > 0)
                {
                    // dd("UPDATE material_average SET total_receipt_qty = CASE {$total_receipt_qty}, total_receipt_value = CASE {$total_receipt_value}, closing_position_qty = CASE {$closing_position_qty}, closing_position_value = CASE {$closing_position_value}, issuance_rate = CASE {$issuance_rate}");
                    $updateRecord = DB::connection()->getPdo()->exec("UPDATE material_average SET total_receipt_qty = CASE {$total_receipt_qty}, total_receipt_value = CASE {$total_receipt_value}, closing_position_qty = CASE {$closing_position_qty}, closing_position_value = CASE {$closing_position_value}, issuance_rate = CASE {$issuance_rate}");
                }

                // Insert Remaining Material Average Records
                foreach ($materialRecords as $key => $value) {
                    $opening_qty = 0;
                    $opening_value = 0;
                    if(array_key_exists($value, $remainingAverageItems))
                    { 
                        $opening_qty = $remainingAverageItems[$value]->closing_position_qty;
                        $opening_value = $remainingAverageItems[$value]->closing_position_value;
                    }
                    $insertRemainingRecord[$key] = [
                        'material_code' => $value,
                        'opening_qty' => $opening_qty,
                        'opening_value' => $opening_value,
                        'total_receipt_qty' => $materialQtyValueRecord[$value]['recieved_qty'],
                        'total_receipt_value' => $materialQtyValueRecord[$value]['totalAmount'],
                        'closing_position_qty' => $opening_qty + $materialQtyValueRecord[$value]['recieved_qty'],
                        'closing_position_value' => $opening_value + $materialQtyValueRecord[$value]['totalAmount'],
                        'issuance_rate' => ($opening_value + $materialQtyValueRecord[$value]['totalAmount']) / ($opening_qty + $materialQtyValueRecord[$value]['recieved_qty']),
                        'period' => date('m'),
                        'year' => date('Y')
                    ]; 
                }
                if(count($insertRemainingRecord) > 0)
                {
                    MaterialAverage::insert($insertRemainingRecord);
                }   
            }
            else {
                MaterialAverage::insert($defaultInsertionAverageMaterialRecord);
            }
            GRNPurchaseOrderMaterial::insert($recordMaterialData);
            MaterialInventory::insert($materialInventory);
            PurchaseOrder::where('code', $req->po_number)->update(['status' => 2]);
            if(count($settlementRecordEnteries) > 0)
            {
                MaterialSettlement::insert($settlementRecordEnteries);
            }
            return back()->with('success', 'GRN Entery added Successfully');
        }
        return back()->withErrors('Internal Server Error');
    }

    /**
     *
     * GRN Form
     *
     */
    public function grnForm(Request $request)
    {
        return view('admin.layouts.pages.grn.form');
    }

    /**
     *
     * Edit Cost Center Form
     *
     */
    public function editGrn($id)
    {   
        $costcenter = $this->table->where('id', Crypt::decryptString($id))->first();
        return view('admin.layouts.pages.costcenter.form', compact('costcenter'));
    }

    /**
     *
     * Data Table
     *
     */
    public function dataTableRequest()
    {
        try {
            $grn = $this->table->select(['id','po_code', 'po_id', 'container_code', 'container_id', 'container_total_amount', 'container_weighted_amount', 'grn_type', 'status']);//->orderBy('users.id', 'Asc');
            return DataTables::of($grn)
                ->addIndexColumn()
                ->addColumn('action', function ($grn) {
                    $aciton = '<div class="btn-group" role="group"><button id="btnGroupDrop1" type="button" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button><div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                    $aciton .= '<a href="' . route("show.grn.details", ["id" => \Illuminate\Support\Facades\Crypt::encryptString($grn->id)]) . '" class="dropdown-item"><i class="fa fa-edit"></i>Show Purchase Order Detail</a></div></div>';
                    return $aciton;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        catch (\Throwable $exception)
        {
            return DataTables::of([])->make(true);
        }
    }

    /**
     *
     * Show GRN Details
     *
     */
    public function showGrnDetail($id)
    {
        $record = [];
        $record = $this->table->where('id', Crypt::decryptString($id))->with('grn_order_items_details')->first();
        if($record)
        {
            $record = $record->toArray();
        }
        return view('admin.layouts.pages.grn.show', compact('record'));
    }

    /**
     *
     * Show Inventory Warehouse Detail Form
     *
     */
    public function warehouseDetailForm()
    {
        return view('admin.layouts.pages.grn.warehouse');
    }

    /**
     *
     * Report Generation
     *
     */
    public function inventoryReport(Request $req)
    {
        $rules =  [
            'record' => 'required|array',
            'record.from_period' => 'required|numeric',
            'record.to_period' => 'required|numeric',
            'record.year' => 'required|numeric',
        ];
        $validator = Validator::make($req->all(), $rules);

        // Validate the input and return correct response
        if ($validator->fails())
        {
            return response()->json(array(
                'status' => false,
                'msg' => 'Validation Error',
                'errors' => $validator->getMessageBag()->toArray()
            ));
        }
        if ($req->record['from_period'] <=  $req->record['to_period'])
        {
            $periodRecord = [];
            for ($i=$req->record['from_period']; $i <= $req->record['to_period'] ; $i++) { 
                array_push($periodRecord, $i);
            }
            $record = DB::table('material_average')->whereIn('period', $periodRecord)->where('year', $req->record['year']);
            if($req->record['material_code']){
                $record = $record->where('material_code', $req->record['material_code']);
            }
            $record = $record->get();
            $collection = collect($record);
            $grouped = $collection->groupBy('material_code');
            return response()->json(array(
                'status' => true,
                'data' => $grouped
            ));
        }
        return response()->json(array(
            'status' => false,
            'msg' => 'From Period should be less then to period',
        ));
    }

    public function showInventoryDetails(Request $req)
    {
        // dd($req->all());
        $detailList = MaterialInventory::where('material_code', $req->materialcode)->where('year', $req->year)->where('period', $req->period)->get()->toArray();
        return view('admin.layouts.pages.grn.inventorylistdetails', compact('detailList'));
    }
    
}