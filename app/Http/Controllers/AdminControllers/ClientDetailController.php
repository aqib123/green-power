<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\TutorialRequest;
use App\Http\Controllers\Controller;

class ClientDetailController extends Controller
{
    private $userTable;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->userTable = new User;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $title = 'Client Details';
        return view('admin.layouts.pages.clients', compact('title'));
    }

    /**
     *
     * Parents Registration
     *
     */
    public function parentsList(Request $req)
    {
        $result = $this->userTable->orderBy('created_at','desc')->where('type', 2)->where('user_type', 1)->paginate(10);
        return view('admin.layouts.pages.parents_list', compact('result'));
    }

    /**
     *
     * Parents Registration
     *
     */
    public function studentsList(Request $req)
    {
        $result = $this->userTable->orderBy('created_at','desc')->where('type', 2)->where('user_type', 2)->paginate(10);
        return view('admin.layouts.pages.students_list', compact('result'));
    }
}
