<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use Auth;
use App\Company;
use App\Http\Controllers\Controller;
use App\Http\GeneralFunctions;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Crypt;

class CompanyController extends Controller
{
	private $table;
    private $tableName;
    use GeneralFunctions;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->table = new Company;
        $this->tableName = 'company';
        $this->middleware('auth');
    }

    /**
     *
     * Vendors Form
     *
     */
    public function index(Request $req)
    {
    	return view('admin.layouts.pages.company.list');
    }

    /**
     *
     * Store Vendor Record
     *
     */
    public function store(Request $req)
    {
        $this->validate($req, [
            'name' => 'required|max:100',
        ], [
            'name.required' => 'Full Name is required',
        ]);
        // Save Record
        $data = $req->only(['name']);
        if($req->id)
        {
            $data = $req->only(['name', 'id']);
            $record = $this->table->where('id', $req->id)->update(['name' => $req->name]);
            return back()->with('success', 'Company Updated Successfully');
        }
        $data['code'] = GeneralFunctions::getNewCode($this->tableName, 0, 'company');
        $record = $this->table->create($data);
        if($record) 
        {
            return back()->with('success', 'Company Added Successfully');
        }
    }

    /**
     *
     * Company Data table record
     *
     */
    public function dataTableRequest(Request $request)
    {
        try {
            $company = $this->table->select(['id', 'name', 'code']);//->orderBy('users.id', 'Asc');
            return DataTables::of($company)
                ->addIndexColumn()
                ->addColumn('action', function ($company) {
                    $aciton = '<div class="btn-group" role="group"><button id="btnGroupDrop1" type="button" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button><div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                    $aciton .= '<a href="' . route("edit.company.form", ["id" => \Illuminate\Support\Facades\Crypt::encryptString($company->id)]) . '" class="dropdown-item"><i class="fa fa-edit"></i>Edit Company</a></div></div>';
                    return $aciton;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        catch (\Throwable $exception)
        {
            return DataTables::of([])->make(true);
        }
    }

    /**
     *
     * Company Form
     *
     */
    public function companyForm(Request $request)
    {
        $newCode = GeneralFunctions::getNewCode($this->tableName, 0, 'company');
        return view('admin.layouts.pages.company.form', compact('newCode'));
    }

    /**
     *
     * Edit company Form
     *
     */
    public function editCompany($id)
    {   
        $company = $this->table->where('id', Crypt::decryptString($id))->first();
        return view('admin.layouts.pages.company.form', compact('company'));
    }
    
}