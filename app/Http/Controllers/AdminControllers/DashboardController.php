<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
	private $userTable;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->userTable = new User;
        $this->middleware('auth');
    }

    /**
     *
     * Dashboard Stats
     *
     */
    public function index(Request $req)
    {
    	$totalUserRegistration = 0;
    	$totalTutorialRequest = 0;
    	return view('admin.layouts.pages.dashboard', compact('totalUserRegistration', 'totalTutorialRequest'));
    }
}