<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use Auth;
use App\Vendors;
use App\Http\Controllers\Controller;
use App\Http\GeneralFunctions;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Crypt;

class VendorsController extends Controller
{
	private $table;
    private $tableName;
    use GeneralFunctions;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->table = new Vendors;
        $this->tableName = 'vendors';
        $this->middleware('auth');
    }

    /**
     *
     * Vendors List
     *
     */
    public function index(Request $req)
    {
        return view('admin.layouts.pages.vendors.list');
    }

    /**
     *
     * Vendors Form
     *
     */
    public function vendorForm(Request $req)
    {
    	return view('admin.layouts.pages.vendors.form');
    }

    /**
     *
     * Store Vendor Record
     *
     */
    public function store(Request $req)
    {
        $this->validate($req, [
            'full_name' => 'required|max:100',
            'phone_number' => 'required|numeric',
            'vendor_type' => 'required'
        ], [
            'full_name.required' => 'Full Name is required',
            'full_name.max' => 'Full Name should be less then 100 characters',
            'phone_number.required' => 'Phone Number is required',
            'phone_number.numeric' => 'Phone Number should be valid number',
            'vendor_type' => 'Vendor Type is required'
        ]);
        try
        {
            // Save Record
            $data = $req->only(['full_name', 'phone_number', 'address', 'description', 'profile_image', 'vendor_type']);
            $data['code'] = GeneralFunctions::getNewCode($this->tableName, $data['vendor_type'], 'vendor_type', true);
            $record = $this->table->create($data);
            if($record) 
            {
                return back()->with('success', 'Vendor Added Successfully');
            }
        } 
        catch(\Throwable $exception) 
        {
            return back()->withErrors('Internal server error');
        }
    }

    /**
     *
     * Vendor Data table record
     *
     */
    public function dataTableRequest(Request $request)
    {
        try {
            $vendor = $this->table->select(['id', 'full_name', 'phone_number', 'address', 'description', 'vendor_type']);//->orderBy('users.id', 'Asc');
            return DataTables::of($vendor)
                ->addIndexColumn()
                // ->addColumn('action', function ($costcenter) {
                //     $aciton = '<div class="btn-group" role="group"><button id="btnGroupDrop1" type="button" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button><div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                //     $aciton .= '<a href="' . route("edit.vendor.form", ["id" => \Illuminate\Support\Facades\Crypt::encryptString($costcenter->id)]) . '" class="dropdown-item"><i class="fa fa-edit"></i>Edit Cost Cneter</a></div></div>';
                //     return $aciton;
                // })
                // ->rawColumns(['action'])
                ->make(true);
        }

        catch (\Throwable $exception)
        {
            return DataTables::of([])->make(true);
        }
    }
}