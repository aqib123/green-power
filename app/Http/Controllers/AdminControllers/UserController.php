<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\GeneralFunctions;
use App\Mail\UserCredentials;
use App\Models\Admin\RMHistory;
use App\Role;
use App\Setting\Setting;
use App\Setting\UserRole;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    use GeneralFunctions;
    public function userList()
    {
        $users = User::with('roles')->where('id','<>', 1)->get();
        return view('admin.layouts.pages.users.users_list')->with('users', $users);
    }

    public function userForm()
    {
        $roles = Role::where('id', '<>', UserRole::SuperAdmin)->get();
        return view('admin.layouts.pages.users.user_form')->with('roles', $roles);
    }

    public function addUser(Request $request)
    {
        if (strpos($request->email, '@') == false)
        {
            $column = 'phone';
            $validation = "";
            $stringLenght = 'min:11';
        }
        else
        {
            $column = 'email';
            $validation = 'email';
            $stringLenght = 'max:100';
        }

        $this->validate($request, [
            'name' => ['required', 'string','max:100'],
            'email' => 'required|string|'.$stringLenght.'|unique:users,'.$column.'|'.$validation,
            'phone' => 'required',
            'password' => 'required|string|min:8|confirmed',
            'role' => 'required|string|not_in:0,1,2,3,4,5,6,7,8,9'
        ],
        [
            'email.unique' => 'This email or phone number has already been taken',
            'email.min' => 'The phone number must have 11 characters'
        ]);

        try
        {
            $role = Role::find(Crypt::decryptString($request->role));
            if (is_null($role))
            {
                return back()->withErrors('The selected role is invalid');
            }
            
            // create user
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email ;
            $user->phone = $request->phone;
            $user->password = Hash::make($request->password);
            $user->role = $role->id;
            $user->active = Setting::AccountActive;
            $user->save();

            // send verification email
            //$token = sha1($user->id).time().self::generate_salt();
            //$verification = new Verify();
            //$verification->email = $user->email;
            //$verification->token = $token;
            //$verification->save();

            // send email to new user
            //Mail::to($user->email)->send(new VerificationMail($token, $user->name));
            // send credentials email
            Mail::to($user->email)->send(new UserCredentials($request->password, $user->name));
            return redirect()->route('user.list')->with('success', 'User has been created');
        }
        catch (\Throwable $exception)
        {
            Log::error($exception->getMessage());
            return back()->withErrors('Internal server error');
        }
    }

    public function editUser($id)
    {
        try
        {
            $roles = Role::where('id', '<>', UserRole::SuperAdmin)->get();
            $user = User::find(Crypt::decryptString($id));
            if (!is_null($user))
            {
                return view('super_admin.users.edit_user')->with(['user' => $user, 'roles' => $roles]);
            }
            else
            {
                return back()->withErrors('No user found');
            }
        }
        catch (\Exception $exception)
        {
            return back()->withErrors('Internal server error');
        }
    }

    public function updateUser(Request $request)
    {
        $validation = (strpos($request->email, '@') == false) ? '' : 'email';
        $validationRules = [
            'name' => ['required', 'string','max:100'],
            'email' => 'required|string|max:255'.'|'.$validation,
            'role' => 'required|string|not_in:0,1,2,3,4,5,6,7,8,9'
        ];
        if (strlen($request->password) >= 1)
        {
            $validationRules['password'] = 'required|string|min:8|confirmed';
        }

        $this->validate($request, $validationRules);

        try
        {
            $user = User::find(Crypt::decryptString($request->id));
            if (!is_null($user))
            {
                $user->name = $request->name;
                $user->role = Crypt::decryptString($request->role);
                if (strlen($request->password) >= 1)
                {
                    $user->password = Hash::make($request->password);
                    $user->save();

                    Mail::to($user->email)->send(new UserCredentials($request->password, $user->name));
                }
                else
                {
                    $user->save();
                }
                return redirect()->route('user.list')->with('success', 'User has been updated');
            }
            else
            {
                return back()->withErrors('User not found');
            }
        }
        catch (\Exception $exception)
        {
            return back()->withErrors('Internal server error');
        }
    }

    public function deleteUser($id)
    {
        try
        {
            $user = User::find(Crypt::decryptString($id));
            if (!is_null($user))
            {
                $user->deleted_at = Carbon::now();
                $user->user_id = $user->id;
                unset($user->id);
                $userArray = $user->toArray();
                $userArray['password'] = $user->password;
                DB::table('user_history')->insert($userArray);
                $user->delete();
                return back()->with('success', 'User has been deleted');
            }
            else
            {
                return back()->withErrors('User not found');
            }
        }
        catch (\Exception $exception)
        {
            return back()->withErrors('Internal server error');
        }
    }

    public function dataTableRequest(Request $request)
    {
        try {
            $users = User::where('type', 1)->with('roles')->select(['users.id', 'name', 'email', 'role', 'active', 'phone']);//->orderBy('users.id', 'Asc');
            return DataTables::of($users)
                ->addIndexColumn()
                ->addColumn('status', function ($users) {
                    if ($users->active == Setting::AccountActive) {
                        $status = '<label class="badge badge-success text-white">Active</label>';
                    } elseif ($users->active == Setting::AccountPending) {
                        $status = '<label class="badge badge-light">Pending</label>';
                    }
                    return $status;
                })
                ->addColumn('action', function ($users) {
                    if ($users->role != UserRole::SuperAdmin) {
                        $aciton = '<div class="btn-group" role="group"><button id="btnGroupDrop1" type="button" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button><div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                        if (GeneralFunctions::checkEditPermission('users'))
                            $aciton .= '<a href="' . route("edit.user", ["id" => \Illuminate\Support\Facades\Crypt::encryptString($users->id)]) . '" class="dropdown-item"><i class="fa fa-edit"></i>Edit</a>';
                        if (GeneralFunctions::checkDeletePermission('users'))
                            $aciton .= '<a href="' . route('delete.user', ['id' => \Illuminate\Support\Facades\Crypt::encryptString($users->id)]) . '" class="dropdown-item delete_btn"><i class="fa fa-trash"></i>Delete</a></div></div>';
                    } else {
                        $aciton = '<lable class="badge badge-danger">Can not Change</lable>';
                    }
                    return $aciton;
                })
                ->rawColumns(['status', 'action'])
                ->make(true);
           /* ->addColumn('phone number',function($users)){
               {
                 if ($users->) {

                 }
               }
        }*/
        }

        catch (\Throwable $exception)
        {
            return DataTables::of([])->make(true);
        }
    }

    public function rmAssignmentForm()
    {
        $rm = User::where([['role', UserRole::RecoverManager], ['active', Setting::AccountActive]])->get();
        $fieldOfficers = User::where([['role', UserRole::FieldOfficer], ['active', Setting::AccountActive]])->get();
        return view('recovery_manager.assignment_form')->with(['rm' => $rm, 'fo' => $fieldOfficers]);
    }

    public function createAssignment(Request $request)
    {
        $this->validate($request, [
            'officer' => 'required|array',
            'manager' => 'required|numeric'
        ], [
            'field_officer.required' => 'Field Officers is required'
        ]);
        $bulk = [];
        try
        {
            $manager = $request->manager;
            $carbon = Carbon::now();
            foreach ($request->officer as $officer)
            {
                $bulk[] = [
                    'field_officer' => $officer,
                    'recovery_manager' => $manager,
                    'type' => Setting::Assigned,
                    'ass_date' => $carbon->toDateString(),
                    'created_at' => $carbon
                ];
            }
            RMHistory::whereIn('field_officer', $request->officer)->update(['type' => Setting::Unassigned]);

            RMHistory::insert($bulk);

            return back()->with('success', 'Field Officer have been assigned');
        }
        catch (\Throwable $exception)
        {
            Log::error('Recovery Manager: '.$exception->getMessage());
            return back()->withErrors('Whoops, looks like something went wrong');
        }
    }

    public function rmAssignmentList()
    {
        return view('recovery_manager.list');
    }

    public function rmHistoryDataTable(Request $request)
    {
        try
        {
            $history = RMHistory::with('field_user', 'rm_user')->select(['id','field_officer', 'recovery_manager', 'type', 'ass_date']);
            return DataTables::of($history)
                ->addIndexColumn()
                ->addColumn('action', function ($history){

                    $aciton = '<div class="btn-group" role="group"><button id="btnGroupDrop1" type="button" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button><div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                    if(GeneralFunctions::checkEditPermission('recovery.manager'))
                        $aciton .= '<a href="'. route("fo.edit.assignment", ["id" => \Illuminate\Support\Facades\Crypt::encryptString($history->id)]).'" class="dropdown-item"><i class="fa fa-edit"></i>Edit</a>';
                    if(GeneralFunctions::checkDeletePermission('recovery.manager'))
                        $aciton .= '<a href="'. route('fo.delete.assignment', ['id' => \Illuminate\Support\Facades\Crypt::encryptString($history->id)]) .'" class="dropdown-item delete_btn"><i class="fa fa-trash"></i>Delete</a></div></div>';

                    return $aciton;
                })
                ->addColumn('type', function ($history){

                    if ($history->type == Setting::Assigned)
                    {
                        $type = '<lable class="badge badge-success">Assigned</lable>';
                    }
                    else if ($history->type == Setting::Unassigned)
                    {
                        $type = '<lable class="badge badge-light">Unassigned</lable>';
                    }

                    return $type;
                })
                ->rawColumns(['action', 'type'])
                ->make(true);
        }
        catch (\Throwable $exception)
        {
            return DataTables::of([])->make(true);
        }
    }

    public function editRMAssignment($id)
    {
        try
        {
            $history = RMHistory::with('field_user')->select(['id', 'recovery_manager', 'type', 'field_officer'])->find(Crypt::decryptString($id));
            if (!is_null($history))
            {
                $manager = User::where([['role', UserRole::RecoverManager], ['active', Setting::AccountActive]])->get();
                $fieldOfficers = User::where([['role', UserRole::FieldOfficer], ['active', Setting::AccountActive]])->get();
                return view('recovery_manager.edit')->with(['history' => $history, 'rm' => $manager, 'fo' => $fieldOfficers]);
            }
            else
            {
                return back()->withErrors('No assignment found');
            }
        }
        catch (\Exception $exception)
        {
            return back()->withErrors('Whoops, looks like something went wrong');
        }
    }

    public function rmUpdateAssignment(Request $request)
    {
        $this->validate($request, [
            'officer' => 'required|array',
            'manager' => 'required|numeric',
            'id' => 'required|string|min:20'
        ],['id.required' => 'Invalid Assignment', 'officer.required' => 'Field Officer is required']);
        try
        {
            $history = RMHistory::find(Crypt::decryptString($request->id));
            if (!is_null($history))
            {
                $history->recovery_manager = $request->manager;
                $history->field_officer = $request->officer[0];
                $history->save();
                return redirect()->route('fo.assignment.list')->with('success', 'Field Officer assignment has been updated');
            }
            else
            {
                return redirect()->route('fo.assignment.list')->withErrors('Invalid assignment');
            }
        }
        catch (\Throwable $exception)
        {
            return back()->withErrors('Whoops, looks like something went wrong');
        }
    }

    public function rmdeleteAssignment($id)
    {
        try
        {
            $history = RMHistory::find(Crypt::decryptString($id));
            if (!is_null($history))
            {
                $history->delete();
                return back()->with('success', 'Assignment has been deleted');
            }
            else
            {
                return back()->withErrors('No assignment found');
            }
        }
        catch (\Throwable $exception)
        {
            return back()->withErrors('Whoops, looks like something went wrong');
        }
    }

    public function foPerDayAvg()
    {
        return view('fo_attendance.foavg');
    }

    public function foAvgDatatable(Request $request)
    {
        try
        {
            $month = $request->last_month ?? Carbon::now()->month;

            $query = DB::table('consumers')->selectRaw('consumers.ibcname, count(visits.id) visitcount')
                ->leftJoin('visits', function ($join) use ($month){
                    $join->on(DB::raw('visits.consumer_id = consumers.id and MONTH(visits.visit_date) = '.(int)$month.' and Year(visits.visit_date)'), DB::raw(''.Carbon::now()->year.''));
                })
            ->groupBy('consumers.ibcname')->orderBy('visitcount', 'DESC');

            return DataTables::of($query)->addColumn('fo_link', function ($query) use ($month){
                return '<a href="' . route('fo.ibc.visit', ['ibc' => Crypt::encryptString($query->ibcname), 'month' => Crypt::encryptString($month)]) . '">'. $query->ibcname .'</a>';
            })
                ->addColumn('month_name', function($query)use ($month){
                    return date("F", mktime(0, 0, 0, (int)$month, 1));
                })
                ->addIndexColumn()->rawColumns(['fo_link', 'month_name'])
                ->make(true);
        }
        catch (\Throwable $exception)
        {
            dd($exception->getMessage());
            return DataTables::of([])->make(true);
        }
    }

    public function IBCVisit($ibc, $month)
    {
        return view('fo_attendance.fo_visit_list')->with(['ibc' => $ibc, 'month' => $month ]);
    }

    public function FoVisitList(Request $request)
    {
        try
        {
            $ibc = Crypt::decryptString($request->ibc);
            $month  = Crypt::decryptString($request->month);

            $query = DB::table('consumers')->selectRaw('consumers.ibcname, visits.id, users.name')
                ->leftJoin('visits', function ($join) use ($month){
                    $join->on(DB::raw('visits.consumer_id = consumers.id and MONTH(visits.visit_date) = '.(int)$month.' and Year(visits.visit_date)'), DB::raw(''.Carbon::now()->year.''));
                })
                ->join('users', function ($join){
                    $join->on('users.id',  '=', 'visits.visiting_person');
                })
                ->where('consumers.ibcname', $ibc);

            return DataTables::of($query)->addIndexColumn()
                ->addColumn('month_name', function($query)use ($month){
                    return date("F", mktime(0, 0, 0, (int)$month, 1));
                })
                ->addColumn('visit_link', function ($query){
                    return '<a href="'. route('visit.images', ['id' => \Illuminate\Support\Facades\Crypt::encryptString($query->id)]) .'" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-eye"></i> View</a>';
                })
                ->rawColumns(['visit_link'])
                ->make(true);
        }
        catch (\Throwable $exception)
        {
            return DataTables::of([])->make(true);
        }
    }
}
