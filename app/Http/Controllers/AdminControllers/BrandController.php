<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use Auth;
use App\Brand;
use App\Http\Controllers\Controller;
use App\Http\GeneralFunctions;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Crypt;

class BrandController extends Controller
{
	private $table;
    private $tableName;
    use GeneralFunctions;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->table = new Brand;
        $this->tableName = 'brand';
        $this->middleware('auth');
    }

    /**
     *
     * Vendors Form
     *
     */
    public function index(Request $req)
    {
    	return view('admin.layouts.pages.brand.list');
    }

    /**
     *
     * Store Brand Record
     *
     */
    public function store(Request $req)
    {
        $this->validate($req, [
            'name' => 'required|max:100',
        ], [
            'name.required' => 'Name is required',
        ]);
        // Save Record
        $data = $req->only(['name']);
        if($req->id)
        {
            $data = $req->only(['name', 'id']);
            $record = $this->table->where('id', $req->id)->update(['name' => $req->name]);
            return back()->with('success', 'Brand Updated Successfully');
        }
        $data['code'] = GeneralFunctions::getNewCode($this->tableName, 0, 'brand');
        $record = $this->table->create($data);
        if($record) 
        {
            return back()->with('success', 'Brand Added Successfully');
        }
    }

    /**
     *
     * Brand Data table record
     *
     */
    public function dataTableRequest(Request $request)
    {
        try {
            $brand = $this->table->select(['id', 'name', 'code']);//->orderBy('users.id', 'Asc');
            return DataTables::of($brand)
                ->addIndexColumn()
                ->addColumn('action', function ($brand) {
                    $aciton = '<div class="btn-group" role="group"><button id="btnGroupDrop1" type="button" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button><div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                    $aciton .= '<a href="' . route("edit.brand.form", ["id" => \Illuminate\Support\Facades\Crypt::encryptString($brand->id)]) . '" class="dropdown-item"><i class="fa fa-edit"></i>Edit Brand</a></div></div>';
                    return $aciton;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        catch (\Throwable $exception)
        {
            return DataTables::of([])->make(true);
        }
    }

    /**
     *
     * Brand Form
     *
     */
    public function brandForm(Request $request)
    {
        $newCode = GeneralFunctions::getNewCode($this->tableName, 0, 'brand');
        return view('admin.layouts.pages.brand.form', compact('newCode'));
    }

    /**
     *
     * Edit Brand Form
     *
     */
    public function editBrand($id)
    {   
        $brand = $this->table->where('id', Crypt::decryptString($id))->first();
        return view('admin.layouts.pages.brand.form', compact('brand'));
    }
    
}