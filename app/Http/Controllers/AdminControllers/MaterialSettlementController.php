<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use Auth;
use App\GRNPurchaseOrder;
use App\PurchaseOrder;
use App\Http\Controllers\Controller;
use App\Http\GeneralFunctions;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Crypt;
use App\MaterialInventory;
use App\GRNPurchaseOrderMaterial;
use App\MaterialAverage;
use App\MaterialSettlement;
use DB;
use Validator;


class MaterialSettlementController extends Controller
{
	private $table;
    private $tableName;
    use GeneralFunctions;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->table = new MaterialSettlement;
        $this->tableName = 'material_settlement';
        $this->middleware('auth');
    }

    /**
     *
     * Vendors Form
     *
     */
    public function index(Request $req)
    {

    	return view('admin.layouts.pages.settlement.list');
    }

    /**
     *
     * Store Material Settlement Record
     *
     */
    public function store(Request $req)
    {
        $this->validate($req, [
            'recieved_vendor_qty' => 'required|numeric',
            'material_settlement' => 'required'
        ]);
        $data = $req->only(['recieved_vendor_qty', 'material_settlement']);
        $record = $this->table->where('id', Crypt::decryptString($data['material_settlement']))->first();
        if($record)
        {
            if($data['recieved_vendor_qty'] > $record->settlement_qty)
            {
                return back()->withErrors('Please put valid quantity'); 
            }
            // Settlement Process
            $documentNumber = GeneralFunctions::getDocumentCode('material_inventory');
            $materialInventory = [
                'storage_location' => 100,
                'movement_type' => 110,
                'material_code' => $record->material_code,
                'po_code' => $record->purchase_order_code,
                'material_document_number' => $documentNumber,
                'quantity' => (int) $data['recieved_vendor_qty'],
                'amount' => $record->settlement_rate * ($data['recieved_vendor_qty'] + $record->settlement_qty),
                'period' => date('m'),
                'year' => date('Y'),
                'company_code' => 104,
                'cost_center_code' => 1005,
                'rate' => $record->settlement_rate,
                'created_at' => date('Y-m-d')
            ];
            MaterialInventory::create($materialInventory);
            MaterialSettlement::where('id', Crypt::decryptString($data['material_settlement']))->update(['status' => 1, 'recieved_vendor_qty' => $data['recieved_vendor_qty']]);
            return back()->with('success', 'GRN Entery added Successfully');
            /**
            
                TODO:
                - Average Material
             */
            
        }
        return back()->withErrors('Internal Server Error');
    }

    /**
     *
     * Settlement Form
     *
     */
    public function settlementForm(Request $request)
    {
        return view('admin.layouts.pages.grn.form');
    }


    /**
     *
     * Data Table
     *
     */
    public function dataTableRequest()
    {
        try {
            $grn = $this->table->select('*');//->orderBy('users.id', 'Asc');
            return DataTables::of($grn)
                ->addIndexColumn()
                ->addColumn('action', function ($grn) {
                    $aciton = '<div class="btn-group" role="group"><button id="btnGroupDrop1" type="button" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button><div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                    $aciton .= '<a href="' . route("show.settlement.details", ["id" => \Illuminate\Support\Facades\Crypt::encryptString($grn->id)]) . '" class="dropdown-item"><i class="fa fa-edit"></i>Show Settlement Form</a></div></div>';
                    return $aciton;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        catch (\Throwable $exception)
        {
            return DataTables::of([])->make(true);
        }
    }

    /**
     *
     * Show GRN Details
     *
     */
    public function showSettlementDetail($id)
    {
        $record = [];
        $record = $this->table->where('id', Crypt::decryptString($id))->first();
        if($record)
        {
            $record = $record->toArray();
        }
        return view('admin.layouts.pages.settlement.form', compact('record'));
    }
    
}