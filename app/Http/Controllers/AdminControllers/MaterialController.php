<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use Auth;
use App\Material;
use App\FGType;
use App\Brand;
use App\Http\Controllers\Controller;
use App\Http\GeneralFunctions;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Crypt;

class MaterialController extends Controller
{
	private $table;
    private $tableName;
    use GeneralFunctions;

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->table = new Material;
        $this->tableName = 'material';
        $this->middleware('auth');
    }

    /**
     *
     * Material List
     *
     */
    public function index(Request $req)
    {
    	return view('admin.layouts.pages.materials.list');
    }

    /**
     *
     * Vendors Form
     *
     */
    public function materialForm(Request $req)
    {
        $brands = Brand::all()->toArray();
        $fgTypes = FGType::all()->toArray();
        return view('admin.layouts.pages.materials.form', compact('brands', 'fgTypes'));
    }

    /**
     *
     * Store Vendor Record
     *
     */
    public function store(Request $req)
    {
        $validationPoints = [
            'name' => 'required|max:100',
            'material_type' => 'required',
            'brand' => 'required',
            'fg_type' => 'required',
            'no_of_uom' => 'required|numeric'
        ];
        if((int) $req->material_varient != 2)
        {
            $validationPoints['watt'] = 'required';
        }

        $this->validate($req, $validationPoints, [
            'name.required' => 'Material Name is required',
            'name.max' => 'Material Name should be less then 100 characters',
            'material_type' => 'Material Type is required'
        ]);
        try
        {
            // Save Record
            $data = $req->only(['name', 'description', 'material_type', 'watt', 'brand', 'fg_type', 'no_of_uom', 'material_varient']);
            $data['code'] = GeneralFunctions::getNewCode($this->tableName, $data['material_type'], 'material_type', true);
            $record = $this->table->create($data);
            if($record) 
            {
                return back()->with('success', 'Material Item Added Successfully');
            }
        } 
        catch(\Throwable $exception) 
        {
            return back()->withErrors('Internal server error');
        }
    }

    /**
     *
     * Vendor Data table record
     *
     */
    public function dataTableRequest(Request $request)
    {
        try {
            $material = $this->table->with('brand', 'fg_type');//->orderBy('users.id', 'Asc');
            return DataTables::of($material)
                ->addIndexColumn()
                // ->addColumn('action', function ($costcenter) {
                //     $aciton = '<div class="btn-group" role="group"><button id="btnGroupDrop1" type="button" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button><div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                //     $aciton .= '<a href="' . route("edit.vendor.form", ["id" => \Illuminate\Support\Facades\Crypt::encryptString($costcenter->id)]) . '" class="dropdown-item"><i class="fa fa-edit"></i>Edit Cost Cneter</a></div></div>';
                //     return $aciton;
                // })
                // ->rawColumns(['action'])
                ->make(true);
        }

        catch (\Throwable $exception)
        {
            return DataTables::of([])->make(true);
        }
    }
}