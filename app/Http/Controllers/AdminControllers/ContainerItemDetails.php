<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContainerItemDetails extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'purchase_order_id', 'conatainer_id'
    ];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'container_item_details';
}