<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettlementContainerItems extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'purchase_order_id', 'material_id', 'quantity', 'rate_in_pkr', 'total_in_pkr', 'status', 'container_id'
    ];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'settlement_container_items_detail';

    public function material_detail()
    {
        return $this->hasOne(Material::class, 'id', 'material_id');
    }

    public function purchase_order_detail()
    {
        return $this->belongsTo(PurchaseOrder::class, 'purchase_order_id', 'id');
    }
}