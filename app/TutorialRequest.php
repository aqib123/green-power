<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TutorialRequest extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'email', 'subjects', 'phone_no', 'country', 'gradelevels', 'status', 'type', 'comment'
    ];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'form_request_migration';

    public function getTypeAttribute($value)
    {
    	return $value == 1 ? 'Student' : 'Parent';
    }

    public function getStatusAttribute($value)
    {
    	return $value == 0 ? '<span class="badge badge-warning">New Request</span>' : '<span class="badge badge-info">Free Trial</span>';
    }
}