<?php

namespace App\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCredentials extends Mailable
{
    use Queueable, SerializesModels;

    private $password;
    private $username;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($password, $username)
    {
        $this->password = $password;
        $this->username = $username;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.user_credentials')->with(['password'=> $this->password, 'year' => Carbon::now()->year, 'userName' => $this->username]);
    }
}
