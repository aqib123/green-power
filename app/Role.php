<?php

namespace App;

use App\Http\GeneralFunctions;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes, GeneralFunctions;

    protected $fillable = ['name', 'created_by'];
    protected $dates = ['deleted_at'];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'permission_role', 'role_id', 'permission_id')->select('screen_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function userByRole()
    {
        return $this->hasMany(User::class, 'role', 'id');
    }
}
