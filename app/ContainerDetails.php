<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ContainerItemDetails;

class ContainerDetails extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 'code', 'status', 'no_of_packages', 'custom_reference', 'cash_no', 'igm_no', 'vessel', 'bill_no', 'exchange_rate_yuan', 'personal_expense_in_yuan', 'shipping_expense_in_yuan', 'personal_expense_in_pkr', 'shipping_expense_in_pkr', 'custom_duty_one', 'custom_duty_two', 'excise_one', 'excise_two', 'do', 'do_charges', 'group_misc', 'examination','yard_payment', 'duty', 'token', 'de_blocking', 'counter_ground', 'sepy', 'pdc_charges', 'auction_release', 'agency'
    ];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'container_details';

    // public function getStatusAttribute($value)
    // {
    // 	return $value == 1 ? 'In transit' : 'Pending';
    // }

    public function container_item_details()
    {
        return $this->hasMany(ContainerItemDetails::class, 'conatainer_id', 'id');
    }

    public function get_settlement_options()
    {
        return $this->hasMany(SettlementContainerItems::class, 'container_id', 'id');
    }

    public function container_files_details()
    {
        return $this->hasMany(ContainerFiles::class, 'conatainer_id', 'id');
    }

}