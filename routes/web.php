<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*----------  Website Pages  ----------*/
Route::get('/', function () {
    return redirect('admin/login');
});
/*----------  End of Website Routes  ----------*/

/*================================================
=            Section for Admin Portal           =
================================================*/
Auth::routes();
Route::prefix('admin')->group(function () {
	Route::get('login', function () {
        return view('admin.layouts.pages.login');
    })->name('admin.login')->middleware('guest');

    Route::get('forget/password', function () {
        return view('auth.passwords.email');
    })->name('admin.forget.password');
    
    Route::group(['middleware' => 'admin.auth'], function () {
        Route::get('dashboard', 'AdminControllers\DashboardController@index');

        Route::get('tutorial/request', 'AdminControllers\TutorialRequestController@index');

        Route::get('register/parents/details', 'AdminControllers\ClientDetailController@parentsList');

        Route::get('register/students/details', 'AdminControllers\ClientDetailController@studentsList');

        /*=======================================================
        =            Section For Access Control List            =
        =======================================================*/
        Route::get('add/role', 'AdminControllers\RolesController@roleForm')->name('role.form');
        Route::post('add/role', 'AdminControllers\RolesController@addRoleWithPermissions')->name('add.role');
        Route::get('role/list', 'AdminControllers\RolesController@rolesList')->name('role.list');
        Route::get('edit/role/{id}', 'AdminControllers\RolesController@editRole')->name('edit.role');
        Route::get('role/delete/{id}', 'AdminControllers\RolesController@deleteRole')->name('delete.role');
        Route::post('role/list', 'AdminControllers\RolesController@dataTableRequest')->name('role.datatable.request');

        Route::get('add/user', 'AdminControllers\UserController@userForm')->name('add.user.form');
        Route::post('add/user', 'AdminControllers\UserController@addUser')->name('add.user');
        Route::get('user/list', 'AdminControllers\UserController@userList')->name('user.list');
        Route::get('edit/user/{id}', 'AdminControllers\UserController@editUser')->name('edit.user');
        Route::post('update/user', 'AdminControllers\UserController@updateUser')->name('update.user');
        Route::get('delete/user/{id}', 'AdminControllers\UserController@deleteUser')->name('delete.user');
        Route::post('user/list', 'AdminControllers\UserController@dataTableRequest')->name('user.datatable.request'); 
        /*=====  End of Section For Access Control List  ======*/

        /*==================================================
        =            Section For Vendor Details            =
        ==================================================*/
        Route::get('vendor/list', 'AdminControllers\VendorsController@index')->name('vendor.list');
        Route::post('vendor/list', 'AdminControllers\VendorsController@dataTableRequest')->name('vendor.datatable.request');
        Route::get('add/vendor', 'AdminControllers\VendorsController@vendorForm')->name('vendor.form');
        Route::post('add/vendor', 'AdminControllers\VendorsController@store')->name('add.vendor');
        
        /*=====  End of Section For Vendor Details  ======*/

        /*==================================================
        =            Section For Material Details          =
        ==================================================*/
        Route::get('material/list', 'AdminControllers\MaterialController@index')->name('vendor.list');
        Route::post('material/list', 'AdminControllers\MaterialController@dataTableRequest')->name('material.datatable.request');
        Route::get('add/material', 'AdminControllers\MaterialController@materialForm')->name('material.form');
        Route::post('add/material', 'AdminControllers\MaterialController@store')->name('add.material');
        
        /*=====  End of Section For Material Details  ======*/

        /*==================================================
        =            Section For PO Details               =
        ==================================================*/
        Route::get('purchase/order/list', 'AdminControllers\PurchaseOrderController@index')->name('vendor.list');
        Route::post('purchase/order/list', 'AdminControllers\PurchaseOrderController@dataTableRequest')->name('purchase_order.datatable.request');
        Route::get('add/purchase/order', 'AdminControllers\PurchaseOrderController@purchaseOrder')->name('purchase.order.form');
        Route::post('add/purchase/order', 'AdminControllers\PurchaseOrderController@store')->name('add.purchase.order');
        Route::post('purchase/order', 'AdminControllers\PurchaseOrderController@getPurchaseOrderDetails')->name('get.purchase.order');
        Route::post('po/material/list', 'AdminControllers\PurchaseOrderController@getPurchaseOrderMaterials')->name('get.purchase.order.materials');
        
        /*=====  End of Section For PO Details  ======*/

        /*==================================================
        =            Section For Container Details         =
        ==================================================*/
        Route::get('container/list', 'AdminControllers\ContainerController@index')->name('container.form');
        Route::post('container/list', 'AdminControllers\ContainerController@dataTableRequest')->name('container.datatable.request');
        Route::get('add/container/details', 'AdminControllers\ContainerController@form')->name('container.form');
        Route::post('add/container/details', 'AdminControllers\ContainerController@store')->name('add.container.details');
        Route::get('edit/container/{id},{status}', 'AdminControllers\ContainerController@editContainer')->name('edit.container');
        
        /*=====  End of Section For Material Details  ======*/
        
        /*==================================================
        =            Section For Company Details           =
        ==================================================*/
        Route::get('company/list', 'AdminControllers\CompanyController@index')->name('company.list');
        Route::post('company/list', 'AdminControllers\CompanyController@dataTableRequest')->name('company.datatable.request');
        Route::get('add/company', 'AdminControllers\CompanyController@companyForm')->name('add.company.form');
        Route::post('add/company', 'AdminControllers\CompanyController@store')->name('add.company');
        Route::get('edit/company/{id}', 'AdminControllers\CompanyController@editCompany')->name('edit.company.form');
        
        /*=====  End of Section For Company Details  ======*/

        /*==================================================
        =            Section For Cost Center Details       =
        ==================================================*/
        Route::get('costcenter/list', 'AdminControllers\CostCenterController@index')->name('costcenter.list');
        Route::post('costcenter/list', 'AdminControllers\CostCenterController@dataTableRequest')->name('costcenter.datatable.request');
        Route::get('add/costcenter', 'AdminControllers\CostCenterController@costCenterForm')->name('add.costcenter.form');
        Route::post('add/costcenter', 'AdminControllers\CostCenterController@store')->name('add.costcenter');
        Route::get('edit/costcenter/{id}', 'AdminControllers\CostCenterController@editCostCenter')->name('edit.costcenter.form');
        
        /*=====  End of Section For Cost Center Details  ======*/

        /*==================================================
        =            Section For GRN Enteries              =
        ==================================================*/
        Route::get('grn/form', 'AdminControllers\GRNController@grnForm')->name('grn.form');
        Route::post('grn/form', 'AdminControllers\GRNController@store')->name('add.grn.record');
        Route::get('grn/list', 'AdminControllers\GRNController@index')->name('grn.list');
        Route::post('grn/list', 'AdminControllers\GRNController@dataTableRequest')->name('grn.datatable.request');
        Route::get('show/grn/detail/{id}', 'AdminControllers\GRNController@showGrnDetail')->name('show.grn.details');
        Route::get('inventory/warehouse/detail', 'AdminControllers\GRNController@warehouseDetailForm')->name('inventory.warehouse.list');
        Route::post('inventory/report', 'AdminControllers\GRNController@inventoryReport')->name('inventory.warehous.report');
        Route::get('show/inventory/details', 'AdminControllers\GRNController@showInventoryDetails')->name('inventory.warehouse.detail.lisr');
        /*=====  End of Section For GRN Enteries  ======*/

        /*==================================================
        =            Section For Brand Details           =
        ==================================================*/
        Route::get('brand/list', 'AdminControllers\BrandController@index')->name('brand.list');
        Route::post('brand/list', 'AdminControllers\BrandController@dataTableRequest')->name('brand.datatable.request');
        Route::get('add/brand', 'AdminControllers\BrandController@brandForm')->name('add.brand.form');
        Route::post('add/brand', 'AdminControllers\BrandController@store')->name('add.brand');
        Route::get('edit/brand/{id}', 'AdminControllers\BrandController@editBrand')->name('edit.brand.form');
        
        /*=====  End of Section For Brand Details  ======*/

        /*==================================================
        =            Section For FG Type Details           =
        ==================================================*/
        Route::get('fgtype/list', 'AdminControllers\FGTypeController@index')->name('fgtype.list');
        Route::post('fgtype/list', 'AdminControllers\FGTypeController@dataTableRequest')->name('fgtype.datatable.request');
        Route::get('add/fgtype', 'AdminControllers\FGTypeController@fgTypeForm')->name('add.fgtype.form');
        Route::post('add/fgtype', 'AdminControllers\FGTypeController@store')->name('add.fgtype');
        Route::get('edit/fgtype/{id}', 'AdminControllers\FGTypeController@editFgType')->name('edit.fgtype.form');
        
        /*=====  End of Section For FG Type Details  ======*/

        /*==================================================
        =            Section For GL Account Report         =
        ==================================================*/
        Route::get('glaccount/report', 'AdminControllers\GLAccountController@index')->name('glaccount.form');
        /*=====  End of Section For GL Account Report  ======*/

        /*==============================================================
        =            Section for Material Settlement Record            =
        ==============================================================*/
        Route::get('settlement/form', 'AdminControllers\MaterialSettlementController@settlementForm')->name('settlement.form');
        Route::post('settlement/form', 'AdminControllers\MaterialSettlementController@store')->name('add.settlement.record');
        Route::get('settlement/list', 'AdminControllers\MaterialSettlementController@index')->name('settlement.list');
        Route::post('settlement/list', 'AdminControllers\MaterialSettlementController@dataTableRequest')->name('settlement.datatable.request');
        Route::get('show/settlement/detail/{id}', 'AdminControllers\MaterialSettlementController@showSettlementDetail')->name('show.settlement.details');
        
        /*=====  End of Section for Material Settlement Record  ======*/
        
        
    });
});
/*=====  End of Section for Admin Portal ======*/
