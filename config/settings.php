<?php

return [
	'DocumentType' => [
		1 => ['D0', 'SYS'],
		2 => ['D1', 'MANUAL']
	],
	'MovementType' => [
		105 => 'GRN With PO',
		104 => 'GRN With PO Reversal',
		201 => 'Goods issue for a cost center',
		202 => 'Goods issue for a cost center Reversal',
		261 => 'Goods issue for an Production order',
		262 => 'Goods issue for an Production order Reversal',
		231 => 'Goods issue for a customer order',
		232 => 'Goods issue for a customer order Reversal',
		331 => 'Withdrawal of sample from stock in quality inspection',
		332 => 'Withdrawal of sample from stock in quality inspection Reversal',
		340 => 'Revaluation of batch',
		341 => 'Revaluation of batch Reversal',
		501 => 'Revaluation of batch',
		502 => 'Revaluation of batch Reversal'
	],
	'TypeOfGRN' => [
		100 => 'Purchase Order',
		101 => 'Production Return'
	],
	'GrnThresholdPercentage' => 0.02
];