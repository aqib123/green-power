<?php

return [
	'tables' => [
		'vendors' => [10000, 20000],
		'material' => [10000, 20000],
		'purchase_order' => [1000],
		'container_details' => [1000],
		'brand' => [1000],
		'company' => [101],
		'fg_types' => [1000]
	]
];