<?php
return [
	4001 => ['INCOME FROM SALES TO CUSTOMERS', 'Income'],
	4002 => ['INCOMES FROM SCRAPE SALES', 'Income'],
	4003 => ['INCOME FROM LOTS (DAMAGES STOCK', 'Income'],
	5001 => ['Salary', 'Expense'],
	5002 => ['Over Time', 'Expense'],
	5002 => ['Other Expense', 'Expense'],
	1021 => ['RM in Transit - Foreign', 'Asset'],
	1043 => ['Cash in Hand', 'Asset'],
	2002 => ['TRADE CREDITORS-Material', 'Liability'],
	1017 => [' RAW material Inventory Account', 'Asset']
];
